package com.mansionly.qaapp.view;

import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.os.StatFs;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.mansionly.qaapp.App;
import com.mansionly.qaapp.BackgroundServices.BackgroundProjectAlbumDataUploader;
import com.mansionly.qaapp.BackgroundServices.RebootServiceReceiver;
import com.mansionly.qaapp.Constants;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.VolleyMultipartRequest;
import com.mansionly.qaapp.client.VolleySingleton;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.localDb.SqliteDatabaseHandler;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.MediaCaptureHelper;
import com.mansionly.qaapp.util.Utility_functions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SiteGallery_Add_Image_Activity extends AppCompatActivity implements
        View.OnClickListener {
    private String mOidStr, mUserIdStr, mCustomerNameStr, mImageFlagStr;
    private Toolbar toolbar;
    private LinearLayout bMilestoneCapturePic, bMilestoneUploadPic;
    private TextView btnUpload, btnCancel, tvimagesAdded;
    private static final int TASK_PIC_CAPTURE_UPLOAD_FLAG = 101;
    private Uri mProfilePicUri;
    private String picFilePath;
    private GridView imGridImageView;
    private ArrayList<Uri> galleryImages = new ArrayList<Uri>();
    private ArrayList<Uri> picUriList = new ArrayList<>();
    private Bitmap milestonePicBmp;
    private MilestonePicsAdapter mAddImageAdapter;
    private File tempFile;
    private File picFile;
    int mImagePositionStr;
    public static int imagePosition = -1;
    private SqliteDatabaseHandler sqliteDatabaseHandler;
    private RebootServiceReceiver myReceiver;
    //    ArrayList<Bitmap> picBmpList = new ArrayList<Bitmap>();
//    ArrayList<File> picFileList = new ArrayList<File>();
//    Bitmap picBmp = null;
    private AppPreferences pref;
    private Map<String, String> milestoneViewableCheckMap = new HashMap<String, String>();
    private Map<String, String> milestonePicIdMap = new HashMap<>();
    private static final int CAMERA_PIC_REQUEST = 100;
    private ArrayList<String> milestoneLocalPicIdList = new ArrayList<String>();
    private MediaCaptureHelper mediaCaptureHelper;
    private ArrayList<String> picFilePathList = new ArrayList<>();
    private static final int UPLOAD_PIC_REQUEST = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sitegallery_add_image);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        sqliteDatabaseHandler = new SqliteDatabaseHandler(SiteGallery_Add_Image_Activity.this);
        imGridImageView = (GridView) findViewById(R.id.imGridImageView);
        bMilestoneCapturePic = (LinearLayout) findViewById(R.id.capture_image);
        bMilestoneCapturePic.setOnClickListener(this);
        bMilestoneUploadPic = (LinearLayout) findViewById(R.id.upload_image);
        bMilestoneUploadPic.setOnClickListener(this);
        btnUpload = (TextView) findViewById(R.id.btnUpload);
        btnCancel = (TextView) findViewById(R.id.btnCancel);
        tvimagesAdded = (TextView) findViewById(R.id.tvimagesAdded);
        btnCancel.setOnClickListener(onCancelClickListener);
        btnUpload.setOnClickListener(onSubmitClickListener);

        mediaCaptureHelper = new MediaCaptureHelper(this, null);
        mediaCaptureHelper.setMediaCaptureStartTrigger(bMilestoneCapturePic);

        Bundle extras = getIntent().getExtras();
        mOidStr = extras.getString(AppConsts.QA_ORDER_ID);
        pref = new AppPreferences(getApplicationContext());
        mUserIdStr = pref.getUserIdPref();
        mOidStr = pref.getOrderIdPref();
        mCustomerNameStr = pref.getCustomerName();
        milestonePicIdMap = pref.getMilestonePicIdMapPref();
        milestoneLocalPicIdList = pref.getTaskLocalPicIdPref();
        milestoneViewableCheckMap = pref.getMilestoneViewCheckMapPref();

        getSupportActionBar().setTitle(mCustomerNameStr);
        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + "SITE GALLERY - ADD IMAGES" + "</small>"));

        try {
            RetrieveTempPicFiles();

        } catch (Exception e) {
            e.printStackTrace();
        }

        setGridAdapter();
        tvimagesAdded.setText("Images Added" + " " + "(" + picUriList.size() + ")");

    }

    private void setGridAdapter() {
        mAddImageAdapter = new MilestonePicsAdapter(getApplicationContext(), picUriList);
        imGridImageView.setAdapter(mAddImageAdapter);

    }

    private void processImageFile(String picFilePaths, boolean isCapturedByCamera) {
        /*picFilePath = App.getCapturedImageAbsolutePath();
        Intent intent;
        if (isCapturedByCamera) {
            intent = new Intent(SiteGallery_Add_Image_Activity.this,
                    SiteGallery_Capture_Pic_Detail_Activity.class);
        } else {
            intent = new Intent(SiteGallery_Add_Image_Activity.this,
                    SiteGallery_Upload_Pic_Detail_Activity.class);
        }
        // intent.putExtra("milestonePicByteArray", byteArray);
        intent.putExtra("activity_log_status", "0");
        intent.putExtra("picFilePath", picFilePath);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);*/

        if (isCapturedByCamera) {
            picFilePath = App.getCapturedImageAbsolutePath();
        }
        milestonePicBmp = App.getCurrentImage();
        try {
            CreateMilestoteUploadTempFile();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            RetrieveTempPicFiles();
        } catch (Exception e) {
            e.printStackTrace();
        }
        pref.saveTaskLocalPicIdPref(milestoneLocalPicIdList);
        pref.saveTaskPicCount(pref.getTaskPicCount() + 1);
        setGridAdapter();
        tvimagesAdded.setText("Images Added" + " " + "(" + pref.getTaskPicCount() + ")");
    }

    private void CreateMilestoteUploadTempFile() throws Exception {
        File picFolderInSDCard = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()
                + File.separator + "Mansionly" + File.separator + "QAApp"
                + File.separator + "MansionlyProjectAlbum");
        if (!picFolderInSDCard.exists()) {
            try {
                if (picFolderInSDCard.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        File picFolderInSDCard1 = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()
                + File.separator + "Mansionly" + File.separator + "QAApp"
                + File.separator + "MansionlyProjectAlbum" + File.separator + "Temp");
        if (!picFolderInSDCard1.exists()) {
            try {
                if (picFolderInSDCard1.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        try {
            // picBmp = Utility_functions.compressImage(picFilePath);
            picFile = Utility_functions.GeneratePicFileFromBmp(picFilePath,
                    milestonePicBmp, 80);
//            picFileList.add(picFile);

            File tempFile = File.createTempFile("album" + System.currentTimeMillis() + "_", /* prefix */
                    ".jpg", /* suffix */
                    picFolderInSDCard); /* directory */
            File tempFile1 = File.createTempFile("album" + System.currentTimeMillis() + "_", /* prefix */
                    ".jpg", /* suffix */
                    picFolderInSDCard1); /* directory */
            Utility_functions.CopyFile(picFile, tempFile);
            Utility_functions.CopyFile(picFile, tempFile1);

            picFile = tempFile;
            picFilePathList.add(tempFile.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    private void RetrieveTempPicFiles() throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp" + File.separator + "MansionlyProjectAlbum" + File.separator + "Temp");
        if (!picFolderInSDCard.exists()) {
            try {
                if (picFolderInSDCard.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (picFolderInSDCard.listFiles().length != 0) {
                picUriList.clear();
                for (File eachFile : picFolderInSDCard.listFiles()) {
                    if (eachFile.getName().contains("album")) {
                        Uri tempUri = Uri.fromFile(eachFile);
                        picUriList.add(tempUri);
                    }
                }
            }
        }
        return;
    }

    View.OnClickListener onUploadPicClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            OnUploadPicClick();
        }
    };
    View.OnClickListener onCapturePicClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            OnCapturePicClick();
        }
    };

    public void OnCapturePicClick() {

        List<Intent> targetedIntents = new ArrayList<Intent>();

        Intent capturePicIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        List<ResolveInfo> resInfoCapturePic = this.getPackageManager().queryIntentActivities(capturePicIntent, 0);

        for (ResolveInfo eachResInfoCapturePic : resInfoCapturePic) {
            String packageNameCapturePic = eachResInfoCapturePic.activityInfo.packageName;
            Intent eachCaptureIntent = new Intent("android.media.action.IMAGE_CAPTURE");

            File photo = null;
            try {
                // place where to store camera taken picture
                photo = createTempProfilePicFile();
                photo.delete();

            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Please check SD card! Image shot is impossible!",
                        Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            mProfilePicUri = Uri.fromFile(photo);
            eachCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mProfilePicUri);
            eachCaptureIntent.putExtra("return-data", true);
            eachCaptureIntent.setPackage(packageNameCapturePic);
            targetedIntents.add(eachCaptureIntent);
        }

        Intent chooserIntent = Intent.createChooser(targetedIntents.remove(0), "Profile Pic");

        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedIntents.toArray(new Parcelable[]{}));

        startActivityForResult(chooserIntent, UPLOAD_PIC_REQUEST);

    }

    private File createTempProfilePicFile() throws Exception {
        File mProfilePicTempFile = null;
        File tempDir = Environment.getExternalStorageDirectory();

        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp" + File.separator + "MansionlyProjectAlbum");

        if (!picFolderInSDCard.exists()) {
            try {
                if (picFolderInSDCard.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            mProfilePicTempFile = File.createTempFile("album" + "_" + new Date().getTime(), /* prefix */
                    ".jpg", /* suffix */
                    picFolderInSDCard);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return mProfilePicTempFile;
    }

    public void OnUploadPicClick() {
        App.setCameraSession(false);
        Intent intent = new Intent();
        // Show only images, no videos or anything else
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), TASK_PIC_CAPTURE_UPLOAD_FLAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
            if (data == null) {
                File myFile = new File(mProfilePicUri.getPath());
                picFilePath = myFile.getAbsolutePath();
                Intent intent = new Intent(SiteGallery_Add_Image_Activity.this, SiteGallery_Capture_Pic_Detail_Activity.class);
                intent.putExtra("activity_log_status", "0");
                intent.putExtra("picFilePath", picFilePath);
                intent.putExtra("picUri", mProfilePicUri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        } else if (requestCode == TASK_PIC_CAPTURE_UPLOAD_FLAG && resultCode == RESULT_OK) {
            galleryImages.clear();

            ClipData clip = data.getClipData();

            if (clip != null) {
                for (int i = 0; i < clip.getItemCount(); i++) {
                    ClipData.Item item = clip.getItemAt(i);
                    Uri uri = item.getUri();
                    galleryImages.add(uri);
                }
            } else {
                Uri selectedImageUri = data.getData();
                galleryImages.add(selectedImageUri);
            }

            for (int i = 0; i < galleryImages.size(); i++) {
                String[] projection = {MediaStore.MediaColumns.DATA};

                CursorLoader cursorLoader = new CursorLoader(getApplicationContext(), galleryImages.get(i),
                        projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                //cursor is null sometimes

                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                picFilePath = cursor.getString(column_index);
                // picFile = new File(picFilePath);
                cursor.close();
                App.setCapturedImageAbsolutePath(picFilePath);

                //decoding should be done considering memory constraints.
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int width = displayMetrics.widthPixels;
                int height = width * 16 / 9;
                Bitmap bitmap = Utility_functions.decodeSampledBitmapFromFile(picFilePath, width, height);

                if (bitmap == null) {
                    Utility_functions.showToastOnMainThread(SiteGallery_Add_Image_Activity.this,
                            "The image file is invalid.");
                    return;
                }
                App.setCurrentImage(bitmap);
                processImageFile(picFilePath, false);
            }
        }
    }


    View.OnClickListener onSubmitClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            mImageFlagStr = "Upload";
            //UploadCancelImageTask();
            for (int i = 0; i < picFilePathList.size(); i++) {
                sqliteDatabaseHandler.insertProjectAlbumTempData(mUserIdStr, picFilePathList.get(i).toString(),
                        mOidStr, "1");
            }
            picFilePathList.clear();
            try {
                DeleteAllTempMilestonePicFile();
                pref.saveTaskPicCount(pref.getTaskPicCount() - 1);
                pref.clearMapPref();
            } catch (Exception e) {
                e.printStackTrace();
            }
            sqliteDatabaseHandler.getProjectAlbumTempTableData();
            startService(new Intent(SiteGallery_Add_Image_Activity.this,
                    BackgroundProjectAlbumDataUploader.class));
            finish();
        }
    };

    private void setReceiver() {
        myReceiver = new RebootServiceReceiver();
        IntentFilter intentFilter = new IntentFilter();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver, intentFilter);
    }

    private void UploadCancelImageTask() {
        String url = Webservice.PROJECT_UPLOAD_CANCEL_IMAGES;

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    JSONObject jobj = new JSONObject(resultResponse);
                    JSONObject gg = jobj.getJSONObject("result");
                    String msg = gg.getString("msg");
                    String msg_string = gg.getString("msg_string");

                    if (msg.equalsIgnoreCase("1")) {
                        try {
                            DeleteAllTempMilestonePicFile();
                            pref.clearMapPref();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(getApplicationContext(), Project_Album_Activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(AppConsts.CURRENT_TAB_INDEX, 0);

                        startActivity(intent);

                    } else {
                        Log.i("Unexpected", msg);
                    }
                } catch (
                        JSONException e)

                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        })

        {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(DBTableColumnName.USER_ID, mUserIdStr);
                params.put(DBTableColumnName.ORDER_ID, mOidStr);
                params.put(DBTableColumnName.QA_MILESTONE_PIC_ADD_REMOVE_FLAG, mImageFlagStr);

                StringBuilder sb = new StringBuilder();
                for (String key : milestonePicIdMap.keySet()) {
                    if (sb.length() > 0) {
                        sb.append("&");
                    }
                    String value = milestonePicIdMap.get(key);
                    try {

                        sb.append(value != null ? URLEncoder.encode(value, "UTF-8") : "");
                    } catch (UnsupportedEncodingException e) {
                        throw new RuntimeException("This method requires UTF-8 encoding support", e);
                    }
                }


                params.put(DBTableColumnName.QA_ACTIVITY_PIC_IDS, sb.toString());

                return params;
            }


        };

        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }

    View.OnClickListener onCancelClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final AlertDialog.Builder taskRejectCancelAlertDlg = new AlertDialog.Builder(SiteGallery_Add_Image_Activity.this);
            taskRejectCancelAlertDlg.setTitle("Do you want to cancel image upload ?");
            taskRejectCancelAlertDlg.setMessage("On Cancel, you will lose the images added");
            taskRejectCancelAlertDlg.setCancelable(true);

            taskRejectCancelAlertDlg.setPositiveButton(
                    "DON'T CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //finish();
                        }
                    });

            taskRejectCancelAlertDlg.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            try {

                                DeleteAllTempMilestonePicFile();
                                picUriList.clear();
                                pref.saveTaskPicCount(pref.getTaskPicCount() - 1);
                                pref.clearMapPref();
                                pref.clearImageCount();
                                mImageFlagStr = "Cancel";
                                tvimagesAdded.setText("Images Added" + " " + "(" + picUriList.size() + ")");
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });

            taskRejectCancelAlertDlg.create().show();

        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.capture_image:
                OnCapturePicClick();
                break;
            case R.id.upload_image:
                OnUploadPicClick();
                break;
        }
    }

    public class MilestonePicsAdapter extends BaseAdapter {
        LayoutInflater inflater;
        Context mContext;
        List<Uri> mAdapterImagesUriList;

        public MilestonePicsAdapter(Context context, ArrayList<Uri> picUriList) {
            mAdapterImagesUriList = picUriList;
            mContext = context;
        }

        @Override
        public int getCount() {
            return mAdapterImagesUriList.size();
        }

        @Override
        public Object getItem(int position) {
            return mAdapterImagesUriList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View itemView, ViewGroup parent) {
            final MilestonePicsAdapter.ViewHolder holder;
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (itemView == null) {
                itemView = inflater.inflate(R.layout.task_pics_layout, null);

                holder = new MilestonePicsAdapter.ViewHolder();
                holder.ivMilestonePic = (ImageView) itemView.findViewById(R.id.gridview_image);
                holder.ivMilestonePic.setPadding(5, 5, 5, 5);
                holder.ivMilestonePic.setScaleType(ImageView.ScaleType.CENTER_CROP);

                itemView.setTag(holder);
            } else {
                holder = (ViewHolder) itemView.getTag();
            }

            //set params on views here.
            Uri uri = mAdapterImagesUriList.get(mAdapterImagesUriList.size() - position - 1);
            Glide.with(SiteGallery_Add_Image_Activity.this).load(uri).into(holder.ivMilestonePic);
            //if (pref.getTaskLocalPicIdPref().size() != 0) {
            holder.ivMilestonePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        /*String id = pref.getTaskLocalPicIdPref().get(position);
                        final String pic_id = milestonePicIdMap.get(id);

                        if (pic_id == null || pic_id.isEmpty()) {
                            Utility_functions.showToastOnMainThread(SiteGallery_Add_Image_Activity.this,
                                    "Something went wrong.");
                            return;
                        }*/
                    Uri tempUri = mAdapterImagesUriList.get(mAdapterImagesUriList.size() - 1
                            - position);
                    Intent intent = new Intent(SiteGallery_Add_Image_Activity.this,
                            Site_GalleryImage_RemoveUpdate_Activity.class);
                    //intent.putExtra("pic_id", pic_id);
                    intent.putExtra("image", tempUri.toString());
                    intent.putExtra("image_position", position);
                    //intent.putExtra("viewableCheck", milestoneViewableCheckMap.get(pic_id));
                    startActivity(intent);
                }
            });
            //}

            return itemView;
        }

        public void refreshList(List<Uri> list) {
            mAdapterImagesUriList = list;
            this.notifyDataSetChanged();
        }

        class ViewHolder {
            public ImageView ivMilestonePic;
        }
    }

    private void DeleteTempMilestonePicFile() throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp" + File.separator + "MansionlyProjectAlbum" + File.separator + "Temp");

        File fList[] = picFolderInSDCard.listFiles();

        for (int i = 0; i < fList.length; i++) {
            File eachFile = fList[mImagePositionStr];
            String selectedFilePrefix = "album";
            if (eachFile.getName().contains(selectedFilePrefix)) {
                eachFile.delete();
            }
        }
        return;
    }

    public void RemoveImageFromGrid(int positions) {
        if (picUriList.size() != 0) {
            picUriList.remove(positions);
            picFilePathList.remove(positions);
            // picUriList.remove(positions);
            MilestonePicsAdapter mAddImageAdapter = new MilestonePicsAdapter(getApplicationContext(), picUriList);
            imGridImageView.setAdapter(mAddImageAdapter);
            mAddImageAdapter.refreshList(picUriList);
            setGridAdapter();
        }
    }

    private void DeleteAllTempMilestonePicFile() throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp" + File.separator + "MansionlyProjectAlbum" + File.separator + "Temp");
        if (!picFolderInSDCard.exists()) {
            try {
                if (picFolderInSDCard.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (picFolderInSDCard.listFiles().length != 0) {
                for (File eachFile : picFolderInSDCard.listFiles()) {
                    if (eachFile.getName().contains("album")) {
                        eachFile.delete();
                    }
                }
            }
        }
        return;
    }

    @Override
    protected void onStart() {
        super.onStart();
        setReceiver();
       /* if (picUriList.size() == 0) {
            btnUpload.setEnabled(false);
            btnUpload.setTextColor(Color.GRAY);

        } else {
            btnUpload.setEnabled(true);
            btnUpload.setTextColor(Color.parseColor("#FF0000"));
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            final AlertDialog.Builder taskRejectCancelAlertDlg = new AlertDialog.Builder(SiteGallery_Add_Image_Activity.this);
            taskRejectCancelAlertDlg.setTitle("Do you want to cancel image upload ?");
            taskRejectCancelAlertDlg.setMessage("On Cancel, you will lose the images added");
            taskRejectCancelAlertDlg.setCancelable(true);

            taskRejectCancelAlertDlg.setPositiveButton(
                    "DON'T CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                            // finish();
                        }
                    });

            taskRejectCancelAlertDlg.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                DeleteAllTempMilestonePicFile();
                                picUriList.clear();
                                pref.saveTaskPicCount(pref.getTaskPicCount() - 1);
                                pref.clearMapPref();
                                pref.clearImageCount();
                                mImageFlagStr = "Cancel";
                                tvimagesAdded.setText("Images Added" + " " + "(" + picUriList.size() + ")");
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            taskRejectCancelAlertDlg.create().show();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder taskRejectCancelAlertDlg = new AlertDialog.Builder(SiteGallery_Add_Image_Activity.this);
        taskRejectCancelAlertDlg.setTitle("Do you want to cancel image upload ?");
        taskRejectCancelAlertDlg.setMessage("On Cancel, you will lose the images added");
        taskRejectCancelAlertDlg.setCancelable(true);

        taskRejectCancelAlertDlg.setPositiveButton(
                "DON'T CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        // finish();
                    }
                });

        taskRejectCancelAlertDlg.setNegativeButton(
                "CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            DeleteAllTempMilestonePicFile();
                            picUriList.clear();
                            pref.saveTaskPicCount(pref.getTaskPicCount() - 1);
                            pref.clearMapPref();
                            pref.clearImageCount();
                            mImageFlagStr = "Cancel";
                            tvimagesAdded.setText("Images Added" + " " + "(" + picUriList.size() + ")");
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

        taskRejectCancelAlertDlg.create().show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == Constants.CAMERA_PERMISSION_REQUEST) {
            mediaCaptureHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onDestroy() {
        mediaCaptureHelper.clearCache();
        super.onDestroy();
    }

    public static void setImagePosition(int value) {
        imagePosition = value;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (imagePosition >= 0) {
            mImagePositionStr = imagePosition;
            setImagePosition(-1);
            try {
                DeleteTempMilestonePicFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
            RemoveImageFromGrid(mImagePositionStr);
            tvimagesAdded.setText("Images Added" + " " + "(" + picUriList.size() + ")");
        }

        String capturedMediaFilePath = App.getCapturedImageAbsolutePath();
        if (capturedMediaFilePath != null && App.isCameraSession()) {
            App.setCameraSession(false);
            processImageFile(capturedMediaFilePath, true);
        }

        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext(), Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }
}
