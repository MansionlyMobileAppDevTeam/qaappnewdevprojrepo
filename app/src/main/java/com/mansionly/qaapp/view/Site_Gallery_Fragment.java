package com.mansionly.qaapp.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.AndroidMultipartEntity;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.model.ProjectGalleryImages;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Site_Gallery_Fragment extends Fragment {

    View rootView;
    ArrayList<Uri> picUriList = new ArrayList<>();
    //    ArrayList<Bitmap> picBmpList = new ArrayList<Bitmap>();
//    ArrayList<File> picFileList = new ArrayList<File>();
//    Bitmap picBmp = null;
    MilestonePicsAdapter mAddImageAdapter;
    GridView imGridImageView;
    TextView btnAddImage;
    String mOidStr, mCustomerNameStr, mUseridStr;
    AppPreferences pref;
    String mPicIdSttr, mFlagCustomerViewable, mFileName, mBigImage, mTotalRecords;
    ArrayList<ProjectGalleryImages> mImageAlbumList = new ArrayList<ProjectGalleryImages>();
    ArrayList<String> mCustomerViewablelist = new ArrayList<>();
    ArrayList<ProjectGalleryImages> mQATaskList = new ArrayList<ProjectGalleryImages>();
    ArrayList<String> milestonePicsFilePathList = new ArrayList<>();
    //    InputStream in = null;
    View tab;
    TabLayout tabLayout;
    TextView tabOne;
    LinearLayout tvNoImagesLyout;
    ImageView imNoimages;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container1, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_qa_album_gallery, container1, false);
        setHasOptionsMenu(true);

        tvNoImagesLyout = (LinearLayout) rootView.findViewById(R.id.tvNoImagesLyout);
        imNoimages = (ImageView) rootView.findViewById(R.id.imNoimages);
        imGridImageView = (GridView) rootView.findViewById(R.id.imGridImageView);
        btnAddImage = (TextView) rootView.findViewById(R.id.btnAddImage);
        tabLayout = (TabLayout) getActivity().findViewById(R.id.tabs);
        tab = tabLayout.getTabAt(0).getCustomView(); // fo
        tabOne = (TextView) tab.findViewById(R.id.tab);

        pref = new AppPreferences(getActivity());
        mUseridStr = pref.getUserIdPref();
        mOidStr = pref.getOrderIdPref();
        mCustomerNameStr = pref.getCustomerName();

        btnAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pref.saveCustomerName(mCustomerNameStr);

                Intent intent = new Intent(getActivity(), SiteGallery_Add_Image_Activity.class);
                intent.putExtra(AppConsts.QA_ORDER_ID, mOidStr);
                intent.putExtra(AppConsts.QA_CUSTOMER_NAME, mCustomerNameStr);

                startActivity(intent);
            }
        });

        new GetSiteGalleryImages().execute();

        return rootView;
    }


    public class GetSiteGalleryImages extends AsyncTask<Void, Void, String> {
        ProgressDialog asyncDialog = new ProgressDialog(getActivity());

        protected void onPreExecute() {
            asyncDialog.setMessage("Loading...");
            asyncDialog.setCancelable(false);
            asyncDialog.setCanceledOnTouchOutside(false);
            //show dialog
            asyncDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String url = Webservice.PROJECT_SITE_GALLERY_IMAGES;

            String responseString = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            try {
                AndroidMultipartEntity entity = new AndroidMultipartEntity(
                        new AndroidMultipartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                            }
                        });

                entity.addPart(DBTableColumnName.USER_ID, new StringBody(mUseridStr));
                entity.addPart(DBTableColumnName.ORDER_ID, new StringBody(mOidStr));
                httppost.setEntity(entity);
                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
                e.printStackTrace();
            } catch (IOException e) {
                responseString = e.toString();
                e.printStackTrace();
            }
            return responseString;
        }

        protected void onPostExecute(String result) {
            asyncDialog.dismiss();

            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject jsonResult = jobj.getJSONObject("result");
                String msg = jsonResult.getString("msg");

                if (msg.equals("0")) {
                    tvNoImagesLyout.setVisibility(View.VISIBLE);
                    imNoimages.setVisibility(View.VISIBLE);
                } else if (msg.equals("1")) {
                    mCustomerViewablelist.clear();
                    milestonePicsFilePathList.clear();
                    try {
                        JSONArray jsonarray = jsonResult.getJSONArray("arr_record_list");
                        mTotalRecords = jsonResult.getString("total_records");

                        if (mTotalRecords.equalsIgnoreCase("0")) {
                            tvNoImagesLyout.setVisibility(View.VISIBLE);
                            imNoimages.setVisibility(View.VISIBLE);
                        }
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject obj = jsonarray.getJSONObject(i);

                            mPicIdSttr = obj.getString(DBTableColumnName.QA_IMAGE_PIC_ID);
                            mFlagCustomerViewable = obj.getString(DBTableColumnName.QA_IMAGE_CUSTOMER_VIEWABLE);
                            mFileName = obj.getString("file_name");
                            mBigImage = obj.getString("bigImg");

                            milestonePicsFilePathList.add(mFileName);
                            mCustomerViewablelist.add(mFlagCustomerViewable);

                            ProjectGalleryImages qaTask = new ProjectGalleryImages(mPicIdSttr,
                                    mFlagCustomerViewable, mFileName, mBigImage);
                            mQATaskList.add(qaTask);

                        }

                        Collections.reverse(milestonePicsFilePathList);
                        Collections.reverse(mCustomerViewablelist);
                        Collections.reverse(mQATaskList);

                        new DisplayMilestonePicsTask(milestonePicsFilePathList).execute();

                        tabOne.setText("SITE GALLERY" + " " + "(" + mTotalRecords + ")");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class DisplayMilestonePicsTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog asyncDialog = new ProgressDialog(getActivity());
        ArrayList<String> mImageHttpUrllist;
        ArrayList<Uri> milestonePicUriList = new ArrayList<>();

        public DisplayMilestonePicsTask(ArrayList<String> milestonePics) {
            this.mImageHttpUrllist = milestonePics;
        }

        @Override
        protected void onPreExecute() {
            asyncDialog.setMessage("Uploading...");
            asyncDialog.setCancelable(false);
            asyncDialog.setCanceledOnTouchOutside(false);
            //show dialog
            asyncDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int ii = 0; ii < mImageHttpUrllist.size(); ii++) {
                String httpUrl = mImageHttpUrllist.get(ii);
                milestonePicUriList.add(Uri.parse(httpUrl));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param) {
            mAddImageAdapter = new MilestonePicsAdapter(milestonePicUriList);
            imGridImageView.setAdapter(mAddImageAdapter);
            asyncDialog.dismiss();
        }
    }

    public class MilestonePicsAdapter extends BaseAdapter {
        ArrayList<Uri> mAdapterImagesUriList;

        public MilestonePicsAdapter(ArrayList<Uri> mImagesBitmap) {
            this.mAdapterImagesUriList = mImagesBitmap;
        }

        @Override
        public int getCount() {
            return mAdapterImagesUriList.size();
        }

        @Override
        public Object getItem(int position) {
            return mAdapterImagesUriList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View itemView, ViewGroup parent) {
            final MilestonePicsAdapter.ViewHolder holder;
//            ImageView image;

            if (itemView == null) {
                holder = new MilestonePicsAdapter.ViewHolder();
                itemView = LayoutInflater.from(getActivity()).inflate(R.layout.task_pics_layout, null);
                holder.ivMilestonePic = (ImageView) itemView.findViewById(R.id.gridview_image);
              //  holder.ivMilestonePic.setPadding(2, 2, 2, 2);
                holder.ivMilestonePic.setScaleType(ImageView.ScaleType.FIT_XY);
                holder.progress = new ProgressDialog(getActivity());
                itemView.setTag(holder);
            } else {
                holder = (MilestonePicsAdapter.ViewHolder) itemView.getTag();
            }

            //set params on views here.
            Glide.with(getActivity()).load(mAdapterImagesUriList.get(position))
                    .into(holder.ivMilestonePic);
            holder.ivMilestonePic.setId(mAdapterImagesUriList.size() - position - 1);
            holder.ivMilestonePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), Site_GalleryImage_Details_Activity.class);
                    intent.putParcelableArrayListExtra("imageArray", mAdapterImagesUriList);
                    intent.putStringArrayListExtra("viewableCheckArray", mCustomerViewablelist);
                    intent.putExtra("imagePosition", position);

                    startActivity(intent);
                }
            });

            return itemView;
        }

//        public void refreshList(List<Bitmap> list) {
//            mImages = list;
//            this.notifyDataSetChanged();
//        }

        class ViewHolder {
            public ImageView ivMilestonePic;
            ProgressDialog progress;
        }
    }

//    public Uri getImageUri(Context inContext, Bitmap inImage) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//        return Uri.parse(path);
//    }
}
