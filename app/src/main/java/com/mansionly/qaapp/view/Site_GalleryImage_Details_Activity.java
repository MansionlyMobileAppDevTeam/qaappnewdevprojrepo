package com.mansionly.qaapp.view;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.Utility_functions;

import java.util.ArrayList;

public class Site_GalleryImage_Details_Activity extends AppCompatActivity {
    AppPreferences pref;
    LinearLayout llNoInternet;
    Toolbar toolbar;
    ArrayList<String> viewableCheckArray = new ArrayList<String>();
    private ArrayList<Uri> imageArraList = new ArrayList<Uri>();
    private static ViewPager mPager;
    String mCustomerNameStr;
    int CurrentPosition;
    TextView tvImagecount, tvSubTitle, tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        llNoInternet = (LinearLayout) findViewById(R.id.internetCheck);
        new Handler().post(internetCheck);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tvTitle = (TextView) toolbar.findViewById(R.id.title);
        tvSubTitle = (TextView) toolbar.findViewById(R.id.subTitle);
        tvImagecount = (TextView) toolbar.findViewById(R.id.count);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        pref = new AppPreferences(getApplicationContext());
        mCustomerNameStr = pref.getCustomerName();

        tvTitle.setText(mCustomerNameStr);
        tvSubTitle.setText("SITE GALLERY");

        imageArraList = getIntent().getExtras().getParcelableArrayList("imageArray");
        viewableCheckArray = getIntent().getExtras().getStringArrayList("viewableCheckArray");
        CurrentPosition = getIntent().getExtras().getInt("imagePosition");

        init();

    }

    private void init() {
        for (int i = 0; i < imageArraList.size(); i++)
            mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new MyAdapter(Site_GalleryImage_Details_Activity.this, imageArraList));
        mPager.setCurrentItem(CurrentPosition);
        String counter = (CurrentPosition + 1) + " of " + imageArraList.size();
        tvImagecount.setText(counter);
        mPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                String counter = (position + 1) + " of " + imageArraList.size();
                tvImagecount.setText(counter);
            }
        });

       /* CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);*/
    }

    Runnable internetCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isOnline = Utility_functions.internetConnected(getApplicationContext());
            if (isOnline) {
                llNoInternet.setVisibility(View.GONE);
            } else {
                llNoInternet.setVisibility(View.VISIBLE);
            }
            new Handler().postDelayed(internetCheck, 5000);
        }
    };

    public class MyAdapter extends PagerAdapter {

        private ArrayList<Uri> images;
        private LayoutInflater inflater;
        private Context context;

        public MyAdapter(Context context, ArrayList<Uri> images) {
            this.context = context;
            this.images = images;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View myImageLayout = inflater.inflate(R.layout.activity_sitegallery_image_details, view, false);
            ImageView myImage = (ImageView) myImageLayout.findViewById(R.id.milestone_pic_imageView);
            CheckBox checkBox = (CheckBox) myImageLayout.findViewById(R.id.checkBox);
            //  myImage.setImageResource(images.get(position));
            Uri imageUri = imageArraList.get(position);
            Glide.with(Site_GalleryImage_Details_Activity.this).load(imageUri)
                    .apply(new RequestOptions().fitCenter())
                    .into(myImage);
            String mCustomerViewableCheck = viewableCheckArray.get(position);

            if (mCustomerViewableCheck.equalsIgnoreCase("1")) {
                checkBox.setText("Customer Viewable");
                checkBox.setChecked(true);
            } else {
                checkBox.setText("Customer Viewable");
                checkBox.setChecked(false);
            }

            view.addView(myImageLayout, 0);
            return myImageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext() , Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }
}
