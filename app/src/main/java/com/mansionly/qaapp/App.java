package com.mansionly.qaapp;

import android.app.Application;
import android.graphics.Bitmap;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by root on 20/11/17.
 */

public class App extends Application {
    private static Bitmap currentImage;
    private static String capturedImageAbsolutePath;
    private static boolean cameraSession;

    public static boolean isCameraSession() {
        return cameraSession;
    }

    public static void setCameraSession(boolean flag) {
        cameraSession = flag;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
    }

    public static void setCapturedImageAbsolutePath(String capturedImageAbsolutePath) {
        App.capturedImageAbsolutePath = capturedImageAbsolutePath;
    }

    public static Bitmap getCurrentImage() {
        return currentImage;
    }

    public static void setCurrentImage(Bitmap currentImage) {
        App.currentImage = currentImage;
    }

    public static String getCapturedImageAbsolutePath() {
        return capturedImageAbsolutePath;
    }

}
