package com.mansionly.qaapp.login;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mansionly.qaapp.Constants;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.Utility_functions;
import com.mansionly.qaapp.view.ForgotPasswordActivity;
import com.mansionly.qaapp.view.MainActivity;
import com.mansionly.qaapp.view.Version_control_dialog_fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogIn_Activity extends AppCompatActivity implements View.OnClickListener {
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;
    private EditText etEmailID, etPassword;
    private Button btnLogin;
    private TextView tvForgotPassword;
    private String strUserId, strPassword;
    private ProgressBar progressBar;
    private Dialog dialog;
    private LinearLayout llNoInternet;
    private TextInputLayout usernameWrapper, passwordWrapper;
    private GoogleLoginManager googleLoginManager;
    private CardView googleCardview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (ContextCompat.checkSelfPermission(LogIn_Activity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(LogIn_Activity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(LogIn_Activity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }


        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext(), Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
        googleCardview =  findViewById(R.id.googleCardview);
        googleCardview.setOnClickListener(this);

        googleLoginManager = new GoogleLoginManager(this);
        llNoInternet = (LinearLayout) findViewById(R.id.internetCheck);
        new Handler().post(internetCheck);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        etEmailID = (EditText) findViewById(R.id.etEmailID);
        etPassword = (EditText) findViewById(R.id.etPassword);
        tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
        usernameWrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);
        passwordWrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);
        btnLogin = (Button) findViewById(R.id.btnLogin);


        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LogIn_Activity.this, ForgotPasswordActivity.class);
                startActivity(intent);

            }
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strUserId = etEmailID.getText().toString().trim();
                strPassword = etPassword.getText().toString().trim();

                if (strUserId.equals("")) {
                    Toast.makeText(getApplicationContext(), "Please enter email address.", Toast.LENGTH_SHORT).show();
                } else if (strPassword.equals("")) {
                    Toast.makeText(getApplicationContext(), "Please enter the password.", Toast.LENGTH_SHORT).show();
                } else if (!isValidEmail(strUserId)) {
                    usernameWrapper.setError("Not a valid email address!");
                } else if (strPassword.length() < 6) {
                    passwordWrapper.setError("Not a valid password!");
                } else {
                    usernameWrapper.setErrorEnabled(false);
                    passwordWrapper.setErrorEnabled(false);
                    new LoginTask().execute(strUserId, strPassword);
                }
            }
        });


        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    //do something
                    strUserId = etEmailID.getText().toString().trim();
                    strPassword = etPassword.getText().toString().trim();

                    if (strUserId.equals("")) {
                        Toast.makeText(getApplicationContext(), "Please enter email address.", Toast.LENGTH_SHORT).show();
                    } else if (strPassword.equals("")) {
                        Toast.makeText(getApplicationContext(), "Please enter the password.", Toast.LENGTH_SHORT).show();
                    } else if (!isValidEmail(strUserId)) {
                        usernameWrapper.setError("Not a valid email address!");
                    } else if (strPassword.length() < 6) {
                        passwordWrapper.setError("Not a valid password!");
                    } else {
                        usernameWrapper.setErrorEnabled(false);
                        passwordWrapper.setErrorEnabled(false);
                        new LoginTask().execute(strUserId, strPassword);
                    }

                }
                return false;
            }
        });

    }

    Runnable internetCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isOnline = Utility_functions.internetConnected(getApplicationContext());
            if (isOnline) {
                llNoInternet.setVisibility(View.GONE);
            } else {
                llNoInternet.setVisibility(View.VISIBLE);
            }
            new Handler().postDelayed(internetCheck, 5000);
        }
    };


    private boolean isValidEmail(String s3) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(s3);
        return matcher.matches();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.googleCardview){
            onGoogleSignInClicked();
        }
    }

    public class LoginTask extends AsyncTask<String, String, String> {

        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pd == null) {
                pd = new ProgressDialog(LogIn_Activity.this);
                pd.setMessage("Loading...");
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... args0) {
            try {

                String username = (String) args0[0];
                String password = (String) args0[1];

                String link = Webservice.LOGIN;

                String data = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8");
                data += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter
                        (conn.getOutputStream());
                wr.write(data);
                wr.flush();
                BufferedReader reader = new BufferedReader
                        (new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }
                return sb.toString();
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("result", result);

            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject gg = jobj.getJSONObject("result");
                String msg = gg.getString("msg");
                String msg_string = gg.getString("msg_string");

                if (msg.equals("0")) {
                    Toast toast = Toast.makeText(getApplicationContext(), msg_string, Toast.LENGTH_LONG);
                    toast.show();
                } else if (msg.equals("1")) {

                    String userid = gg.getString(DBTableColumnName.USER_ID);
                    String userType = gg.getString(DBTableColumnName.USER_TYPE);
                    String userName = gg.getString(DBTableColumnName.USER_NAME);
                    String userEmailId = gg.getString(DBTableColumnName.USER_EMAIL_ID);
                    String user_profile_qa_supervisor = gg.getString(DBTableColumnName.USER_PROFILE_QA_SUPERVISOR);
                    JSONArray privilegeArray = gg.getJSONArray(DBTableColumnName.USER_PRIVILEGE);

                    Map<String, Object> userInfo = new HashMap<String, Object>();
                    userInfo.put(AppConsts.USER_ID, userid);
                    userInfo.put(AppConsts.USER_NAME, userName);
                    userInfo.put(AppConsts.USER_TYPE, userType);
                    userInfo.put(AppConsts.USER_EMAIL_ID, userEmailId);

                    AppPreferences prefs = new AppPreferences(getApplicationContext());
                    prefs.saveUserLoginStatus(true);
                    prefs.saveUserInfo(userInfo);
                    prefs.saveUserIdPref(userid);
                    prefs.saveUserEnailIdPref(userEmailId);
                    prefs.saveUserPrivilage(privilegeArray);
                    prefs.saveUserQASupervisor(user_profile_qa_supervisor);

                    Intent intent = new Intent(LogIn_Activity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                pd.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {

                }
                return;
            }

        }
    }

    public void onGoogleSignInClicked() {
        googleLoginManager.startLogin();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.RequestCode.GOOGLE_SIGN_IN) {
            googleLoginManager.onActivityResult(data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
