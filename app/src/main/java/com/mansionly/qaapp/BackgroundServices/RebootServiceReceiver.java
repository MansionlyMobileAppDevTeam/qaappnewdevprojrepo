package com.mansionly.qaapp.BackgroundServices;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class RebootServiceReceiver extends BroadcastReceiver
{

    static final String ACTION = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION)) {
            //Service
            Intent serviceIntent = new Intent(context, BackgroundMileStoneDataUploader.class);
            context.startService(serviceIntent);
        }
    }

}