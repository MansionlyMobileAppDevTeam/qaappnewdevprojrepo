package com.mansionly.qaapp.view;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.util.Utility_functions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mansionly.qaapp.R;

public class ForgotPasswordActivity extends AppCompatActivity {
    EditText etForgotEmailID;
    Button btnResetPassword;
    TextInputLayout usernameWrapper;
    ProgressBar progressBar;
    Toolbar toolbar;
    LinearLayout llNoInternet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        llNoInternet = (LinearLayout) findViewById(R.id.internetCheck);
        new Handler().post(internetCheck);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Forgot Password");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        Glide.with(ForgotPasswordActivity.this).load(R.drawable.forgot_password_key)
                .apply(new RequestOptions().centerCrop())
                .into((ImageView) findViewById(R.id.ivForgotpasswordKey));

        etForgotEmailID = (EditText) findViewById(R.id.etForgotEmailID);
        btnResetPassword = (Button) findViewById(R.id.btnResetPassword);
        usernameWrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);

        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String forgot_email = etForgotEmailID.getText().toString().trim();

                if (forgot_email.equals("")) {
                    Toast.makeText(getApplicationContext(), "Please enter email address.", Toast.LENGTH_SHORT).show();
                } else if (!isValidEmail(forgot_email)) {
                    usernameWrapper.setError("Not a valid email address!");
                } else {

                    new ForgotPasswordTask().execute(forgot_email);
                }

            }
        });
    }
    Runnable internetCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isOnline = Utility_functions.internetConnected(getApplicationContext());
            if (isOnline) {
                llNoInternet.setVisibility(View.GONE);
            } else {
                llNoInternet.setVisibility(View.VISIBLE);
            }
            new Handler().postDelayed(internetCheck, 5000);
        }
    };
    private class ForgotPasswordTask extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pd == null) {
                pd = new ProgressDialog(ForgotPasswordActivity.this);
                pd.setMessage("loading");
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... args0) {
            try {

                String email = (String) args0[0];

                String link = Webservice.FORGOT_PASSWORD;

                String data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8");
                //data += "&" + URLEncoder.encode("newpass", "UTF-8") + "=" + URLEncoder.encode(new_password, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter
                        (conn.getOutputStream());
                wr.write(data);
                wr.flush();
                BufferedReader reader = new BufferedReader
                        (new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }
                return sb.toString();
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject gg = jobj.getJSONObject("result");
                String msg = gg.getString("msg");
                String msg_string = gg.getString("msg_string");

                if (msg.equals("0")) {
                    Toast.makeText(getApplicationContext(), msg_string, Toast.LENGTH_LONG).show();

                }

                if (msg.equals("1")) {
                    pd.dismiss();
                    Toast.makeText(getApplicationContext(), msg_string, Toast.LENGTH_LONG).show();
                    finish();

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isValidEmail(String s3) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(s3);
        return matcher.matches();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
