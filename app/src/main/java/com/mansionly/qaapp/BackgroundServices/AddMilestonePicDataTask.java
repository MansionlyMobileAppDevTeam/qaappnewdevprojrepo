package com.mansionly.qaapp.BackgroundServices;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.mansionly.qaapp.client.AndroidMultipartEntity;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.localDb.SqliteDatabaseHandler;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

public class AddMilestonePicDataTask extends AsyncTask<Void, Void, String> {
    String db_column_id, actor_user_id, order_id, milestone_plan_id, activity_log_status, section_of_house,
            file_name, customer_viewable;
    SqliteDatabaseHandler sqliteDatabaseHandler;
    Context context;

    AddMilestonePicDataTask(Context context, String db_column_id, String actor_user_id, String order_id, String milestone_plan_id,
                            String activity_log_status, String section_of_house, String file_name,
                            String customer_viewable) {
        this.context = context;
        sqliteDatabaseHandler = new SqliteDatabaseHandler(context);
        this.db_column_id = db_column_id;//used for deleting record froom local db..
        this.actor_user_id = actor_user_id;
        this.order_id = order_id;
        this.milestone_plan_id = milestone_plan_id;
        this.activity_log_status = activity_log_status;
        this.section_of_house = section_of_house;
        this.file_name = file_name;
        this.customer_viewable = customer_viewable;
    }

    protected void onPreExecute() {
        // Toast.makeText(context,file_name,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected String doInBackground(Void... params) {
        String url = Webservice.UPLOAD_MILESTONE_DATA_IMAGES_BACKGROUND;

        String responseString = null;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);

        try {
            AndroidMultipartEntity entity = new AndroidMultipartEntity(
                    new AndroidMultipartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                        }
                    });

            File picFile = new File(file_name);

            entity.addPart(DBTableColumnName.COLUMN_USER_ID, new StringBody(actor_user_id));
            entity.addPart(DBTableColumnName.COLUMN_QA_ORDER_ID, new StringBody(order_id));
            entity.addPart(DBTableColumnName.COLUMN_QA_MILESTONE_PLAN_ID, new StringBody(milestone_plan_id));
            entity.addPart(DBTableColumnName.COLUMN_QA_FILE_NAME, new FileBody(picFile));
            entity.addPart(DBTableColumnName.COLUMN_QA_ACTIVITY_LOG_STATUS, new StringBody(activity_log_status));
            entity.addPart(DBTableColumnName.COLUMN_QA_SECTION_OF_HOUSE, new StringBody(section_of_house));
            entity.addPart(DBTableColumnName.COLUMN_QA_IMAGE_CUSTOMER_VIEWABLE, new StringBody(customer_viewable));

            httppost.setEntity(entity);

            // Making server call
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity r_entity = response.getEntity();

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // Server response
                responseString = EntityUtils.toString(r_entity);
            } else {
                responseString = "Error occurred! Http Status Code: " + statusCode;
            }

        } catch (ClientProtocolException e) {
            responseString = e.toString();
            e.printStackTrace();
        } catch (IOException e) {
            responseString = e.toString();
            e.printStackTrace();
        }
        return responseString;
    }

    protected void onPostExecute(String result) {

        try {
            JSONObject jobj = new JSONObject(result);
            JSONObject jsonResult = jobj.getJSONObject("result");
            String msg = jsonResult.getString("msg");

            if (msg.equals("0")) {
                Toast.makeText(context, "Error while uploading QA Images", Toast.LENGTH_SHORT).show();
            } else if (msg.equals("1")) {
                int len1 = sqliteDatabaseHandler.getTempTableData().length();
                sqliteDatabaseHandler.removeUploadedRecordInTempTable(db_column_id);
                int len2 = sqliteDatabaseHandler.getTempTableData().length();

                //  Toast.makeText(context,"Before "+len1+"  After "+len2,Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}