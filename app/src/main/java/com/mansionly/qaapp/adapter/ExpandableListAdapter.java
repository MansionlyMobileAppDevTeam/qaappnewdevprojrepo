package com.mansionly.qaapp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.mansionly.qaapp.R;
import com.mansionly.qaapp.model.Continent;
import com.mansionly.qaapp.model.QATask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter{

    private Context _context;
    private List<String> header; // expandablelist_header_title titles
    // Child data in format of expandablelist_header_title title, expandablelist_child_layout title
    private HashMap<String, ArrayList<QATask>> child;
    private ArrayList<Continent> mContinentList;
    private ArrayList<Continent> originalList;

    public ExpandableListAdapter(FragmentActivity activity, ArrayList<Continent> continentList) {
        this._context = activity;
        mContinentList = new ArrayList<Continent>();
        mContinentList.addAll(continentList);
        this.originalList = new ArrayList<Continent>();
        this.originalList.addAll(mContinentList);
    }

    @Override
    public int getGroupCount() {
        // Get expandablelist_header_title size
        return mContinentList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // return children count
        ArrayList<QATask> countryList = mContinentList.get(groupPosition).getChildList();
        return countryList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        // Get expandablelist_header_title position
        return mContinentList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        // This will return the expandablelist_child_layout
        ArrayList<QATask> countryList = mContinentList.get(groupPosition).getChildList();
        return countryList.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        // Getting expandablelist_header_title title
        Continent continent = (Continent) getGroup(groupPosition);

        // Inflating expandablelist_header_title layout and setting btn_approved_background
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expandablelist_header_title, parent, false);
        }

        TextView header_text = (TextView) convertView.findViewById(R.id.header);
        header_text.setText(continent.getHeaderName());

        // If group is expanded then change the btn_approved_background into bold and change the
        // icon
        if (isExpanded) {
            header_text.setTypeface(null, Typeface.BOLD);
            header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up, 0);
        } else {
            // If group is not expanded then change the btn_approved_background back into normal
            // and change the icon
            header_text.setTypeface(null, Typeface.NORMAL);
            header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        // Getting expandablelist_child_layout text
        QATask childList = (QATask) getChild(groupPosition, childPosition);

        // Inflating expandablelist_child_layout layout and setting textview
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expandablelist_child_layout, parent, false);

            TextView tvCustomerName=(TextView) convertView.findViewById(R.id.tvCustomerName);
            TextView tvApartmentAddress=(TextView) convertView.findViewById(R.id.tvApartmentAddress);
            TextView tvSectionofHouse=(TextView) convertView.findViewById(R.id.tvSectionofHouse);
            TextView tvPlannedDate=(TextView) convertView.findViewById(R.id.tvPlannedDate);
            TextView tvActivityName=(TextView) convertView.findViewById(R.id.tvActivityName);

            tvCustomerName.setText(childList.getCustomerName());
            tvApartmentAddress.setText(childList.getPropertyAddress());
            tvSectionofHouse.setText(childList.getQASectionOfHouse());
            tvPlannedDate.setText(childList.getQAPlannedDate());
            tvActivityName.setText(childList.getQAActivityName());


        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return true;
    }
    public void filterData(String query) {

        query = query.toLowerCase();
        Log.v("MyListAdapter", String.valueOf(mContinentList.size()));
        mContinentList.clear();

        if (query.isEmpty()) {
            mContinentList.addAll(originalList);
        } else {

            for (Continent continent : originalList) {

                ArrayList<QATask> childList = continent.getChildList();
                ArrayList<QATask> newList = new ArrayList<QATask>();

                for (QATask country : childList) {
                    String activity=country.getQAActivityName().toLowerCase();
                    if (activity.contains(query)) {
                        newList.add(country);
                        if (newList.size() > 0) {
                            String header = continent.getHeaderName();
                            Continent nContinent = new Continent(header, newList);
                            mContinentList.add(nContinent);
                        }
                    }
                }

            }
        }
        notifyDataSetChanged();

    }

}
