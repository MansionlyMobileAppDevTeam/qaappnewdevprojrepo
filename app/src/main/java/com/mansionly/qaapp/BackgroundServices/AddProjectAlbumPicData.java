package com.mansionly.qaapp.BackgroundServices;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.Toast;

import com.mansionly.qaapp.App;
import com.mansionly.qaapp.client.AndroidMultipartEntity;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.localDb.SqliteDatabaseHandler;
import com.mansionly.qaapp.util.Utility_functions;
import com.mansionly.qaapp.view.SiteGallery_Add_Image_Activity;
import com.mansionly.qaapp.view.SiteGallery_Upload_Pic_Detail_Activity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class AddProjectAlbumPicData extends AsyncTask<Void, Void, String> {
    String db_column_id, pa_actor_user_id, pa_project_site_images,
            pa_order_id, pa_qa_image_customer_viewable;
    Context context;
    SqliteDatabaseHandler sqliteDatabaseHandler;

    AddProjectAlbumPicData(Context context, String db_column_id, String pa_actor_user_id, String pa_project_site_images,
                           String pa_order_id, String pa_qa_image_customer_viewable) {

        this.context=context;
        sqliteDatabaseHandler = new SqliteDatabaseHandler(context);
        this.db_column_id = db_column_id; //Used for deleting sqlite record
        this.pa_actor_user_id = pa_actor_user_id;
        this.pa_project_site_images = pa_project_site_images;
        this.pa_order_id = pa_order_id;
        this.pa_qa_image_customer_viewable = pa_qa_image_customer_viewable;
    }

    protected void onPreExecute() {

    }

    @Override
    protected String doInBackground(Void... params) {
        String url = Webservice.PROJECT_ADD_REMOVE_IMAGES;

        String responseString = null;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);

        try {
            AndroidMultipartEntity entity = new AndroidMultipartEntity(
                    new AndroidMultipartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                        }
                    });

            File picFile = new File(pa_project_site_images);

            entity.addPart(DBTableColumnName.USER_ID, new StringBody(pa_actor_user_id));
            entity.addPart(DBTableColumnName.ORDER_ID, new StringBody(pa_order_id));
            entity.addPart(DBTableColumnName.QA_MILESTONE_PIC_ADD_REMOVE_FLAG, new StringBody("addImg"));
            entity.addPart(DBTableColumnName.PROJECT_SITE_IMAGES, new FileBody(picFile));
            entity.addPart(DBTableColumnName.QA_IMAGE_CUSTOMER_VIEWABLE, new StringBody(pa_qa_image_customer_viewable));
            httppost.setEntity(entity);
            // Making server call
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity r_entity = response.getEntity();

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // Server response
                responseString = EntityUtils.toString(r_entity);
            } else {
                responseString = "Error occurred! Http Status Code: " + statusCode;
            }

        } catch (ClientProtocolException e) {
            responseString = e.toString();
            e.printStackTrace();
        } catch (IOException e) {
            responseString = e.toString();
            e.printStackTrace();
        }
        return responseString;
    }

    protected void onPostExecute(String result) {

        try {
            JSONObject jobj = new JSONObject(result);
            JSONObject jsonResult = jobj.getJSONObject("result");
            String msg = jsonResult.getString("msg");

            if (msg.equals("0")) {
                Toast.makeText(context, "Error while uploading album Images", Toast.LENGTH_SHORT).show();

            } else if (msg.equals("1")) {
                sqliteDatabaseHandler.removeUploadedRecordInProjectAlbumTable(db_column_id);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
