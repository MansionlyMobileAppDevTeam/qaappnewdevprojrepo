package com.mansionly.qaapp.view;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.VolleyMultipartRequest;
import com.mansionly.qaapp.client.VolleySingleton;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.CustomDateTimePicker;
import com.mansionly.qaapp.util.Utility_functions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Set_Rejected_Task_Revised_Date_Activity extends AppCompatActivity {
    String mCustomerNameStr, mCustomerMailIdStr, mOidStr,  mUserIdStr, mMilestonePlanIdStr;
    AppPreferences pref;
    ImageView ivRevisedDate;
    TextView tvRevisedDate, tvRejectionDate, tvRejectionBy, tvSetDate , tvCancel,tvAdd_comment_txt;
    EditText etRejectionComment;
    CustomDateTimePicker custom;
    Toolbar toolbar;
    LinearLayout llNoInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rejected_task_add_date_comment_layout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        llNoInternet = (LinearLayout) findViewById(R.id.internetCheck);
        new Handler().post(internetCheck);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Set revised completion date");

        ivRevisedDate = (ImageView) findViewById(R.id.revised_date_imageView);
        tvRevisedDate = (TextView) findViewById(R.id.revised_date_value_textView);
        tvRejectionDate = (TextView) findViewById(R.id.rejection_date_value_textView);
        tvRejectionBy = (TextView) findViewById(R.id.rejection_by_value_textView);
        etRejectionComment = (EditText) findViewById(R.id.rejection_comment_editText);
        tvSetDate = (TextView) findViewById(R.id.set_date_textView);
        tvCancel = (TextView) findViewById(R.id.cancel_textView);
        tvAdd_comment_txt= (TextView) findViewById(R.id.add_comment_txt);

        pref = new AppPreferences(getApplicationContext());
        mUserIdStr = pref.getUserIdPref();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mOidStr = extras.getString(AppConsts.QA_ORDER_ID);
            mMilestonePlanIdStr = extras.getString(AppConsts.QA_MILESTONE_ID);
            mCustomerNameStr = extras.getString(AppConsts.QA_CUSTOMER_NAME);
            mCustomerMailIdStr = extras.getString(AppConsts.QA_CUSTOMER_MAILID);
        }
        etRejectionComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvAdd_comment_txt.setTextColor(Color.parseColor("#00786c"));
                etRejectionComment.setBackgroundResource(R.drawable.background_comment_box);
            }
        });

       // getSupportActionBar().setTitle(mCustomerNameStr);

        tvSetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean cancel = false;

                etRejectionComment.setError(null);
                tvRevisedDate.setError(null);
                String rejectionComment = etRejectionComment.getText().toString();
                String revisedDate = tvRevisedDate.getText().toString();

                if (TextUtils.isEmpty(rejectionComment)) {
                    tvAdd_comment_txt.setTextColor(Color.parseColor("#FF0000"));
                    etRejectionComment.setBackgroundResource(R.drawable.background_comment_error_box);
                    cancel = true;
                }


                if (TextUtils.isEmpty(revisedDate)) {
                    tvRevisedDate.setError(getString(R.string.error_field_required));
                    cancel = true;
                }

                if (!cancel) {
                    SetRejectedTaskDate();
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });

        ivRevisedDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom = new CustomDateTimePicker(Set_Rejected_Task_Revised_Date_Activity.this,
                        new CustomDateTimePicker.ICustomDateTimeListener() {

                            @Override
                            public void onSet(Dialog dialog, Calendar calendarSelected,
                                              Date dateSelected, int year, String monthFullName,
                                              String monthShortName, int monthNumber, int date,
                                              String weekDayFullName, String weekDayShortName,
                                              int hour24, int hour12, int min, int sec,
                                              String AM_PM) {
                                Time chosenDate = new Time();
                                chosenDate.set(sec, min, hour24, date, monthNumber, year);
                                long lDate = chosenDate.toMillis(true);
                                String dateTimeStr = (String) DateFormat.format("dd/MM/yyyy", lDate);
                                tvRevisedDate.setText(dateTimeStr);
                                tvRevisedDate.setError(null);

                            }

                            @Override
                            public void onCancel() {

                            }
                        });

                custom.set24HourFormat(false);
                custom.setDate(Calendar.getInstance());
                custom.showDialog();
            }
        });

        InitView();
    }

    Runnable internetCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isOnline = Utility_functions.internetConnected(getApplicationContext());
            if (isOnline) {
                llNoInternet.setVisibility(View.GONE);
            } else {
                llNoInternet.setVisibility(View.VISIBLE);
            }
            new Handler().postDelayed(internetCheck, 5000);
        }
    };

    private void InitView() {
        tvRejectionDate.setText(pref.getCurrentTaskRejectionDate());
        tvRejectionBy.setText(pref.getCurrentTaskRejectedBy());

    }


    private void SetRejectedTaskDate() {
        String url = Webservice.TASK_STATUS_CHANGE;

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    JSONObject jobj = new JSONObject(resultResponse);
                    JSONObject gg = jobj.getJSONObject("result");
                    String msg = gg.getString("msg");
                    String msg_string = gg.getString("msg_string");

                    if (msg.equalsIgnoreCase("1")) {
                        try {
                            pref.clearMapPref();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(getApplicationContext(), Menu_Activity.class);
                        intent.putExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 2);
                        startActivity(intent);

                    } else {
                        Log.i("Unexpected", msg);
                    }
                } catch (
                        JSONException e)

                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        })

        {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(DBTableColumnName.USER_ID, mUserIdStr);
                params.put(DBTableColumnName.QA_MILESTONE_ORDER_ID, mOidStr);
                params.put(DBTableColumnName.QA_EAILID, mCustomerMailIdStr);
                params.put(DBTableColumnName.QA_MILESTONE_ID, mMilestonePlanIdStr);
                params.put(DBTableColumnName.QA_ACTIVITY_RESULT_STATUS, "0");
                params.put(DBTableColumnName.QA_PLAN_REVISED_DATE, tvRevisedDate.getText().toString());
                params.put(DBTableColumnName.QA_ACTIVITY_RESULT_COMMENT, etRejectionComment.getText().toString());

                return params;
            }
        };

        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext() , Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }
}
