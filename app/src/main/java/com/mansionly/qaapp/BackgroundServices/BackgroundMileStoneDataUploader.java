package com.mansionly.qaapp.BackgroundServices;

/**
 * Created by Pavan Patil on 3/28/2018.
 */

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.mansionly.qaapp.R;
import com.mansionly.qaapp.localDb.SqliteDatabaseHandler;
import com.mansionly.qaapp.util.Utility_functions;
import com.mansionly.qaapp.view.MainActivity;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class BackgroundMileStoneDataUploader extends IntentService {

    public Context context = this;
    public Handler handler = null;
    public static Runnable runnable = null;
    SqliteDatabaseHandler sqliteDatabaseHandler;
    List<String> imagesList = new ArrayList<String>();
    AddMilestonePicDataTask addMilestonePicDataTask;

    public BackgroundMileStoneDataUploader() {
        super("BackgroundMileStoneDataUploader");
    }

    public void runBackgroundUploadTask()
    {
        sqliteDatabaseHandler = new SqliteDatabaseHandler(this);
        if (!Utility_functions.internetConnected(this)) {
            //Toast.makeText(this, "Check internet connectivity", Toast.LENGTH_SHORT).show();
            //stopForeground(true);
        } else {
            if (sqliteDatabaseHandler.getTempTableData().length()>0) {
                int sqliteDataLength=sqliteDatabaseHandler.getTempTableData().length();
                for(int i=0;i<sqliteDataLength;i++)
                {
                    try {
                        //setNotification(sqliteDatabaseHandler.getTempTableData().length(),i+1);
                        new AddMilestonePicDataTask(this,sqliteDatabaseHandler.getTempTableData().getJSONObject(i).getString("id"),
                                sqliteDatabaseHandler.getTempTableData().getJSONObject(i).getString("actor_user_id"),
                                sqliteDatabaseHandler.getTempTableData().getJSONObject(i).getString("order_id"),
                                sqliteDatabaseHandler.getTempTableData().getJSONObject(i).getString("milestone_plan_id"),
                                sqliteDatabaseHandler.getTempTableData().getJSONObject(i).getString("activity_log_status"),
                                sqliteDatabaseHandler.getTempTableData().getJSONObject(i).getString("section_of_house"),
                                sqliteDatabaseHandler.getTempTableData().getJSONObject(i).getString("file_name"),
                                sqliteDatabaseHandler.getTempTableData().getJSONObject(i).getString("flag_customer_viewable"))
                                .execute();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {

                //stopForeground(true);//Hide notification
                stopSelf();
                try {
                    //Delete all temp folder files if no data in sqlite table
                    Utility_functions.DeleteAllUploadedTempMilestonePicFiles();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @Override
    public void onDestroy() {
        /* IF YOU WANT THIS SERVICE KILLED WITH THE APP THEN UNCOMMENT THE FOLLOWING LINE */
       // Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();
        super.onDestroy();

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                runBackgroundUploadTask();
                handler.postDelayed(runnable, 1000);// Run task after every 1 minute
            }
        };
        handler.postDelayed(runnable, 0);
    }

    public void setNotification(int total, int uploading)
    {
        Intent notificationIntent = new Intent(this, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.qa_app)
                .setContentTitle("Mansionly QA")
                .setContentText("Uploading "+uploading+" Of "+total+" Images")
                .setContentIntent(pendingIntent).build();

        startForeground(1337, notification);
    }

    public void clearNotification() {
        stopForeground(true);
    }


}