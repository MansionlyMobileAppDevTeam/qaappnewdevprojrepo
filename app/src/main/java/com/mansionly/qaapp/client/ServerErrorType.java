package com.mansionly.qaapp.client;

public class ServerErrorType {
    public static final int INVALID_USER_ID = 1;
    public static final String SERVER_RESPONSE_PERMISSION_DENIED = "550";
    public static final String SERVER_RESPONSE_INVALID_ACCOUNT = "404";
    public static final int WRONG_PASSWORD = 3;
    public static final int INVALID_SESSION = 6;
    public static final int SERVER_EXCEPTION = 8;
}
