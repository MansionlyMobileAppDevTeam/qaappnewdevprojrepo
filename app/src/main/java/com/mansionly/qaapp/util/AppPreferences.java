package com.mansionly.qaapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class AppPreferences {

    private static final String APP_SHARED_PREFS = "com.mansionly.qaapp"; // Name
    private SharedPreferences appPrefs;
    private Editor prefsEditor;
    private Context mContext;
    Map<String, String> outputMap = new HashMap<String, String>();

    public AppPreferences(Context context) {
        this.mContext = context;
        this.appPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = appPrefs.edit();
    }

    public void saveUserInfo(Map<String, Object> userInfo) {
        String userId = "";
        String userName = "";
        String userType = "";
        String userMailId = "";
        String orderId = "";

        if (userInfo.containsKey("o_id")) {
            orderId = (String) userInfo.get("o_id");
            prefsEditor.putString("o_id", orderId);
        }

        if (userInfo.containsKey(AppConsts.USER_ID)) {
            userId = (String) userInfo.get(AppConsts.USER_ID);
            prefsEditor.putString(AppConsts.USER_NAME, userId);
        }

        if (userInfo.containsKey(AppConsts.USER_NAME)) {
            userName = (String) userInfo.get(AppConsts.USER_NAME);
            prefsEditor.putString(AppConsts.USER_NAME, userName);
        }

        if (userInfo.containsKey(AppConsts.USER_TYPE)) {
            userType = (String) userInfo.get(AppConsts.USER_TYPE);
            prefsEditor.putString(AppConsts.USER_TYPE, userType);
        }

        if (userInfo.containsKey(AppConsts.USER_EMAIL_ID)) {
            userMailId = (String) userInfo.get(AppConsts.USER_EMAIL_ID);
            prefsEditor.putString(AppConsts.USER_EMAIL_ID, userMailId);
        }

        if (userInfo.containsKey(AppConsts.USER_EMAIL_ID)) {
            userMailId = (String) userInfo.get(AppConsts.USER_EMAIL_ID);
            prefsEditor.putString(AppConsts.USER_EMAIL_ID, userMailId);
        }

        prefsEditor.commit();
    }

    public Map<String, Object> getUserInfo() {
        Map<String, Object> custInfo = new HashMap<String, Object>();
        custInfo.put(AppConsts.USER_ID, getUserIdPref());
        custInfo.put(AppConsts.USER_NAME, getUserNamePref());
        custInfo.put(AppConsts.USER_EMAIL_ID, getUserEmail());
        custInfo.put(AppConsts.USER_NAME, getUserNamePref());
        custInfo.put("o_id", getOrderIdPref());

        return custInfo;
    }

    public String getUserIdPref() {
        return appPrefs.getString(AppConsts.USER_ID, (String) "");
    }

    public void saveUserIdPref(String userId) {
        prefsEditor.putString(AppConsts.USER_ID, userId);
        prefsEditor.commit();
    }

    public String getUserNamePref() {
        return appPrefs.getString(AppConsts.USER_NAME, (String) "");
    }

    public String getOrderIdPref() {
        return appPrefs.getString(AppConsts.QA_MILESTONE_ORDER_ID, (String) "");
    }

    public void saveUserNamePref(String userName) {
        prefsEditor.putString(AppConsts.USER_NAME, userName);
        prefsEditor.commit();
    }

    public boolean isUserLoggedIn() {
        return appPrefs.getBoolean(AppConsts.USER_LOGIN, false);
    }

    public void saveUserLoginStatus(boolean loginStatus) {
        prefsEditor.putBoolean(AppConsts.USER_LOGIN, loginStatus);
        prefsEditor.commit();
    }

    public String getUserEmail() {
        return appPrefs.getString(AppConsts.USER_EMAIL_ID, (String) "");
    }

    public void saveUserEnailIdPref(String userEmailId) {
        prefsEditor.putString(AppConsts.USER_EMAIL_ID, userEmailId);
        prefsEditor.commit();
    }

    public void saveUserPrivilage(JSONArray jsonArrPrivilege) {
        final ArrayList<String> privilege = new ArrayList<String>();

        for (int i = 0; i < jsonArrPrivilege.length(); i++) {
            try {
                privilege.add(jsonArrPrivilege.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            prefsEditor.putString(AppConsts.USER_PRIVILEGE, privilege.get(i));
            prefsEditor.commit();
        }
    }


    public void saveOrderIdPref(String o_id) {
        prefsEditor.putString(AppConsts.QA_MILESTONE_ORDER_ID, o_id);
        prefsEditor.commit();
    }

    public void saveQaComment(String comment) {
        prefsEditor.putString(AppConsts.QA_COMMENT, comment);
        prefsEditor.commit();
    }

    public void saveMileStoneId(String milestone_plan_id) {
        prefsEditor.putString(AppConsts.QA_MILESTONE_ID, milestone_plan_id);
        prefsEditor.commit();
    }

    public String getMileStoneId() {
        return appPrefs.getString(AppConsts.QA_MILESTONE_ID, (String) "");
    }

    public void saveQuoteIdpref(String quote_id) {

        prefsEditor.putString(AppConsts.QA_QUOTE_ID, quote_id);
        prefsEditor.commit();

    }

    public String getQuoteIDpref() {
        return appPrefs.getString(AppConsts.QA_QUOTE_ID, (String) "");
    }

    public void saveSectionOfHousepref(String section_of_house) {
        prefsEditor.putString(AppConsts.QA_SECTION_OFHOUSE, section_of_house);
        prefsEditor.commit();
    }

    public void saveActivitypref(String activitystring) {
        prefsEditor.putString(AppConsts.QA_ACTIVITY, activitystring);
        prefsEditor.commit();
    }

    public void saveSellerNameref(String executioner_seller) {
        prefsEditor.putString(AppConsts.QA_SELLER_NAME, executioner_seller);
        prefsEditor.commit();
    }

    public void savePlanDatepref(String plan_completion_date) {
        prefsEditor.putString(AppConsts.QA_PLAN_DATE, plan_completion_date);
        prefsEditor.commit();
    }

    public String getSectionOfHousePref() {
        return appPrefs.getString(AppConsts.QA_SECTION_OFHOUSE, (String) "");
    }

    public String getActivity() {
        return appPrefs.getString(AppConsts.QA_ACTIVITY, (String) "");
    }

    public String getSellerName() {
        return appPrefs.getString(AppConsts.QA_SELLER_NAME, (String) "");
    }

    public String getPlanDate() {
        return appPrefs.getString(AppConsts.QA_PLAN_DATE, (String) "");
    }


    public void saveRejectEditComment(String s) {
        prefsEditor.putString(AppConsts.QA_REJECT_EDIT_COMMENT, s);
        prefsEditor.commit();

    }

    public String getRejectEditCommentpref() {
        return appPrefs.getString(AppConsts.QA_REJECT_EDIT_COMMENT, (String) "");
    }

    public void saveApproveEditComment(String s) {
        prefsEditor.putString(AppConsts.QA_APPROVE_EDIT_COMMENT, s);
        prefsEditor.commit();

    }

    public String getApproveEditCommentpref() {
        return appPrefs.getString(AppConsts.QA_APPROVE_EDIT_COMMENT, (String) "");
    }

    public void saveRevisedDate(String s) {

        prefsEditor.putString(AppConsts.QA_DATE_TIME, s);
        prefsEditor.commit();
    }

    public String getRevisedDateref() {
        return appPrefs.getString(AppConsts.QA_DATE_TIME, (String) "");
    }

    public void saveMilestonePicIdListPref(ArrayList<String> milestoneImageList) {

        Set<String> allImageSet = new HashSet<String>();
        allImageSet.addAll(milestoneImageList);
        prefsEditor.putStringSet(AppConsts.MILESTONE_PIC_ID_LIST, allImageSet);
        prefsEditor.commit();
    }

    public ArrayList<String> getMilestonePicIdListPref() {

        ArrayList<String> milestonePicIdsList = new ArrayList<String>();
        Set<String> milestonePicIdSet = appPrefs.getStringSet(AppConsts.MILESTONE_PIC_ID_LIST, null);
        if (milestonePicIdSet != null)
            milestonePicIdsList = new ArrayList<String>(milestonePicIdSet);

        return milestonePicIdsList;
    }

    public void saveMilestonePicIdCount(int size) {
        prefsEditor.putInt(AppConsts.MILESTONE_PIC_COUNT, size);
        prefsEditor.commit();
    }


    public void saveMilestoneViewCheckMapPref(Map<String, String> inputMap) {
        JSONObject jsonObject = new JSONObject(inputMap);
        String jsonString = jsonObject.toString();
        prefsEditor.putString(AppConsts.MILESTONE_VIEW_CHECK_LIST, jsonString);
        prefsEditor.commit();
    }

    public Map<String, String> getMilestoneViewCheckMapPref() {
        try {
            String jsonString = appPrefs.getString(AppConsts.MILESTONE_VIEW_CHECK_LIST, (new JSONObject()).toString());
            if (jsonString != null) {
                if (!jsonString.isEmpty()) {
                    JSONObject jsonObject = new JSONObject(jsonString);
                    Iterator<String> keysItr = jsonObject.keys();
                    while (keysItr.hasNext()) {
                        String key = keysItr.next();
                        String value = (String) jsonObject.get(key);
                        outputMap.put(key, value);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return outputMap;
    }

    public void Deletekeyxxx(String pic_id) {

        Map<String, String> allEntries = (Map<String, String>) appPrefs.getAll();
        for (Map.Entry<String, String> entry : allEntries.entrySet()) {
            String key = entry.getKey();
            if (key.contains(pic_id)) {
                prefsEditor.remove(key);
            }
            prefsEditor.commit();
        }
    }

    public void saveMilestonePicCommentMapPref(Map<String, String> milestonePicCommentsMap) {

        JSONObject jsonObject = new JSONObject(milestonePicCommentsMap);
        String jsonString = jsonObject.toString();
        prefsEditor.putString(AppConsts.MILESTONE_PIC_COMMENT_LIST, jsonString);
        prefsEditor.commit();
    }

    public Map<String, String> getMilestonePicCommentsMapPref() {
        Map<String, String> outputMap = new HashMap<String, String>();
        try {
            String jsonString = appPrefs.getString(AppConsts.MILESTONE_PIC_COMMENT_LIST, (new JSONObject()).toString());
            if (jsonString != null) {
                {
                    if (!jsonString.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(jsonString);
                        Iterator<String> keysItr = jsonObject.keys();
                        while (keysItr.hasNext()) {
                            String key = keysItr.next();
                            String value = (String) jsonObject.get(key);
                            outputMap.put(key, value);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return outputMap;
    }

    public void saveMilestonePicLabelMapPref(Map<String, String> milestonePicLabelsMap) {

        JSONObject jsonObject = new JSONObject(milestonePicLabelsMap);
        String jsonString = jsonObject.toString();
        prefsEditor.putString(AppConsts.MILESTONE_PIC_LABEL_LIST, jsonString);
        prefsEditor.commit();
    }

    public Map<String, String> getMilestonePicLabelsMapPref() {
        Map<String, String> outputMap = new HashMap<String, String>();
        try {
            String jsonString = appPrefs.getString(AppConsts.MILESTONE_PIC_LABEL_LIST, (new JSONObject()).toString());
            if (jsonString != null) {
                if (!jsonString.isEmpty()) {
                    JSONObject jsonObject = new JSONObject(jsonString);
                    Iterator<String> keysItr = jsonObject.keys();
                    while (keysItr.hasNext()) {
                        String key = keysItr.next();
                        String value = (String) jsonObject.get(key);
                        outputMap.put(key, value);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return outputMap;
    }

    public Map<String, String> getMilestonePicIdMapPref() {

        Map<String, String> outputMap = new HashMap<String, String>();
        try {
            String jsonString = appPrefs.getString(AppConsts.MILESTONE_PIC_ID_LIST, (new JSONObject()).toString());
            if (jsonString != null) {
                if (!jsonString.isEmpty()) {
                    JSONObject jsonObject = new JSONObject(jsonString);
                    Iterator<String> keysItr = jsonObject.keys();
                    while (keysItr.hasNext()) {
                        String key = keysItr.next();
                        String value = (String) jsonObject.get(key);
                        outputMap.put(key, value);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return outputMap;
    }

    public void saveMilestonePicIdMapPref(Map<String, String> milestonePicIdMap) {

        JSONObject jsonObject = new JSONObject(milestonePicIdMap);
        String jsonString = jsonObject.toString();
        prefsEditor.putString(AppConsts.MILESTONE_PIC_ID_LIST, jsonString);
        prefsEditor.commit();
    }

    public void clearMapPref() {
        prefsEditor.remove(AppConsts.MILESTONE_PIC_ID_LIST);
        prefsEditor.remove(AppConsts.MILESTONE_PIC_LABEL_LIST);
        prefsEditor.remove(AppConsts.MILESTONE_PIC_COMMENT_LIST);
        prefsEditor.remove(AppConsts.MILESTONE_VIEW_CHECK_LIST);
        prefsEditor.remove(AppConsts.TASK_LOCAL_PIC_ID);
        prefsEditor.remove(AppConsts.TASK_LOCAL_PIC_COUNT);
        prefsEditor.remove(AppConsts.SERVER_ID_TO_FILE_NAME_LIST);
        prefsEditor.commit();
    }

    public void clearImageCount() {
        prefsEditor.remove(AppConsts.TASK_PIC_COUNT);
        prefsEditor.commit();
    }

    public void saveCurrentTaskType(String currTaskType) {

        prefsEditor.putString(AppConsts.CURRENT_TASK_TYPE, currTaskType);
        prefsEditor.commit();

    }

    public String getCurrentTaskTypePref() {

        return appPrefs.getString(AppConsts.CURRENT_TASK_TYPE, (String) "");
    }

    public void saveCurrentTaskRejectionDate(String currTaskRejectionDate) {
        prefsEditor.putString(AppConsts.TASK_REJECTION_DATE, currTaskRejectionDate);
        prefsEditor.commit();

    }

    public String getCurrentTaskRejectionDate() {
        return appPrefs.getString(AppConsts.TASK_REJECTION_DATE, (String) "");

    }

    public void saveCurrentTaskRejectedBy(String currTaskRejectionDate) {
        prefsEditor.putString(AppConsts.TASK_REJECTED_BY, currTaskRejectionDate);
        prefsEditor.commit();
    }

    public String getCurrentTaskRejectedBy() {
        return appPrefs.getString(AppConsts.TASK_REJECTED_BY, (String) "");
    }


    public void saveTaskPicCount(int taskPicCount) {
        prefsEditor.putInt(AppConsts.TASK_PIC_COUNT, taskPicCount);
        prefsEditor.commit();
    }

    public int getTaskPicCount() {
        return appPrefs.getInt(AppConsts.TASK_PIC_COUNT, 0);

    }

    public void saveListSize(int size) {
        prefsEditor.putInt("list", size);
        prefsEditor.commit();
    }

    public int getListSize() {
        return appPrefs.getInt("list", 0);
    }

    public ArrayList<String> getTaskLocalPicIdPref() {

        int size = appPrefs.getInt(AppConsts.TASK_LOCAL_PIC_COUNT, 0);

        ArrayList<String> taskLocalPicIds = new ArrayList<String>();

        for (int i = 0; i < size; i++) {
            taskLocalPicIds.add(appPrefs.getString(AppConsts.TASK_LOCAL_PIC_ID + i, null));
        }

        return taskLocalPicIds;
    }


    public void saveTaskLocalPicIdPref(ArrayList<String> localPicIds) {

        prefsEditor.putInt(AppConsts.TASK_LOCAL_PIC_COUNT, localPicIds.size());

        for (int i = 0; i < localPicIds.size(); i++) {
            prefsEditor.remove(AppConsts.TASK_LOCAL_PIC_ID + i);
            prefsEditor.putString(AppConsts.TASK_LOCAL_PIC_ID + i, localPicIds.get(i));

        }

        prefsEditor.commit();
        return;

    }


    public void saveUserQASupervisor(String user_profile_qa_supervisor) {
        prefsEditor.putString(AppConsts.USER_PROFILE_QA_SUPERVISOR, user_profile_qa_supervisor);
        prefsEditor.commit();
    }

    public String getUserQASupervisor() {
        return appPrefs.getString(AppConsts.USER_PROFILE_QA_SUPERVISOR, (String) "");
    }

    public void saveCustomerName(String mCustomerNameStr) {
        prefsEditor.putString(AppConsts.QA_CUSTOMER_NAME, mCustomerNameStr);
        prefsEditor.commit();
    }

    public String getCustomerName() {
        return appPrefs.getString(AppConsts.QA_CUSTOMER_NAME, (String) "");
    }

    public HashMap<String, String> getServerIdToFileNameMap() {
        HashMap<String, String> outputMap = new HashMap<>();
        try {
            String jsonString = appPrefs.getString(AppConsts.SERVER_ID_TO_FILE_NAME_LIST,
                    (new JSONObject()).toString());

            if (jsonString != null) {
                if (!jsonString.isEmpty()) {
                    JSONObject jsonObject = new JSONObject(jsonString);
                    Iterator<String> keysItr = jsonObject.keys();
                    while (keysItr.hasNext()) {
                        String key = keysItr.next();
                        String value = (String) jsonObject.get(key);
                        outputMap.put(key, value);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputMap;
    }

    public void saveServerIdToFileNameMap(HashMap<String, String> serverIdToFileNameMap) {
        JSONObject jsonObject = new JSONObject(serverIdToFileNameMap);
        String jsonString = jsonObject.toString();
        prefsEditor.putString(AppConsts.SERVER_ID_TO_FILE_NAME_LIST, jsonString);
        prefsEditor.commit();
    }

    public void saveFlagInternalWork(String flagInternalWork) {
        prefsEditor.putString("flagInternalWork", flagInternalWork);
        prefsEditor.commit();
    }
    public String getFlagInternalWork() {
        return appPrefs.getString("flagInternalWork","");

    }

    public void saveSectionOfHouseIDpref(String section_of_house) {
        prefsEditor.putString(AppConsts.QA_SECTION_OFHOUSE_ID, section_of_house);
        prefsEditor.commit();
    }

    public String getSectionOfHouseIDPref() {
        return appPrefs.getString(AppConsts.QA_SECTION_OFHOUSE_ID, (String) "");
    }
}

