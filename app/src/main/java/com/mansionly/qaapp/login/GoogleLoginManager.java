package com.mansionly.qaapp.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.mansionly.qaapp.Constants;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Shraddha on 9/1/18.
 */

public class GoogleLoginManager {
    private Activity mActivity;
    private GoogleSignInClient mGoogleSignInClient;

    public GoogleLoginManager(Activity mActivity) {
        this.mActivity = mActivity;
        init();
    }

    private void init() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(mActivity,
                googleSignInOptions);

    }

    public void startLogin() {
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(mActivity);
        if (account != null) {
            //user is already logged in.
            onSignInComplete(account);
            return;
        }
        //start google sign in.
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        mActivity.startActivityForResult(signInIntent, Constants.RequestCode.GOOGLE_SIGN_IN);
    }

    /**
     * Called when google sign in is successful via google popup.
     * We need to hit our backend after this.
     */
    private void onSignInComplete(GoogleSignInAccount googleSignInAccount) {
        String token = googleSignInAccount.getId();
        String email = googleSignInAccount.getEmail();

        new LoginTask().execute(token, email);
    }

    public class LoginTask extends AsyncTask<String, String, String> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pd == null) {
                pd = new ProgressDialog(mActivity);
                pd.setMessage("Loading...");
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... args0) {
            try {
                String token = (String) args0[0];
                String email = (String) args0[1];

                String link = Webservice.SOCIAL_LOGIN;

                String data = "&" + URLEncoder.encode("gmail_token_id", "UTF-8") + "=" +
                        URLEncoder.encode(token, "UTF-8");
                data += "&" + URLEncoder.encode("email", "UTF-8") + "=" +
                        URLEncoder.encode(email, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter
                        (conn.getOutputStream());
                wr.write(data);
                wr.flush();
                BufferedReader reader = new BufferedReader
                        (new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }
                return sb.toString();
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("result", result);
            pd.dismiss();
            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject gg = jobj.getJSONObject("result");
                String msg = gg.getString("msg");

                if (msg.equals("1")) {
                    onSignInSuccess(gg);
                } else if (msg.equals("0")) {
                    Toast toast = Toast.makeText(mActivity, "Google sign in failed.", Toast.LENGTH_LONG);
                    toast.show();
                }
                pd.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void onSignInSuccess(JSONObject result) {
        storeUserDetails(result);
        Intent intent = new Intent(mActivity, MainActivity.class);
        mActivity.startActivity(intent);
        mActivity.finish();
    }

    private void storeUserDetails(JSONObject result) {
        try {
            String userid = result.getString(DBTableColumnName.USER_ID);
            String userType = result.getString(DBTableColumnName.USER_TYPE);
            String userName = result.getString(DBTableColumnName.USER_NAME);
            String userEmailId = result.getString(DBTableColumnName.USER_EMAIL_ID);
            JSONArray privilegeArray = result.getJSONArray(DBTableColumnName.USER_PRIVILEGE);

            Map<String, Object> userInfo = new HashMap<String, Object>();
            userInfo.put(AppConsts.USER_ID, userid);
            userInfo.put(AppConsts.USER_NAME, userName);
            userInfo.put(AppConsts.USER_TYPE, userType);
            userInfo.put(AppConsts.USER_EMAIL_ID, userEmailId);

            AppPreferences prefs = new AppPreferences(mActivity);
            prefs.saveUserLoginStatus(true);
            prefs.saveUserInfo(userInfo);
            prefs.saveUserIdPref(userid);
            prefs.saveUserEnailIdPref(userEmailId);
            prefs.saveUserPrivilage(privilegeArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(Intent data) {
        Task<GoogleSignInAccount> completedTask = GoogleSignIn.getSignedInAccountFromIntent(data);
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            onSignInComplete(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Toast toast = Toast.makeText(mActivity, "Google sign in failed.", Toast.LENGTH_LONG);
            toast.show();
        }
    }

}
