package com.mansionly.qaapp.client;

public class ServerResponseType {

    public static final String SERVER_RESPONSE_RESULT = "result";

    public static final String SERVER_RESPONSE_MSG = "msg";
    public static final String SERVER_RESPONSE_MSG_DETAIL = "msg_string";

    public static final String SERVER_RESPONSE_SUCCESS = "1";
    public static final String SERVER_RESPONSE_FAILURE = "0";

    public static final String SERVER_RESPONSE_VERSION_CODE = "versioncode";


}
