package com.mansionly.qaapp.view;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;

import java.util.ArrayList;
import java.util.List;

public class Project_Album_Activity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    TabsPagerAdapter adapter;
    Toolbar toolbar;
    String mOidStr, mCustomerNameStr, mUseridStr;
    AppPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_album_tabview);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        adapter = new TabsPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        mOidStr = extras.getString(AppConsts.QA_ORDER_ID);

        pref = new AppPreferences(getApplicationContext());
        mUseridStr = pref.getUserIdPref();
        mOidStr = pref.getOrderIdPref();
        mCustomerNameStr = pref.getCustomerName();
        getSupportActionBar().setTitle(mCustomerNameStr);

        createViewPager(viewPager);
        viewPager.setOffscreenPageLimit(2);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        if (viewPager != null) {
            tabLayout.setupWithViewPager(viewPager);
        }

        int tabIndex = getIntent().getIntExtra(AppConsts.CURRENT_TAB_INDEX, 0);

        TabLayout.Tab tab = tabLayout.getTabAt(tabIndex);
        if (tab != null)
            tab.select();

        tabLayout.setupWithViewPager(viewPager);

        setTabText();
    }


    private void setTabText() {

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        TextView tabOne = (TextView) LayoutInflater.from(getApplicationContext()).inflate(R.layout.custom_tab, null);
        TextView tabTwo = (TextView) LayoutInflater.from(getApplicationContext()).inflate(R.layout.custom_tab, null);

        tabOne.setText("SITE GALLERY");
        tabTwo.setText("QA ALBUM");

        switch (metrics.densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                tabOne.setTextSize(11);
                tabTwo.setTextSize(11);
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                tabOne.setTextSize(11);
                tabTwo.setTextSize(11);
                break;
            case DisplayMetrics.DENSITY_HIGH:
                tabOne.setTextSize(12);
                tabTwo.setTextSize(12);
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                tabOne.setTextSize(12);
                tabTwo.setTextSize(12);
                break;
            case DisplayMetrics.DENSITY_560:
                tabOne.setTextSize(16);
                tabTwo.setTextSize(16);
                break;
        }

        tabLayout.getTabAt(0).setCustomView(tabOne);
        tabLayout.getTabAt(1).setCustomView(tabTwo);
    }


    private void createViewPager(ViewPager viewPager) {
        adapter.addFrag(new Site_Gallery_Fragment(), "SITE GALLERY");
        adapter.addFrag(new QA_Album_Fragment(), "QA ALBUM");
        viewPager.setAdapter(adapter);
    }

    class TabsPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public TabsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(getApplicationContext(), Menu_Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

            intent.putExtra(AppConsts.CURRENT_TAB_INDEX, 3);
            intent.putExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 3);

            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), Menu_Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra(AppConsts.CURRENT_TAB_INDEX, 3);
        intent.putExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 3);

        startActivity(intent);
    }
    @Override
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext() , Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }
}
