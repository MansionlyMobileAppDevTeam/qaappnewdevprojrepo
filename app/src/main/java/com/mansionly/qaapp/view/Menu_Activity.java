package com.mansionly.qaapp.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.Utility_functions;

import java.io.File;

import com.mansionly.qaapp.R;

public class Menu_Activity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    FrameLayout frame;
    Fragment fragment;
    FragmentTransaction fragmentTransaction;
    public static int navItemIndex = 0;
    // tags used to attach the fragments
    private static final String TAG_MY_ACTIVITIES = "myAcivities";
    private static final String TAG_ALL_ACTIVITIES = "allActivities";
    private static final String TAG_REJECTED_ACTIVITIES = "rejectedActivities";
    private static final String TAG_PROJECT_ALBUM = "projectAlbum";

    public static String CURRENT_TAG = TAG_MY_ACTIVITIES;
    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;
    // flag to load home fragment when user presses back key
    private Handler mHandler;
    FragmentManager myFragmentManager;
    private View navHeader;
    private NavigationView navigationView;
    TextView tv_email, tv_name, tvVersion;
    AppPreferences pref ;
    LinearLayout llNoInternet;
    String mUserQASupervisor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext(), Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        llNoInternet = (LinearLayout) findViewById(R.id.internetCheck);
        new Handler().post(internetCheck);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        frame = (FrameLayout) findViewById(R.id.frame);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        ImageView coverImage = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.coverImage);
        Glide.with(Menu_Activity.this).load(R.drawable.header_image)
                .apply(new RequestOptions().centerCrop())
                .into(coverImage);

        mHandler = new Handler();
        myFragmentManager = getSupportFragmentManager();

        pref = new AppPreferences(getApplicationContext());
        mUserQASupervisor = pref.getUserQASupervisor();

        if (mUserQASupervisor.equalsIgnoreCase("0"))
            hideItem();

        navHeader = navigationView.getHeaderView(0);
        tv_name = (TextView) navHeader.findViewById(R.id.tv_name);
        tv_email = (TextView) navHeader.findViewById(R.id.tv_email);
        tvVersion = (TextView) navHeader.findViewById(R.id.tvVersion);

        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_drawer_items);
        // load nav menu expandablelist_header_title data
        loadNavHeader();
        // initializing navigation menu
        setUpNavigationView();

        navItemIndex = getIntent().getIntExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 0);

        loadHomeFragment();


    }

    private void hideItem() {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.rejected_activities).setVisible(false);
    }

    Runnable internetCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isOnline = Utility_functions.internetConnected(getApplicationContext());
            if (isOnline) {
                llNoInternet.setVisibility(View.GONE);
            } else {
                llNoInternet.setVisibility(View.VISIBLE);
            }
            new Handler().postDelayed(internetCheck, 5000);
        }
    };

    private void loadNavHeader() {

        tv_name.setText(pref.getUserNamePref());
        tv_email.setText(pref.getUserEmail());
        tvVersion.setText(getAppVersionName(Menu_Activity.this));
    }

    public static String getAppVersionName(Context context) {
        String version = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "Version "+version;
    }
    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.my_activities:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_MY_ACTIVITIES;
                        break;
                    case R.id.all_activities:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_ALL_ACTIVITIES;
                        break;
                    case R.id.rejected_activities:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_REJECTED_ACTIVITIES;
                        break;
                    case R.id.project_album:
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_PROJECT_ALBUM;
                        break;
                    default:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_MY_ACTIVITIES;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }


            @Override
            public void onDrawerStateChanged(int newState) {
                // this where Drawer start opening
                if (newState == DrawerLayout.STATE_SETTLING && !drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputMethodManager.isAcceptingText())
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();


    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawerLayout.closeDrawers();
            // show or hide the fab button
            return;
        }

        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        //Closing drawer on item click
        drawerLayout.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        Fragment fragment = null;
        switch (navItemIndex) {
            case 0:
                fragment = new My_Tasks_Fragment();
                break;
            case 1:
                fragment = new All_tasks_fragments();
                break;
            case 2:
                fragment = new Rejected_Tasks_Fragment();
                break;
            case 3:
                fragment = new Project_Album_Fragment();
                break;

        }

        return fragment;
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void DeleteTempPicFile() throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp");

        File fList[] = picFolderInSDCard.listFiles();

        for (int i = 0; i < fList.length; i++) {
            fList[i].delete();
        }
        return;
    }

    @Override
    public void onBackPressed() {

        if (navItemIndex == 0) {
            new AlertDialog.Builder(this)
                    .setTitle("Really Exit?")
                    .setMessage("Are you sure you want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            try {
                                Menu_Activity.super.onBackPressed();
                                moveTaskToBack(true);
                                System.exit(0);
                                DeleteTempPicFile();
                                pref.saveTaskPicCount(0);
                                pref.clearMapPref();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }).create().show();
        }
        else {
            navItemIndex = 0;
            CURRENT_TAG = TAG_MY_ACTIVITIES;
            loadHomeFragment();
            //getSupportFragmentManager().popBackStack();
        }
    }
}