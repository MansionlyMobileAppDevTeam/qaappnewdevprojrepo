package com.mansionly.qaapp.util;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.mansionly.qaapp.Constants;
import com.mansionly.qaapp.App;
import com.mansionly.qaapp.activity.CameraActivity;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by root on 4/3/17.
 */

public class MediaCaptureHelper implements ActivityCompat.OnRequestPermissionsResultCallback {
    private Activity mActivity;
    private ImageView targetView;
    private String imagePath;
    private ArrayList<OnCaptureListener> listenerArrayList;

    public interface OnCaptureListener {
        void onCapture();
    }

    public void addOnCaptureListener(OnCaptureListener onCaptureListener) {
        listenerArrayList.add(onCaptureListener);
    }

    public MediaCaptureHelper(Activity mActivity, ImageView targetView) {
        this.mActivity = mActivity;
        this.targetView = targetView;
        this.listenerArrayList = new ArrayList<>();
    }

    private boolean hasPermissions() {
        if ((ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) !=
                PackageManager.PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED)) {
            return false;
        }
        return true;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(mActivity,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                Constants.CAMERA_PERMISSION_REQUEST);
    }

    private void startCamera() {
        mActivity.startActivity(new Intent(mActivity, CameraActivity.class));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.CAMERA_PERMISSION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startCamera();
                } else {
                    Toast.makeText(mActivity, "Permission denied by user.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    public void useNewImage(Bitmap bitmap) {
        clearCache();
        targetView.setImageBitmap(bitmap);
    }

    public void clearCache() {
        App.setCurrentImage(null);
        String filePath = App.getCapturedImageAbsolutePath();
        if (filePath == null) {
            return;
        }
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
    }

    public void setMediaCaptureStartTrigger(View mediaCaptureStartTrigger) {
        mediaCaptureStartTrigger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!hasPermissions()) {
                    requestPermissions();
                    return;
                }
                if (listenerArrayList != null && !listenerArrayList.isEmpty()) {
                    for (OnCaptureListener onCaptureListener : listenerArrayList) {
                        onCaptureListener.onCapture();
                    }
                }
                startCamera();
            }
        });
    }


}
