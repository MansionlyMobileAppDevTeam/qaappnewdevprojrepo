package com.mansionly.qaapp.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.model.Milestone;
import com.mansionly.qaapp.model.Milestone_history;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.CustomScrollView;
import com.mansionly.qaapp.util.Utility_functions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.mansionly.qaapp.R.id.linearLayout2;

public class Rejected_Task_Detail_activity extends AppCompatActivity {

    String mCustomerNameStr, mCustomerMailIdStr, mOidStr, mDetailsTypeStr, mUserIdStr, mMasgStr, mMilestonePlanIdStr,
            mOrderIdStr, mQuoteIdStr, mWorkOrderIdStr, mSectionOfHouseStr, mExecSellernameStr, mPropertyAddressStr, mQAactivityName,
            mUniqueOidStr, mHistoryDetailsTypeStr;
    Toolbar toolbar;
    TextView tvWokOrderID, tvApartmentAddress, tvSellerName, tvSectionofHouse, btnSetRevisedDate, btnApprove, tvActivityName;
    static ArrayList<Milestone> taskList;
    static Milestone milestone;
    Milestone_history milestone_history;
    ProgressBar progressBar;
    AppPreferences pref;
    ArrayList<Milestone_history> milestoneHistoryList;
    String mPicCommentStr, mPiclabelStr, flag_customer_viewable, file_name, bigImg, milestone_plan_log_id, mPicIdStr;
    boolean flag = true;
    ArrayList<String> milestoneBgImagePathList = new ArrayList<String>();
    ArrayList<String> milestonePicIDList = new ArrayList<String>();
    ArrayList<String> milestoneCommentList = new ArrayList<String>();
    ArrayList<String> milestoneLabelList = new ArrayList<String>();
    ArrayList<String> milestoneCustomerViewList = new ArrayList<String>();
    ArrayList<String> milestonePicsFilePathList = new ArrayList<>();
    LinearLayout llTaskHistoryDetail;
    TableLayout tlHistoryPics;
    ArrayList<Uri> milestonePicUriList;
    View hiddenInfo2;
    LinearLayout llNoInternet;
    CustomScrollView scrollview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rejected_task_detail_layout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        llNoInternet = (LinearLayout) findViewById(R.id.internetCheck);
        new Handler().post(internetCheck);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        tvWokOrderID = (TextView) findViewById(R.id.tvWokOrderID);
        tvApartmentAddress = (TextView) findViewById(R.id.tvApartmentAddress);
        tvSellerName = (TextView) findViewById(R.id.tvSellerName);
        tvSectionofHouse = (TextView) findViewById(R.id.tvSectionofHouse);
        tvActivityName = (TextView) findViewById(R.id.tvActivityName);
        btnSetRevisedDate = (TextView) findViewById(R.id.btnReject);
        btnApprove = (TextView) findViewById(R.id.btnApprove);


        taskList = new ArrayList<>();
        milestoneHistoryList = new ArrayList<Milestone_history>();
        milestonePicUriList = new ArrayList<>();

        pref = new AppPreferences(getApplicationContext());
        mUserIdStr = pref.getUserIdPref();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mOidStr = extras.getString(AppConsts.QA_ORDER_ID);
            mMilestonePlanIdStr = extras.getString(AppConsts.QA_MILESTONE_ID);
            mCustomerNameStr = extras.getString(AppConsts.QA_CUSTOMER_NAME);
            mCustomerMailIdStr = extras.getString(AppConsts.QA_CUSTOMER_MAILID);
            mPropertyAddressStr = extras.getString(AppConsts.QA_PROPERTY_ADDRESS);

        }

        mDetailsTypeStr = "milestonePlan";
        mHistoryDetailsTypeStr = "orderMilestonePlanActivityHistory";

        getSupportActionBar().setTitle(mCustomerNameStr);
        new ActionLogListing().execute();

        new QaHistoryLogListing().execute();
    }

    Runnable internetCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isOnline = Utility_functions.internetConnected(getApplicationContext());
            if (isOnline) {
                llNoInternet.setVisibility(View.GONE);
            } else {
                llNoInternet.setVisibility(View.VISIBLE);
            }
            new Handler().postDelayed(internetCheck, 5000);
        }
    };

    public class ActionLogListing extends AsyncTask<String, String, String> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pd == null) {
                pd = new ProgressDialog(Rejected_Task_Detail_activity.this);
                pd.setMessage("Loading...");
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... args0) {
            try {
                String link = Webservice.ORDERDETAILS;
                String data = URLEncoder.encode(DBTableColumnName.USER_ID, "UTF-8") + "=" + URLEncoder.encode(mUserIdStr, "UTF-8");
                data += "&" + URLEncoder.encode(DBTableColumnName.QA_MILESTONE_ORDER_ID, "UTF-8") + "=" + URLEncoder.encode(mOidStr, "UTF-8");
                data += "&" + URLEncoder.encode("details_type", "UTF-8") + "=" + URLEncoder.encode(mDetailsTypeStr, "UTF-8");
                data += "&" + URLEncoder.encode(DBTableColumnName.QA_MILESTONE_ID, "UTF-8") + "=" + URLEncoder.encode(mMilestonePlanIdStr, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter
                        (conn.getOutputStream());
                wr.write(data);
                wr.flush();
                BufferedReader reader = new BufferedReader
                        (new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }
                return sb.toString();
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("result", result);

            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject gg = jobj.getJSONObject("result");
                mMasgStr = gg.getString("msg");

                if (mMasgStr.equals("0")) {
                    Toast.makeText(getApplicationContext(), (String) "msg : 1",
                            Toast.LENGTH_LONG).show();

                } else if (mMasgStr.equals("1")) {
                    taskList.clear();

                    mUniqueOidStr = gg.getString("unique_oid");
                    mOidStr = gg.getString("o_id");
                    mCustomerNameStr = gg.getString("customer_name");
                    mPropertyAddressStr = gg.getString("customer_address");

                    JSONArray jarray = gg.getJSONArray("arrMilestonePlanDtls");

                    try {

                        for (int i = 0; i < jarray.length(); i++) {
                            JSONObject c = jarray.getJSONObject(i);
                            mOrderIdStr = c.getString("order_id");
                            mQuoteIdStr = c.getString("quote_id");
                            mWorkOrderIdStr = c.getString("unique_workorder_id");
                            mMilestonePlanIdStr = c.getString("milestone_plan_id");
                            mSectionOfHouseStr = c.getString("section_of_house_title");
                            mExecSellernameStr = c.getString("executioner_seller_name");
                            mQAactivityName = c.getString("activity");

                            milestone = new Milestone(mOrderIdStr, mQuoteIdStr, mMilestonePlanIdStr, mWorkOrderIdStr, mSectionOfHouseStr, mExecSellernameStr, mQAactivityName);
                            taskList.add(milestone);
                        }

                        milestone = taskList.get(0);
                        tvSellerName.setText(milestone.getExecutioner_seller_name());
                        tvWokOrderID.setText("[" + mUniqueOidStr + "]");
                        if (mPropertyAddressStr.equalsIgnoreCase("null") ||
                                mPropertyAddressStr.trim().isEmpty())
                            tvApartmentAddress.setText("No address available");
                        else
                            tvApartmentAddress.setText(mPropertyAddressStr);
                        tvSectionofHouse.setText(milestone.getSection_house());
                        tvActivityName.setText(milestone.getActivity());

                        pref.saveQuoteIdpref(milestone.getQuote_id());
                        pref.saveMileStoneId(milestone.getMilestonePlanId());

                        btnApprove.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(getApplicationContext(), Approve_Rejected_Task_Activity.class);
                                intent.putExtra(AppConsts.QA_ORDER_ID, mOidStr);
                                intent.putExtra(AppConsts.QA_MILESTONE_ID, mMilestonePlanIdStr);
                                intent.putExtra(AppConsts.QA_CUSTOMER_EAILID, mCustomerMailIdStr);
                                intent.putExtra("activity_log_status", "0");
                                startActivity(intent);


                            }
                        });

                        btnSetRevisedDate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Rejected_Task_Detail_activity.this, Set_Rejected_Task_Revised_Date_Activity.class);
                                //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intent.putExtra(AppConsts.QA_ORDER_ID, mOidStr);
                                intent.putExtra(AppConsts.QA_MILESTONE_ID, mMilestonePlanIdStr);
                                intent.putExtra(DBTableColumnName.QA_SECTION_OF_HOUSE, milestone.getSection_house());
                                intent.putExtra(DBTableColumnName.QA_ACTIVITY_NAME, milestone.getActivity());
                                intent.putExtra(AppConsts.QA_CUSTOMER_EAILID, mCustomerMailIdStr);
                                intent.putExtra("activity_log_status", "0");
                                startActivity(intent);
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // progressBar.setVisibility(View.GONE);
                    pd.dismiss();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class QaHistoryLogListing extends AsyncTask<String, String, String> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pd == null) {
                pd = new ProgressDialog(Rejected_Task_Detail_activity.this);
                pd.setMessage("Loading...");
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... args0) {

            try {

                String link = Webservice.HISTORY_LOG;

                String data = URLEncoder.encode(DBTableColumnName.USER_ID, "UTF-8") + "=" + URLEncoder.encode(mUserIdStr, "UTF-8");
                data += "&" + URLEncoder.encode(DBTableColumnName.QA_MILESTONE_ORDER_ID, "UTF-8") + "=" + URLEncoder.encode(mOidStr, "UTF-8");
                data += "&" + URLEncoder.encode("details_type", "UTF-8") + "=" + URLEncoder.encode(mHistoryDetailsTypeStr, "UTF-8");
                data += "&" + URLEncoder.encode(DBTableColumnName.QA_MILESTONE_ID, "UTF-8") + "=" + URLEncoder.encode(mMilestonePlanIdStr, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter
                        (conn.getOutputStream());
                wr.write(data);
                wr.flush();
                BufferedReader reader = new BufferedReader
                        (new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }
                return sb.toString();
            } catch (Exception e) {
                e.printStackTrace();

                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("result", result);
            pd.dismiss();
            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject gg = jobj.getJSONObject("result");
                mMasgStr = gg.getString("msg");
                String msg_string = gg.getString("msg_string");

                if (mMasgStr.equals("0")) {
                    //Toast.makeText(getApplicationContext(), msg_string, Toast.LENGTH_SHORT).show();

                } else if (mMasgStr.equals("1")) {
                    milestoneHistoryList.clear();

                    try {

                        mUniqueOidStr = gg.getString("unique_oid");
                        JSONArray jarray = gg.getJSONArray("arrQAOrderHistoryLogs");

                        try {

                            for (int i = 0; i < jarray.length(); i++) {
                                milestoneBgImagePathList.clear();
                                milestonePicIDList.clear();
                                milestoneCommentList.clear();
                                milestoneLabelList.clear();
                                milestoneCustomerViewList.clear();
                                milestonePicsFilePathList.clear();
                                JSONObject jsonObject = jarray.getJSONObject(i);

                                String order_id = jsonObject.getString("order_id");
                                String quote_id = jsonObject.getString("quote_id");
                                String milestone_plan_id = jsonObject.getString("milestone_plan_id");
                                String milestone_plan_log_id = jsonObject.getString("milestone_plan_log_id");
                                String activity = jsonObject.getString("activity");
                                String qa_comment_log = jsonObject.getString("qa_comment_log");
                                String qa_status_log = jsonObject.getString("qa_status_log");
                                String action_by_user_name = jsonObject.getString("action_by_user_name");
                                String on_date = jsonObject.getString("on_date");
                                String plan_revised_date = jsonObject.getString("plan_revised_date");

                                JSONArray milestonePicsJSONArray = jsonObject.getJSONArray(DBTableColumnName.QA_ACTIVITY_RESULT_PICS);

                                if (milestonePicsJSONArray != null) {

                                    for (int ii = 0; ii < milestonePicsJSONArray.length(); ii++) {
                                        JSONObject Object = milestonePicsJSONArray.getJSONObject(ii);

                                        file_name = Object.getString("file_name");
                                        milestonePicsFilePathList.add(file_name);

                                        bigImg = Object.getString("bigImg");
                                        milestoneBgImagePathList.add(bigImg);

                                        mPicIdStr = Object.getString("picId");
                                        mPicCommentStr = Object.getString("picComment");
                                        mPiclabelStr = Object.getString("picLable");
                                        flag_customer_viewable = Object.getString("flag_customer_viewable");

                                        milestonePicIDList.add(mPicIdStr);
                                        milestoneCommentList.add(mPicCommentStr);
                                        milestoneLabelList.add(mPiclabelStr);
                                        milestoneCustomerViewList.add(flag_customer_viewable);

                                    }
                                }

                                milestone_history = new Milestone_history(order_id, quote_id, milestone_plan_id, milestone_plan_log_id,
                                        activity, qa_status_log, qa_comment_log, milestonePicsFilePathList, milestoneBgImagePathList, milestoneCommentList, milestoneLabelList, milestoneCustomerViewList, milestonePicIDList, action_by_user_name, on_date, plan_revised_date);
                                milestoneHistoryList.add(milestone_history);

                                setHistoryData(i);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                        pd.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setHistoryData(int pos) {
        final Milestone_history milestoneHistDetail = milestoneHistoryList.get(pos);
        final LinearLayout myLayout = (LinearLayout) findViewById(linearLayout2);
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View hiddenInfo = inflater.inflate(R.layout.layout_milestone_history_item, null);
        myLayout.addView(hiddenInfo);

        final TextView tvPerformedBy = (TextView) hiddenInfo.findViewById(R.id.tvPerformedBy);
        final TextView onDate = (TextView) hiddenInfo.findViewById(R.id.onDate);
        final TextView onMonthYear = (TextView) hiddenInfo.findViewById(R.id.onMonthYear);
        final String display_status = milestoneHistDetail.getDisplay_status();

        String dtStart = milestoneHistDetail.getOnDate();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        try {
            date = format.parse(dtStart);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String day = (String) DateFormat.format("dd", date);
        String month = (String) DateFormat.format("MMM", date);
        String year = (String) DateFormat.format("yy", date);

        onDate.setText(day);
        onMonthYear.setText(month + "'" + year);

        if (milestoneHistDetail.getPlan_revised_date().equalsIgnoreCase("")) {
            if (display_status.equalsIgnoreCase("Rejected")) {
                tvPerformedBy.setBackgroundResource(R.drawable.btn_rejected_background);
            }
            tvPerformedBy.setText(milestoneHistDetail.getDisplay_status() + " " + "by" + " " + milestoneHistDetail.getActionByUserName());
        } else {
            if (display_status.equalsIgnoreCase("Rejected")) {
                tvPerformedBy.setBackgroundResource(R.drawable.btn_rejected_background);
            }
            tvPerformedBy.setText("Completion Date set" + " " + "by" + " " + milestoneHistDetail.getActionByUserName());
        }
        tvPerformedBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout myLayout2 = (LinearLayout) hiddenInfo.findViewById(R.id.linearLayout);
                LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                if (myLayout2.getChildCount() == 0) {

                    tvPerformedBy.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up, 0);
                    hiddenInfo2 = inflater.inflate(R.layout.layout_history_item_images, null);
                    myLayout2.addView(hiddenInfo2);
                    tlHistoryPics = (TableLayout) hiddenInfo2.findViewById(R.id.task_history_pics_tablelayout);
                    llTaskHistoryDetail = (LinearLayout) hiddenInfo2.findViewById(R.id.task_history_detail_linearlayout);
                    llTaskHistoryDetail.setVisibility(View.VISIBLE);

                    TextView tvTaskRevisedDate = (TextView) hiddenInfo2.findViewById(R.id.task_revised_date);
                    TextView tvActivityName2 = (TextView) hiddenInfo2.findViewById(R.id.task_history_detail_textview);
                    String activity_name = milestoneHistDetail.getComment();
                    tvActivityName2.setText(activity_name);
                    if (!milestoneHistDetail.getPlan_revised_date().equalsIgnoreCase("")) {
                        tvTaskRevisedDate.setVisibility(View.VISIBLE);
                        tvTaskRevisedDate.setText("Revised Completion Date : " + milestoneHistDetail.getPlan_revised_date());
                    }

                    new DisplayMilestonePicsTask(milestoneHistDetail.getMilestonePics()).execute();
                    milestonePicIDList = milestoneHistDetail.getMilestonePicIDList();
                    milestoneCommentList = milestoneHistDetail.getMilestoneCommentList();
                    milestoneLabelList = milestoneHistDetail.getMilestoneLabelList();
                    milestoneCustomerViewList = milestoneHistDetail.getMilestoneCheckView();

                } else {
                    tvPerformedBy.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);
                    myLayout2.removeAllViews();
                    milestonePicUriList.clear();

                    flag = true;
                }

            }
        });
    }

    private class DisplayMilestonePicsTask extends AsyncTask<Void, Void, Void> {
        ArrayList<String> mHttpUrlList;
        ArrayList<Uri> milestonePicUriList = new ArrayList<>();

        public DisplayMilestonePicsTask(ArrayList<String> milestonePics) {
            this.mHttpUrlList = milestonePics;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int ii = 0; ii < mHttpUrlList.size(); ii++) {
                String filepath = mHttpUrlList.get(ii);
                milestonePicUriList.add(Uri.parse(filepath));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param) {

            int total = milestonePicUriList.size();

            ArrayList<ImageView> imageViewList = new ArrayList<ImageView>();

            DisplayMetrics dMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
            int width = dMetrics.widthPixels;
            int margin = Utility_functions.dpToPixel(getApplicationContext(), 40);
            int dimen = (width - margin) / 3;

            int padding = Utility_functions.dpToPixel(getApplicationContext(), 5);


            for (int k = 0; k < total; k++) {

                Uri oneUri = milestonePicUriList.get(k);

                ImageView oImageView = new ImageView(getApplicationContext());
                oImageView.setLayoutParams(new TableRow.LayoutParams(dimen, dimen));
                Glide.with(Rejected_Task_Detail_activity.this).load(oneUri)
                        .apply(new RequestOptions().centerInside())
                        .into(oImageView);
                oImageView.setPadding(padding, padding, padding, padding);
                imageViewList.add(oImageView);

                final int finalK = k;
                oImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String picId = milestonePicIDList.get(finalK);
                        String comment = milestoneCommentList.get(finalK);
                        String label = milestoneLabelList.get(finalK);
                        String customerView = milestoneCustomerViewList.get(finalK);

                        Uri tempUri = milestonePicUriList.get(finalK);

                        Intent intent = new Intent(Rejected_Task_Detail_activity.this,
                                Display_Reject_Task_pic_detail_activity.class);
                        intent.putExtra("image", tempUri.toString());
                        intent.putExtra("picId", picId);
                        intent.putExtra("image_position", finalK);
                        intent.putExtra("comment", comment);
                        intent.putExtra("label", label);
                        intent.putExtra("viewableCheck", customerView);
                        intent.putExtra("flag", "read_Only");
                        startActivity(intent);
                    }
                });
            }


            int row = (total % 3 == 0) ? total / 3 : (total / 3) + 1;

            ArrayList<TableRow> trHistoryPicsList = new ArrayList<TableRow>();

            for (int l = 0; l < row; l++) {
                TableRow trHistoryPics = new TableRow(getApplicationContext());
                trHistoryPicsList.add(trHistoryPics);
            }

            for (int kk = 1; kk <= 2; kk++) {
                tlHistoryPics.setColumnShrinkable(kk, true);
            }
            tlHistoryPics.setGravity(Gravity.CENTER);

            if (total > 0) {
                if (total < 3) {

                    trHistoryPicsList.get(0).setLayoutParams(new TableRow.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

                    for (int j = 0; j < total; j++) {
                        trHistoryPicsList.get(0).addView(imageViewList.get(j));
                    }
                    tlHistoryPics.setColumnShrinkable(1, true);
                    tlHistoryPics.setGravity(Gravity.LEFT);
                    tlHistoryPics.addView(trHistoryPicsList.get(0));

                } else {

                    int lastRowCount = total % 3;

                    for (int i = 0; i < row; i++) {

                        if (lastRowCount == 0) {
                            for (int j = 0; j <= 2; j++) {
                                trHistoryPicsList.get(i).addView(imageViewList.get(j));

                            }
                            trHistoryPicsList.get(i).setLayoutParams(new TableRow.LayoutParams(android.widget.TableRow.LayoutParams.WRAP_CONTENT, android.widget.TableRow.LayoutParams.WRAP_CONTENT, 1f));
                            tlHistoryPics.addView(trHistoryPicsList.get(i), new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

                        } else {
                            if (i < row - 1) {
                                for (int j = i * 3; j < i * 3 + 3; j++) {
                                    trHistoryPicsList.get(i).addView(imageViewList.get(j));
                                }

                                trHistoryPicsList.get(i).setLayoutParams(new TableRow.LayoutParams(android.widget.TableRow.LayoutParams.WRAP_CONTENT, android.widget.TableRow.LayoutParams.WRAP_CONTENT, 1f));
                                tlHistoryPics.addView(trHistoryPicsList.get(i), new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

                            } else {
                                for (int j = (row - 1) * 3; j < (row - 1) * 3 + lastRowCount; j++) {
                                    trHistoryPicsList.get(i).addView(imageViewList.get(j));

                                }
                                trHistoryPicsList.get(i).setLayoutParams(new TableRow.LayoutParams(android.widget.TableRow.LayoutParams.WRAP_CONTENT, android.widget.TableRow.LayoutParams.WRAP_CONTENT, 1f));
                                tlHistoryPics.addView(trHistoryPicsList.get(i), new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                            }
                        }
                    }
                    // tlHistoryPics.setGravity(Gravity.CENTER);

                }
            }
        }
    }

    private Bitmap getResizedBitmap(Bitmap bitmap, int h, int w) {

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float scaleWidth = ((float) w) / width;
        float scaleHeight = ((float) h) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, false);

        return resizedBitmap;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext() , Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }
}
