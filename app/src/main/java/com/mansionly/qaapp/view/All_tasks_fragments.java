package com.mansionly.qaapp.view;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.mansionly.qaapp.R;
import com.mansionly.qaapp.util.AppConsts;


public class All_tasks_fragments extends Fragment {
    View rootView;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    TabsPagerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_allactivities_tabview, container, false);
        setHasOptionsMenu(true);

        adapter = new TabsPagerAdapter(getChildFragmentManager());
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        createViewPager(viewPager);
        viewPager.setOffscreenPageLimit(2);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        if (viewPager != null) {
            tabLayout.setupWithViewPager(viewPager);
        }
        int tabIndex = getActivity().getIntent().getIntExtra(AppConsts.CURRENT_TAB_INDEX, 0);

        TabLayout.Tab tab = tabLayout.getTabAt(tabIndex);
        if (tab != null)
            tab.select();
        tabLayout.setupWithViewPager(viewPager);

        setTabText();
        return rootView;
    }

    private void setTabText() {

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab_alltask, null);
        TextView tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab_alltask, null);
        TextView tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab_alltask, null);

        tabOne.setText("TODAY");
        tabTwo.setText("UPCOMING");
        tabThree.setText("HISTORY");

        switch (metrics.densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                tabOne.setTextSize(10);
                tabTwo.setTextSize(10);
                tabThree.setTextSize(10);
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                tabOne.setTextSize(10);
                tabTwo.setTextSize(10);
                tabThree.setTextSize(10);
                break;
            case DisplayMetrics.DENSITY_HIGH:
                tabOne.setTextSize(11);
                tabTwo.setTextSize(11);
                tabThree.setTextSize(11);
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                tabOne.setTextSize(12);
                tabTwo.setTextSize(12);
                tabThree.setTextSize(12);
                break;
            case DisplayMetrics.DENSITY_560:
                tabOne.setTextSize(16);
                tabTwo.setTextSize(16);
                tabThree.setTextSize(16);
                break;
        }

        tabLayout.getTabAt(0).setCustomView(tabOne);
        tabLayout.getTabAt(1).setCustomView(tabTwo);
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }

    private void createViewPager(ViewPager viewPager) {
        adapter.addFrag(new All_Today_Tasks_Fragment(), "TODAY");
        adapter.addFrag(new All_Upcoming_Task_Fragment(), "UPCOMING");
        adapter.addFrag(new All_Task_History_Fragment(), "HISTORY");
        viewPager.setAdapter(adapter);
    }

    class TabsPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public TabsPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}

