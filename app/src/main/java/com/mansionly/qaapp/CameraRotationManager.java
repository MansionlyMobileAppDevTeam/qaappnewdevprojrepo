package com.mansionly.qaapp;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.mansionly.qaapp.util.Utility_functions;

/**
 * Created by root on 26/11/17.
 */

public class CameraRotationManager implements SensorEventListener {

    private static final long SENSOR_MIN_DELAY_MILLIS = 500;

    private Context mContext;
    private OnOrientationChangedListener onOrientationChangedListener;
    private SensorManager sensorManager;
    private Sensor accelerometer, magnetometer;
    private static boolean sensorAvailable;
    private long lastSensorEpoch;
    private float[] mGravity, mGeoMagnetic, R, I;

    public void setOnOrientationChangedListener(
            OnOrientationChangedListener onOrientationChangedListener) {
        this.onOrientationChangedListener = onOrientationChangedListener;
    }

    public interface OnOrientationChangedListener {
        void orientationChanged(double degrees);
    }

    public CameraRotationManager(Context mContext) {
        this.mContext = mContext;
        this.sensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        this.accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        this.lastSensorEpoch = 0;
        R = new float[9];
        I = new float[9];
        if (accelerometer != null && magnetometer != null) {
            sensorAvailable = true;
        } else {
            sensorAvailable = false;
        }
    }

    public void startListening() {
        if (!sensorAvailable) {
            return;
        }
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stopListening() {
        if (!sensorAvailable) {
            return;
        }
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (onOrientationChangedListener == null) {
            return;
        }
        long epoch = System.currentTimeMillis();
        if ((System.currentTimeMillis() - lastSensorEpoch) < SENSOR_MIN_DELAY_MILLIS) {
            return;
        }
        lastSensorEpoch = epoch;

        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            mGravity = sensorEvent.values;

            double aX = sensorEvent.values[0];
            double aY = sensorEvent.values[1];
            //aZ= event.values[2];
            double angle = Math.atan2(aX, aY) / (Math.PI / 180);
            onOrientationChangedListener.orientationChanged(angle);
        }

//        if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
//            mGeoMagnetic = sensorEvent.values;
//        if (mGravity != null && mGeoMagnetic != null) {
//            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeoMagnetic);
//            if (success) {
//                float orientation[] = new float[3];
//                SensorManager.getOrientation(R, orientation);
//                float azimuth = orientation[2]; // orientation contains: azimut, pitch and roll
//                onOrientationChangedListener.orientationChanged(Math.toDegrees(Double.parseDouble(azimuth + "")));
//            }
//        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
