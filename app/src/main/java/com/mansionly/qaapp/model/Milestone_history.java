package com.mansionly.qaapp.model;

import java.util.ArrayList;


public class Milestone_history {

    String mMilestonePlanId;
    String mMilestoneplanlogid;
    String mExecutioner_seller_name;
    String mOrderId;
    String mSectionOfHouse;
    String mQAActivityName;
    String mPlanCompletionDate;
    String mQAStatus;
    String mQAComment;
    String mPlanReviseDate;
    String mStatus;
    String quote_id;
    String status,mCommentDate;
    ArrayList<String> mMilestonePics;
    ArrayList<String> mMilestoneBgImagePathList;
    ArrayList<String> mMilestonePicIDList;
    ArrayList<String> mMilestoneCommentList;
    ArrayList<String> mMilestoneLabelList;
    ArrayList<String> mMilestoneCustomerViewList;
    String mActionbyUserName,mOnDate;

    public Milestone_history(String order_id, String quote_id, String milestone_plan_id, String milestone_plan_log_id, String activity, String qa_status_log, String qa_comment_log,
                             ArrayList<String> milestonePics, ArrayList<String> milestoneBgImagePathList, ArrayList<String> milestoneCommentList, ArrayList<String> milestoneLabelList, ArrayList<String> customerView, ArrayList<String> milestonePicIDList, String action_by_user_name, String on_date, String plan_revised_date) {

        this.mOrderId = order_id;
        this.quote_id = quote_id;
        this.mMilestonePlanId = milestone_plan_id;
        this.mMilestoneplanlogid = milestone_plan_log_id;
        this.mQAActivityName = activity;
        this.mQAStatus = qa_status_log;
        this.mQAComment = qa_comment_log;
        this.mStatus = status;
        this.mActionbyUserName=action_by_user_name;
        this.mOnDate = on_date;
        this.mPlanReviseDate = plan_revised_date;

        mMilestonePics = new ArrayList<String>();
        mMilestoneBgImagePathList= new ArrayList<String>();
        mMilestonePicIDList= new ArrayList<String>();
        mMilestoneCommentList= new ArrayList<String>();
        mMilestoneLabelList= new ArrayList<String>();
        mMilestoneCustomerViewList= new ArrayList<String>();

        for (int ii = 0; ii < milestonePics.size(); ii++) {
            mMilestonePics.add(milestonePics.get(ii));
        }

        for (int ii = 0; ii < milestoneBgImagePathList.size(); ii++) {
            mMilestoneBgImagePathList.add(milestoneBgImagePathList.get(ii));
        }

        for (int ii = 0; ii < milestonePicIDList.size(); ii++) {
            mMilestonePicIDList.add(milestonePicIDList.get(ii));
        }

        for (int ii = 0; ii < milestoneCommentList.size(); ii++) {
            mMilestoneCommentList.add(milestoneCommentList.get(ii));
        }

        for (int ii = 0; ii < milestoneLabelList.size(); ii++) {
            mMilestoneLabelList.add(milestoneLabelList.get(ii));
        }
        for (int ii = 0; ii < customerView.size(); ii++) {
            mMilestoneCustomerViewList.add(customerView.get(ii));
        }

    }
    public String getCommentDate() {
        return mCommentDate;
    }

    public String getMilestone_plan_id() {
        return mMilestonePlanId;
    }

    public String getMilestone_plan_log_id() {
        return mMilestoneplanlogid;
    }

    public String getExecutioner_seller_name() {
        return mExecutioner_seller_name;
    }

    public String getActivityName() {
        return mQAActivityName;
    }

    public String getPlanCompletionDate() {
        return mPlanCompletionDate;
    }

    public String getStatus() {
        return status;
    }

    public String getSection_house() {
        return mSectionOfHouse;
    }

    public String getDisplay_status() {
        return mQAStatus;
    }

    public String getComment() {
        return mQAComment;
    }

    public String getPlan_revised_date() {
        return mPlanReviseDate;
    }

    public ArrayList<String> getMilestonePics() {
        return mMilestonePics;
    }

    public ArrayList<String> getBigImagePics() {
        return mMilestoneBgImagePathList;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public ArrayList<String> getMilestonePicIDList() {
        return mMilestonePicIDList;
    }


    public ArrayList<String> getMilestoneCommentList() {
        return mMilestoneCommentList;
    }

    public ArrayList<String> getMilestoneLabelList() {
        return mMilestoneLabelList;
    }

    public String getActionByUserName() {
        return mActionbyUserName;
    }

    public String getOnDate() {
        return mOnDate;
    }

    public ArrayList<String> getMilestoneCheckView() {
        return mMilestoneCustomerViewList;
    }
}
