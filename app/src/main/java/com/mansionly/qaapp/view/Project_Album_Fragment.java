package com.mansionly.qaapp.view;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.AndroidMultipartEntity;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.model.ProjectAlbum;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.CustomScrollView;
import com.mansionly.qaapp.util.Utility_functions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.mansionly.qaapp.R.color.red;

public class Project_Album_Fragment extends Fragment {
    String mMsgStr, mUserIdStr, mOidStr, mEmailIdStr, mWorkOidStr, mUniqueOidStr, mApartmentStr = "", mNameStr = "", mProjectCreationDateStr = "", mLobTitleStr = "",
            offset_start, mOrderTypeStr = "", mTotalRecordsStr = "", mProjectLiatOffsetStr = "", mLimitStr = "", mMilestonePlanIdStr = "", mQaActivityName = "", mQASectionOfHouseStr = "", mExecSellerName = "";
    String sortBy;
    String searchText = "";
    View rootView;
    RecyclerView mRecyclerView;
    CustomScrollView scrollview;
    ImageView empty_text;
    AppPreferences pref;
    boolean pullToRefresh = false;
    long totalSize = 0;
    ArrayList<ProjectAlbum> mProjectList = new ArrayList<>();
    TodayTaskAdpater mAdapter;
    String selectedMenuItem = "QADate";
    boolean ischecked = true;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container1, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.project_album_list, container1, false);
        setHasOptionsMenu(true);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setNestedScrollingEnabled(false);
        scrollview = (CustomScrollView) rootView.findViewById(R.id.lead_details_scrollview);
        scrollview.setScrollViewListener(leadDetailsScrollViewListener);
        empty_text = (ImageView) rootView.findViewById(R.id.empty_text);

        pref = new AppPreferences(getActivity());
        mUserIdStr = pref.getUserIdPref();
        mEmailIdStr = pref.getUserEmail();
        mProjectLiatOffsetStr = "0";
        mLimitStr = "0";
        sortBy = "DATE";

        new UpdateListing(true).execute();

        return rootView;
    }

    CustomScrollView.ScrollViewListener leadDetailsScrollViewListener = new CustomScrollView.ScrollViewListener() {

        @Override
        public void onScrollChanged(CustomScrollView scrollView, int currLen, int currTop, int prevLen, int prevTop) {
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);

            int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (diff == 0) {
                new UpdateListing(false).execute();
            }
        }
    };

    private class UpdateListing extends AsyncTask<String, Integer, String> {
        private ProgressDialog pd;
        private boolean showLoader;

        public UpdateListing(boolean showLoader) {
            this.showLoader = showLoader;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            if (pullToRefresh == true) {
                pd.dismiss();
                pullToRefresh = false;
            } else {
                if (showLoader) {
                    pd.show();
                }
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String responseString = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Webservice.GET_PROJECT_LIST);

            try {
                AndroidMultipartEntity entity = new AndroidMultipartEntity(
                        new AndroidMultipartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                // mProjectLiatOffsetStr = params[0];
                entity.addPart(AppConsts.QA_USER_ID, new StringBody(mUserIdStr));
                entity.addPart(AppConsts.QA_OFFSET, new StringBody(mProjectLiatOffsetStr));
                entity.addPart(AppConsts.QA_LIMIT, new StringBody(mLimitStr));
                entity.addPart(AppConsts.QA_TASK_SORT_BY, new StringBody(sortBy));
                entity.addPart(AppConsts.QA_TASK_SEARCH, new StringBody(searchText));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();

                responseString = e.toString();
            } catch (IOException e) {
                e.printStackTrace();
                responseString = e.toString();
            }
            return responseString;
        }


        @Override
        protected void onPostExecute(String result) {
            try {

                JSONObject jobj = new JSONObject(result);
                JSONObject gg = jobj.getJSONObject("result");
                mMsgStr = gg.getString(DBTableColumnName.DB_QUERY_NUM);
                String msg_string = gg.getString(DBTableColumnName.DB_QUERY_RESULT);

                if (mMsgStr.equals("0")) {
                    if (mProjectList.isEmpty()) {
                        if (mAdapter != null)
                            mAdapter.clear();
                        empty_text.setVisibility(View.VISIBLE);
                        // swipeRefreshLayout.setEnabled(false);
                    } else if (mProjectList.size() == 0) {
                        empty_text.setVisibility(View.GONE);
                    }

                } else if (mMsgStr.equals("1")) {
                    empty_text.setVisibility(View.GONE);
                    if (mProjectLiatOffsetStr.equalsIgnoreCase("0"))
                        mProjectList.clear();

                    try {

                        JSONArray jarray = gg.getJSONArray("arr_record_list");
                        mProjectLiatOffsetStr = gg.getString("offset");

                        for (int i = 0; i < jarray.length(); i++) {
                            JSONObject c = jarray.getJSONObject(i);

                            mOidStr = c.getString(DBTableColumnName.QA_MILESTONE_ORDER_ID);
                            mUniqueOidStr = c.getString(DBTableColumnName.QA_UNIQUE_ORDER_ID);
                            mApartmentStr = c.getString(DBTableColumnName.QA_APARTMENT);
                            mNameStr = c.getString(DBTableColumnName.QA_NAME);
                            mProjectCreationDateStr = c.getString(DBTableColumnName.PROJECT_CREATION_DATE);
                            mLobTitleStr = c.getString(DBTableColumnName.QA_LOBTITLE);

                            ProjectAlbum projectAlbum = new ProjectAlbum(mOidStr, mUniqueOidStr, mApartmentStr, mNameStr, mProjectCreationDateStr, mLobTitleStr);
                            mProjectList.add(projectAlbum);
                            // swipeRefreshLayout.setRefreshing(false);

                        }

                        if (mAdapter == null) {
                            mAdapter = new TodayTaskAdpater(mProjectList);
                        }
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        mRecyclerView.setLayoutManager(mLayoutManager);
                        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                } else if (mMsgStr.equals("550")) {
                    Toast.makeText(getActivity(), "550", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }
            pd.dismiss();
        }
    }

    public class TodayTaskAdpater extends RecyclerView.Adapter<TodayTaskAdpater.MyViewHolder> {

        private List<ProjectAlbum> projectAlbumList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tvApartmentAddress, tvCustomerName, tvProjectCreationDate, tvprojectCreationDateVal;
            ImageView imMilestoneImage;

            public MyViewHolder(View rootView) {
                super(rootView);
                tvCustomerName = (TextView) rootView.findViewById(R.id.tvCustomerName);
                tvApartmentAddress = (TextView) rootView.findViewById(R.id.tvApartmentAddress);
                tvProjectCreationDate = (TextView) rootView.findViewById(R.id.tvProjectCreationDate);
                tvprojectCreationDateVal = (TextView) rootView.findViewById(R.id.creation_date_textview);
                imMilestoneImage = (ImageView) rootView.findViewById(R.id.imMilestoneImage);

            }
        }

        public TodayTaskAdpater(List<ProjectAlbum> milestonesList) {
            this.projectAlbumList = milestonesList;
            if (getActivity() != null) {
                getActivity().invalidateOptionsMenu();
            }
        }

        @Override
        public TodayTaskAdpater.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.project_album_list_item, parent, false);

            return new TodayTaskAdpater.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final TodayTaskAdpater.MyViewHolder holder, final int position) {

            final ProjectAlbum projectAlbum = projectAlbumList.get(position);

            if (projectAlbum.getPropertyAddress().equalsIgnoreCase("") ||
                    projectAlbum.getPropertyAddress().trim().isEmpty())
                holder.tvApartmentAddress.setText("No address available");
            else
                holder.tvApartmentAddress.setText(projectAlbum.getPropertyAddress());

            holder.tvCustomerName.setText(projectAlbum.getCustomerName());
            if (!projectAlbum.getProjectCreationDate().equalsIgnoreCase(""))
                holder.tvProjectCreationDate.setText(Utility_functions.GetFormatedDate(projectAlbum.getProjectCreationDate()));
            else
                holder.tvProjectCreationDate.setText("No date available");

            String lobTitle = projectAlbum.getQALobTitle();

            if (lobTitle == null)
                holder.imMilestoneImage.setImageResource(R.mipmap.ic_residential);
            else if (lobTitle.equalsIgnoreCase("Institutional"))
                holder.imMilestoneImage.setImageResource(R.drawable.instituional);
            else if (lobTitle.equalsIgnoreCase("Project"))
                holder.imMilestoneImage.setImageResource(R.mipmap.ic_residential);
            else holder.imMilestoneImage.setImageResource(R.mipmap.ic_residential);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pref.saveOrderIdPref(projectAlbum.getOrderId());
                    pref.saveCustomerName(projectAlbum.getCustomerName());
                    Intent intent = new Intent(getActivity(), Project_Album_Activity.class);
                    intent.putExtra(AppConsts.QA_ORDER_ID, projectAlbum.getOrderId());
                    intent.putExtra(AppConsts.QA_PROPERTY_ADDRESS, projectAlbum.getPropertyAddress());
                    intent.putExtra(AppConsts.QA_CUSTOMER_NAME, projectAlbum.getCustomerName());

                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return projectAlbumList.size();
        }

        public void clear() {
            // TODO Auto-generated method stub
            projectAlbumList.clear();
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        SpannableString s;
        menu.findItem(R.id.Title).setEnabled(false);

        if (mProjectList.size() == 0) {
            menu.findItem(R.id.search_menuItem).setVisible(false);
            menu.findItem(R.id.sort_menuItem).setVisible(false);
        }
        if (selectedMenuItem.equals("CustomerName")) {
            MenuItem miCustName = menu.findItem(R.id.SortByCustomerName);
            String miCustNameStr = miCustName.getTitle().toString();
            s = new SpannableString(miCustNameStr);
            s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), 0, s.length(), 0);
            miCustName.setTitle(s);
        } else {
            MenuItem miQADate = menu.findItem(R.id.SortByQADate);
            String miQADateStr = miQADate.getTitle().toString();
            s = new SpannableString(miQADateStr);
            s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), 0, s.length(), 0);
            miQADate.setTitle(s);
        }

        return;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.project_album_sort_menu, menu);

        final MenuItem item = menu.findItem(R.id.search_menuItem);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(taskSearchLstener);
        searchView.setOnCloseListener(searchCloseListener);
        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        mProjectList.clear();
                        searchText = "";
                        mProjectLiatOffsetStr = "0";
                        new UpdateListing(true).execute();
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();

        } else if (item.getItemId() == R.id.SortByCustomerName) {
            if (ischecked == true) {
                selectedMenuItem = "CustomerName";
                mProjectLiatOffsetStr = "0";
                new UpdateListing(true).execute();
                sortBy = "NAME";
                scrollview.scrollTo(0, View.FOCUS_UP);
                ischecked = false;
            }

        } else if (item.getItemId() == R.id.SortByQADate) {
            if (ischecked == false) {
                selectedMenuItem = "QADate";
                mProjectLiatOffsetStr = "0";
                new UpdateListing(true).execute();
                sortBy = "DATE";
                scrollview.scrollTo(0, View.FOCUS_UP);
                ischecked = true;
            }

        }

        ActivityCompat.invalidateOptionsMenu(getActivity());
        return super.onOptionsItemSelected(item);
    }


    SearchView.OnCloseListener searchCloseListener = new SearchView.OnCloseListener() {
        @Override
        public boolean onClose() {
            mProjectList.clear();
            searchText = "";
            mProjectLiatOffsetStr = "0";
            new UpdateListing(true).execute();
            return false;
        }
    };


    SearchView.OnQueryTextListener taskSearchLstener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            mProjectList.clear();
            query = query.toLowerCase();
            searchText = query;
            mProjectLiatOffsetStr = "0";
            new UpdateListing(true).execute();

            InputMethodManager inputMethodManager = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }

    };

}
