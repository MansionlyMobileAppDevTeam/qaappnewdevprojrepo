package com.mansionly.qaapp.view;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.AndroidMultipartEntity;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.Utility_functions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Site_GalleryImage_RemoveUpdate_Activity extends AppCompatActivity {
    AppPreferences pref;
    LinearLayout llNoInternet;
    Toolbar toolbar;
    String mCustomerNameStr, picFilePath, pic_id, customerViewableStatus;
    ImageView ivMilestonePic;
    CheckBox checkBox;
    String viewableCheck;
    String mUseridStr;
    String mO_idStr;
    int mImagePositionStr;
    TextView btnRemoveImage, btnUpdate;
    File picFile;
    Map<String, String> milestoneViewableCheckMap = new HashMap<String, String>();
    Map<String, String> milestonePicIdMap = new HashMap<String, String>();
    ArrayList<String> milestoneLocalPicIdList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sitegallery_image_removeupdate);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        llNoInternet = (LinearLayout) findViewById(R.id.internetCheck);
        new Handler().post(internetCheck);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnRemoveImage = (TextView) findViewById(R.id.btnRemoveImage);
        btnUpdate = (TextView) findViewById(R.id.btnUpdate);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        pref = new AppPreferences(getApplicationContext());
        mUseridStr = pref.getUserIdPref();
        mO_idStr = pref.getOrderIdPref();
        mCustomerNameStr = pref.getCustomerName();
        milestonePicIdMap = pref.getMilestonePicIdMapPref();
        milestoneViewableCheckMap = pref.getMilestoneViewCheckMapPref();
        milestoneLocalPicIdList = pref.getTaskLocalPicIdPref();

        getSupportActionBar().setTitle(mCustomerNameStr);
        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + "SITE GALLERY IMAGES" + "</small>"));

        ivMilestonePic = (ImageView) findViewById(R.id.milestone_pic_imageView);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        Uri image = Uri.parse(getIntent().getExtras().getString("image"));
        //viewableCheck = getIntent().getExtras().getString("viewableCheck");
       // pic_id = getIntent().getExtras().getString("pic_id");
        mImagePositionStr = getIntent().getExtras().getInt("image_position");

        Glide.with(this).load(image).into(ivMilestonePic);

        if (viewableCheck != null) {
            if (viewableCheck.equalsIgnoreCase("1")) {
                checkBox.setText("Customer Viewable");
                checkBox.setChecked(true);
            } else {
                checkBox.setText("Customer Viewable");
                checkBox.setChecked(false);
            }
        }

        btnRemoveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder taskRejectCancelAlertDlg = new AlertDialog.Builder(Site_GalleryImage_RemoveUpdate_Activity.this);
                taskRejectCancelAlertDlg.setTitle("Do you want to remove this image?");
                taskRejectCancelAlertDlg.setCancelable(true);

                taskRejectCancelAlertDlg.setPositiveButton(
                        "NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                // finish();
                            }
                        });

                taskRejectCancelAlertDlg.setNegativeButton(
                        "REMOVE",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SiteGallery_Add_Image_Activity.setImagePosition(mImagePositionStr);
                                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                pref.saveTaskPicCount(pref.getTaskPicCount() - 1);
                                finish();

                            }
                        });

                taskRejectCancelAlertDlg.create().show();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBox.isChecked())
                    customerViewableStatus = "1";
                else
                    customerViewableStatus = "0";
                new UpdateSiteGalleryImageDetails().execute();
            }
        });

    }

    public class RemoveMilestonePicDataTask extends AsyncTask<Void, Void, String> {
        ProgressDialog asyncDialog = new ProgressDialog(Site_GalleryImage_RemoveUpdate_Activity.this);

        protected void onPreExecute() {
            asyncDialog.setMessage("Uploading...");
            asyncDialog.setCancelable(false);
            asyncDialog.setCanceledOnTouchOutside(false);
            //show dialog
            asyncDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String url = Webservice.PROJECT_ADD_REMOVE_IMAGES;

            String responseString = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            try {
                AndroidMultipartEntity entity = new AndroidMultipartEntity(
                        new AndroidMultipartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                            }
                        });

                entity.addPart(DBTableColumnName.USER_ID, new StringBody(mUseridStr));
                entity.addPart(DBTableColumnName.ORDER_ID, new StringBody(mO_idStr));
                entity.addPart(DBTableColumnName.QA_MILESTONE_PIC_ADD_REMOVE_FLAG, new StringBody("delImg"));
                if (pic_id == null)
                    entity.addPart(DBTableColumnName.QA_ACTIVITY_PIC_IDS, new StringBody(""));
                else
                    entity.addPart(DBTableColumnName.QA_ACTIVITY_PIC_IDS, new StringBody(pic_id));

                httppost.setEntity(entity);
                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
                e.printStackTrace();
            } catch (IOException e) {
                responseString = e.toString();
                e.printStackTrace();
            }
            return responseString;
        }

        protected void onPostExecute(String result) {
            asyncDialog.dismiss();

            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject jsonResult = jobj.getJSONObject("result");
                String msg = jsonResult.getString("msg");

                if (msg.equals("0")) {

                } else if (msg.equals("1")) {
                    try {
                        deleteTempMilestonePicFile();
                        milestoneViewableCheckMap.remove(pic_id);
                        milestonePicIdMap.remove(milestoneLocalPicIdList.get(mImagePositionStr));
                        milestoneLocalPicIdList.remove(milestoneLocalPicIdList.get(mImagePositionStr));

                        pref.saveMilestoneViewCheckMapPref(milestoneViewableCheckMap);
                        pref.saveMilestonePicIdMapPref(milestonePicIdMap);
                        pref.saveTaskLocalPicIdPref(milestoneLocalPicIdList);

                        Intent intent = new Intent(Site_GalleryImage_RemoveUpdate_Activity.this, SiteGallery_Add_Image_Activity.class);
                        intent.putExtra("o_id", mO_idStr);
                        intent.putExtra("userid", mUseridStr);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class UpdateSiteGalleryImageDetails extends AsyncTask<Void, Void, String> {
        ProgressDialog asyncDialog = new ProgressDialog(Site_GalleryImage_RemoveUpdate_Activity.this);

        protected void onPreExecute() {
            asyncDialog.setMessage("Uploading...");
            asyncDialog.setCancelable(false);
            asyncDialog.setCanceledOnTouchOutside(false);
            //show dialog
            asyncDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String url = Webservice.PROJECT_IMAGES_UPDATE_DETAILS;

            String responseString = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            try {
                AndroidMultipartEntity entity = new AndroidMultipartEntity(
                        new AndroidMultipartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                            }
                        });

                entity.addPart(DBTableColumnName.USER_ID, new StringBody(mUseridStr));
                entity.addPart(DBTableColumnName.ORDER_ID, new StringBody(mO_idStr));
                if (pic_id == null)
                    entity.addPart(DBTableColumnName.QA_ACTIVITY_PIC_IDS, new StringBody(""));
                else
                    entity.addPart(DBTableColumnName.QA_ACTIVITY_PIC_IDS, new StringBody(pic_id));
                entity.addPart(DBTableColumnName.QA_IMAGE_CUSTOMER_VIEWABLE, new StringBody(customerViewableStatus));


                httppost.setEntity(entity);
                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
                e.printStackTrace();
            } catch (IOException e) {
                responseString = e.toString();
                e.printStackTrace();
            }
            return responseString;
        }

        protected void onPostExecute(String result) {
            asyncDialog.dismiss();

            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject jsonResult = jobj.getJSONObject("result");
                String msg = jsonResult.getString("msg");

                if (msg.equals("0")) {

                } else if (msg.equals("1")) {
                    try {
                        milestoneViewableCheckMap.put(pic_id, customerViewableStatus);
                        pref.saveMilestoneViewCheckMapPref(milestoneViewableCheckMap);

                        Intent intent = new Intent(Site_GalleryImage_RemoveUpdate_Activity.this, SiteGallery_Add_Image_Activity.class);
                        intent.putExtra("o_id", mO_idStr);
                        intent.putExtra("userid", mUseridStr);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        Toast toast = Toast.makeText(Site_GalleryImage_RemoveUpdate_Activity.this, "Image updated successfully", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void deleteTempMilestonePicFile() throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp" + File.separator + "MansionlyProjectAlbum");

        File fileArray[] = picFolderInSDCard.listFiles();

        HashMap<String, String> serverIdToFileNameMap =
                pref.getServerIdToFileNameMap();
        //pic_id is server id.
        if (serverIdToFileNameMap == null) {
            Utility_functions.showToastOnMainThread(Site_GalleryImage_RemoveUpdate_Activity.this,
                    "Image not found.");
            return;
        }
        String fileNameToDelete = serverIdToFileNameMap.get(pic_id);
        if (fileNameToDelete == null || fileNameToDelete.isEmpty()) {
            Utility_functions.showToastOnMainThread(Site_GalleryImage_RemoveUpdate_Activity.this,
                    "Image not found.");
            return;
        }

        for (int i = 0; i < fileArray.length; i++) {
            File oneFile = fileArray[i];
            if (fileNameToDelete.contentEquals(oneFile.getName())) {
                oneFile.delete();
                serverIdToFileNameMap.remove(pic_id);
                pref.saveServerIdToFileNameMap(serverIdToFileNameMap);
                break;
            }
        }
    }

    Runnable internetCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isOnline = Utility_functions.internetConnected(getApplicationContext());
            if (isOnline) {
                llNoInternet.setVisibility(View.GONE);
            } else {
                llNoInternet.setVisibility(View.VISIBLE);
            }
            new Handler().postDelayed(internetCheck, 5000);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext() , Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }

}
