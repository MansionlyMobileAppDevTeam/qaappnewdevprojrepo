package com.mansionly.qaapp.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.login.LogIn_Activity;
import com.mansionly.qaapp.util.AppPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import com.mansionly.qaapp.R;

public class Fragment_Change_Password extends Fragment {

    View rootView;
    private Toolbar toolbar;
    EditText etCurrentPassword,etNewPass,etRe_enterNewPass;
    Button btnChangePassword;
    AppPreferences pref;
    ProgressBar progressBar;
    String userid, string_password, msg, msg_string,string_retype_password, string_current_pass;
    TextInputLayout currentPasWrapper,newPasWrapper,reEnterPasWrapper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_change_password, container, false);
        setHasOptionsMenu(true);

        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Change Password");

        pref = new AppPreferences(getActivity());
        userid = pref.getUserIdPref();
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        etCurrentPassword=(EditText) rootView.findViewById(R.id.etCurrentPassword);
        etNewPass=(EditText) rootView.findViewById(R.id.etNewPass);
        etRe_enterNewPass=(EditText) rootView.findViewById(R.id.etRe_enterNewPass);
        btnChangePassword=(Button) rootView.findViewById(R.id.btnChangePassword);
        currentPasWrapper = (TextInputLayout) rootView.findViewById(R.id.usernameWrapper);
        newPasWrapper = (TextInputLayout) rootView.findViewById(R.id.NewPasWrapper);
        reEnterPasWrapper = (TextInputLayout) rootView.findViewById(R.id.ReEnterPasWrapper);

        etRe_enterNewPass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    //do something
                    string_password = etNewPass.getText().toString().trim();
                    string_retype_password = etRe_enterNewPass.getText().toString().trim();
                    string_current_pass = etCurrentPassword.getText().toString().trim();

                    if (string_current_pass.equals("")) {
                        Toast.makeText(getActivity(), "Please enter the Password.", Toast.LENGTH_SHORT).show();
                    } else if (string_password.equals("")) {
                        Toast.makeText(getActivity(), "Please enter the Password.", Toast.LENGTH_SHORT).show();
                    } else if (string_password.length() < 6) {
                        newPasWrapper.setError("Not a valid email address!");
                    } else if (string_retype_password.equals("")) {
                        Toast.makeText(getActivity(), "Please enter retype password.", Toast.LENGTH_SHORT).show();
                    } else if (string_retype_password.length() < 6) {
                        reEnterPasWrapper.setError("Not a valid email address!");
                    } else if (!string_retype_password.equals(string_password)) {
                        Toast.makeText(getActivity(), "Entered retype password doesn't match with password.", Toast.LENGTH_SHORT).show();
                    } else {

                        new ChangePasswordWeb().execute();
                    }
                }
                return false;
            }
        });


        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                string_password = etNewPass.getText().toString().trim();
                string_retype_password = etRe_enterNewPass.getText().toString().trim();
                string_current_pass = etCurrentPassword.getText().toString().trim();

                if (string_current_pass.equals("")) {
                    Toast.makeText(getActivity(), "Please enter the Password.", Toast.LENGTH_SHORT).show();
                } else if (string_password.equals("")) {
                    Toast.makeText(getActivity(), "Please enter the Password.", Toast.LENGTH_SHORT).show();
                } else if (string_password.length() < 6) {
                    newPasWrapper.setError("Not a valid email address!");
                } else if (string_retype_password.equals("")) {
                    Toast.makeText(getActivity(), "Please enter retype password.", Toast.LENGTH_SHORT).show();
                } else if (string_retype_password.length() < 6) {
                    reEnterPasWrapper.setError("Not a valid email address!");
                } else if (!string_retype_password.equals(string_password)) {
                    Toast.makeText(getActivity(), "Entered retype password doesn't match with password.", Toast.LENGTH_SHORT).show();
                } else {

                    new ChangePasswordWeb().execute();
                }
            }
        });


        return rootView;
    }

    private class ChangePasswordWeb extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pd == null) {
                pd = new ProgressDialog(getActivity());
                pd.setMessage("Loading...");
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... args0) {
            try {

                String link = Webservice.CHANGE_PASSWORD;

                String data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userid, "UTF-8");
                data += "&" + URLEncoder.encode("old_pass", "UTF-8") + "=" + URLEncoder.encode(string_current_pass, "UTF-8");
                data += "&" + URLEncoder.encode("new_pass", "UTF-8") + "=" + URLEncoder.encode(string_password, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter
                        (conn.getOutputStream());
                wr.write(data);
                wr.flush();
                BufferedReader reader = new BufferedReader
                        (new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }
                return sb.toString();
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("result", result);

            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject gg = jobj.getJSONObject("result");
                msg = gg.getString("mMsgStr");
                msg_string = gg.getString("msg_string");

                if (msg.equals("0")) {
                    Toast.makeText(getActivity(), msg_string, Toast.LENGTH_LONG).show();
                }

                if (msg.equals("1")) {
                    pref.saveUserLoginStatus(false);
                    Intent intent = new Intent(getActivity(), LogIn_Activity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
                pd.dismiss();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
