package com.mansionly.qaapp.util;

public class AppConsts {

    public static final String HOST = "52.221.229.188";
    public static final String SUCCESS = "success";
    public static final String ERROR_TYPE = "errorType";
    public static final String ERROR = "error";
    public static final String JSON_RESPONSE = "json_response";
    public static final String SERVER_ID_TO_FILE_NAME_LIST = "SERVER_ID_TO_FILE_NAME_LIST";

    public static String CURRENT_NAV_ITEM_INDEX = "currNavItemIndex";
    public static String CURRENT_TAB_INDEX = "currTabIndex";
    public static int THUMBNAIL_SIZE = 256;


    public static String USER_LOGIN = "userLogin";
    public static String USER_ID = "userId";
    public static String USER_NAME = "userName";
    public static String USER_TYPE = "userType";
    public static String USER_EMAIL_ID = "userEmailId";
    public static String USER_PRIVILEGE = "userType";
    public static final String USER_PROFILE_QA_SUPERVISOR = "user_profile_qa_supervisor";

    public static int MINI_THUMBNAIL_SIZE = 90;

    public static final String QA_USER_ID = "user_id";
    public static final String QA_ORDER_ID = "orderId";
    public static final String QA_MILESTONE_ORDER_ID = "o_id";
    public static final String QA_UNIQUE_ORDER_ID = "uniqueOrderId";
    public static final String QA_ORDER_TYPE = "ordtype";
    public static final String QA_OFFSET = "offset";
    public static final String QA_LIMIT = "limit";
    public static final String QA_CUSTOMER_EAILID = "email";
    public static final String QA_MILESTONE_ID = "milestone_plan_id";
    public static final String QA_COMMENT = "comment";
    public static final String QA_IMAGE = "image";
    public static final String QA_WORK_ORDER_ID = "unique_workorder_id";
    public static final String TASK_REJECTION_DATE = "taskRejectionDate";
    public static final String TASK_REJECTED_BY = "taskRejectedBy";
    public static final String TASK_PIC_COUNT = "taskPicCount";
    public static final String TASK_LOCAL_PIC_COUNT = "taskLocalPicIdsCount";
    public static final String TASK_LOCAL_PIC_ID = "taskLocalPicIds";

    public static final String QA_TASK_SORT_BY = "sort_by";
    public static final String QA_TASK_SORT_TYPE = "sort_type";
    public static final String QA_TASK_SEARCH = "search_word";

    public static final String QA_REJECT_EDIT_COMMENT = "rejectEditcomment";
    public static final String QA_APPROVE_EDIT_COMMENT = "approveEditcomment";
    public static final String QA_DATE_TIME = "dateTime";
    public static final String ALL_IMAGES = "imageArray";

    public static final String MILESTONE_PIC_COMMENT_LIST = "milestonePicCommentList";
    public static final String MILESTONE_PIC_LABEL_LIST = "milestonePicLabelList";
    public static final String MILESTONE_PIC_ID_LIST = "milestonePicIdList";
    public static final String MILESTONE_VIEW_CHECK_LIST = "milestoneViewCheckList";
    public static final String MILESTONE_BMP_LIST = "milestonePicBmpList";


    public static final String MILESTONE_PIC_COUNT = "milestonePicCount";


    public static final String QA_QUOTE_ID = "quote_id";
    public static final String QA_SECTION_OFHOUSE = "section_of_house_title";
    public static final String QA_SECTION_OFHOUSE_ID = "section_of_house_id";
    public static final String QA_PLAN_DATE = "plan_completion_date";
    public static final String QA_SELLER_NAME = "executioner_seller_name";
    public static final String QA_ACTIVITY = "activity";
    public static final String QA_CUSTOMER_NAME = "name";
    public static final String QA_CUSTOMER_MAILID = "mailId";


    public static final String QA_PROPERTY_ADDRESS = "propertyAddress";
    public static final String CURRENT_TASK_TYPE = "currTaskType";


}
