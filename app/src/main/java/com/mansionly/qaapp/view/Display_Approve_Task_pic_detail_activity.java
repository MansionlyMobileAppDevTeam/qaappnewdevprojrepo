package com.mansionly.qaapp.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.Utility_functions;

import java.util.HashMap;
import java.util.Map;

public class Display_Approve_Task_pic_detail_activity extends AppCompatActivity {
    ImageView ivMilestonePic;
    Toolbar toolbar;
    LinearLayout llNoInternet;
    TextView comment, label;
    CheckBox customerView;
    TextView btnRemove;
    AppPreferences pref;
    String picId;
    int mImagePositionStr = 0;
    Map<String, String> milestoneViewableCheckMap = new HashMap<String, String>();
    Map<String, String> milestonePicCommentsMap = new HashMap<String, String>();
    Map<String, String> milestonePicLabelsMap = new HashMap<String, String>();
    Map<String, String> milestonePicIdMap = new HashMap<String, String>();
    LinearLayout btnLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_pic_detail_layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        pref = new AppPreferences(getApplicationContext());
        milestoneViewableCheckMap = pref.getMilestoneViewCheckMapPref();
        milestonePicCommentsMap = pref.getMilestonePicCommentsMapPref();
        milestonePicLabelsMap = pref.getMilestonePicLabelsMapPref();
        milestonePicIdMap = pref.getMilestonePicIdMapPref();

        llNoInternet = (LinearLayout) findViewById(R.id.internetCheck);
        new Handler().post(internetCheck);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Details");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ivMilestonePic = (ImageView) findViewById(R.id.milestone_full_pic_imageView);
        comment = (TextView) findViewById(R.id.comment);
        label = (TextView) findViewById(R.id.label);
        customerView = (CheckBox) findViewById(R.id.checkBox);
        btnRemove = (TextView) findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(picDetailRemoveClickListener);
        btnLayout = (LinearLayout) findViewById(R.id.btnLayout);

        Intent intent = getIntent();
        Uri image = Uri.parse(intent.getExtras().getString("image"));
        Glide.with(this).load(image)
                .apply(new RequestOptions().fitCenter())
                .into(ivMilestonePic);

        picId = intent.getExtras().getString("picId");
        mImagePositionStr = intent.getExtras().getInt("image_position");
        String commentstring = intent.getExtras().getString("comment");
        String piclabel = intent.getExtras().getString("label");
        String viewableCheck = intent.getExtras().getString("viewableCheck");
        String flag = intent.getExtras().getString("flag");
        if (flag.equalsIgnoreCase("read_Only"))
            btnLayout.setVisibility(View.GONE);
        else
            btnLayout.setVisibility(View.VISIBLE);

        comment.setText(commentstring);
        label.setText(piclabel);
        if (viewableCheck.equalsIgnoreCase("1")) {
            customerView.setText("Customer Viewable");
            customerView.setChecked(true);

        } else {
            customerView.setText("Customer Viewable");
            customerView.setChecked(false);

        }

    }

    View.OnClickListener picDetailRemoveClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            //Intent intent = new Intent(Display_Approve_Task_pic_detail_activity.this, Task_Approve_Step_1_Activity.class);

            //intent.putExtra("picId", picId);
            //intent.putExtra("position", mImagePositionStr);
            Task_Approve_Step_1_Activity.setImagePosition(mImagePositionStr);
           // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pref.saveTaskPicCount(pref.getTaskPicCount() - 1);
            finish();
            //startActivity(intent);

        }
    };

    Runnable internetCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isOnline = Utility_functions.internetConnected(getApplicationContext());
            if (isOnline) {
                llNoInternet.setVisibility(View.GONE);
            } else {
                llNoInternet.setVisibility(View.VISIBLE);
            }
            new Handler().postDelayed(internetCheck, 5000);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext() , Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }
}