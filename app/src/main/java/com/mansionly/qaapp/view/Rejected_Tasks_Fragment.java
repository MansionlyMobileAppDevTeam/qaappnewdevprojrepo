package com.mansionly.qaapp.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.AndroidMultipartEntity;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.model.QATask;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.CustomScrollView;
import com.mansionly.qaapp.util.Utility_functions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Rejected_Tasks_Fragment extends Fragment {

    String mMsgStr, mUserIdStr, mOidStr, mEmailIdStr, mWorkOidStr, mUniqueOidStr, mApartmentStr = "", mNameStr = "", mLastActionDateStr = "", mLobTitleStr = "",
            mOrderTypeStr, mRejectedTaskOffsetStr, mLimitStr, mMilestonePlanIdStr, mQaActivityName = "", mQASectionOfHouseStr = "", mExecSellerName = "",
            mRejectedBy = "", mRejectionDate;

    ArrayList<QATask> mQATaskList = new ArrayList<QATask>();
    ProgressBar pbTodayTask;
    long totalSize = 0;
    View rootView;
    AppPreferences pref;
    TextView empty_text;
    RecyclerView recyclerView;
    RejectedTaskAdpater mAdapter;
    String sortBy = "DATE";
    String searchText = "";
    String selectedMenuItem = "QADate";
    CustomScrollView scrollview;
    boolean ischecked = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.rejected_activities_list, container, false);
        setHasOptionsMenu(true);

        pbTodayTask = (ProgressBar) rootView.findViewById(R.id.today_task_progressBar);
        empty_text = (TextView) rootView.findViewById(R.id.empty_text);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        scrollview = (CustomScrollView) rootView.findViewById(R.id.lead_details_scrollview);
        scrollview.setScrollViewListener(leadDetailsScrollViewListener);

        pref = new AppPreferences(getActivity());
        mUserIdStr = pref.getUserIdPref();
        mEmailIdStr = pref.getUserEmail();
        mOrderTypeStr = "";// orders by user ID
        mRejectedTaskOffsetStr = "0";
        mLimitStr = "0";
        sortBy = "DATE";

        new RetrieveRejectedTaskList(true).execute("0");

        return rootView;
    }

    CustomScrollView.ScrollViewListener leadDetailsScrollViewListener = new CustomScrollView.ScrollViewListener() {

        @Override
        public void onScrollChanged(CustomScrollView scrollView, int currLen, int currTop, int prevLen, int prevTop) {

            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (diff == 0) {
                new RetrieveRejectedTaskList(false).execute();

            }

        }
    };

    private class RetrieveRejectedTaskList extends AsyncTask<String, Integer, String> {
        private ProgressDialog pd;
        private boolean showLoader;

        public RetrieveRejectedTaskList(boolean showLoader) {
            this.showLoader = showLoader;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pd == null) {
                pd = new ProgressDialog(getActivity());
                pd.setMessage("Loading...");
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                if (showLoader) {
                    pd.show();
                }
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String responseString = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Webservice.REJECTED_TASK_LIST);

            try {
                AndroidMultipartEntity entity = new AndroidMultipartEntity(
                        new AndroidMultipartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                entity.addPart(AppConsts.QA_USER_ID, new StringBody(mUserIdStr));
                entity.addPart(AppConsts.QA_ORDER_TYPE, new StringBody(mOrderTypeStr));
                entity.addPart(AppConsts.QA_OFFSET, new StringBody(mRejectedTaskOffsetStr));
                entity.addPart(AppConsts.QA_LIMIT, new StringBody(mLimitStr));
                entity.addPart(AppConsts.QA_CUSTOMER_EAILID, new StringBody(mEmailIdStr));
                entity.addPart(AppConsts.QA_TASK_SORT_BY, new StringBody(sortBy));
                entity.addPart(AppConsts.QA_TASK_SEARCH, new StringBody(searchText));
                if (sortBy.equalsIgnoreCase("DATE"))
                    entity.addPart(AppConsts.QA_TASK_SORT_TYPE, new StringBody("DESC"));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();

                responseString = e.toString();
            } catch (IOException e) {
                e.printStackTrace();
                responseString = e.toString();
            }
            return responseString;
        }


        @Override
        protected void onPostExecute(String result) {
            try {

                JSONObject jobj = new JSONObject(result);
                JSONObject gg = jobj.getJSONObject("result");
                mMsgStr = gg.getString(DBTableColumnName.DB_QUERY_NUM);
                String msg_string = gg.getString(DBTableColumnName.DB_QUERY_RESULT);

                if (mMsgStr.equals("0")) {
                    if (mQATaskList.isEmpty()) {
                        if (mAdapter != null)
                            mAdapter.clear();
                        empty_text.setVisibility(View.VISIBLE);
                    } else {
                        empty_text.setVisibility(View.GONE);
                    }

                } else if (mMsgStr.equals("1")) {

                    empty_text.setVisibility(View.GONE);

                    try {

                        JSONArray jarray = gg.getJSONArray("arr_record_list");
                        mRejectedTaskOffsetStr = gg.getString("offset");

                        for (int i = 0; i < jarray.length(); i++) {
                            JSONObject c = jarray.getJSONObject(i);

                            mOidStr = c.getString(DBTableColumnName.QA_MILESTONE_ORDER_ID);
                            mWorkOidStr = c.getString(DBTableColumnName.QA_WORK_ORDER_ID);
                            mUniqueOidStr = c.getString(DBTableColumnName.QA_UNIQUE_ORDER_ID);
                            mApartmentStr = c.getString(DBTableColumnName.QA_APARTMENT);
                            mNameStr = c.getString(DBTableColumnName.QA_NAME);
                            mQASectionOfHouseStr = c.getString(DBTableColumnName.QA_SECTION_OF_HOUSE);
                            mMilestonePlanIdStr = c.getString(DBTableColumnName.QA_MILESTONE_ID);
                            mLastActionDateStr = c.getString("last_action_date");
                            mLobTitleStr = c.getString(DBTableColumnName.QA_LOBTITLE);
                            mExecSellerName = c.getString(DBTableColumnName.QA_EXECSELLER_NAME);
                            mQaActivityName = c.getString(DBTableColumnName.QA_ACTIVITY_NAME);
                            mRejectedBy = c.getString(DBTableColumnName.TASK_REJECTED_BY);

                            QATask qaTask = new QATask(mOidStr, mUniqueOidStr, mWorkOidStr,
                                    mApartmentStr, mNameStr, mMilestonePlanIdStr,
                                    mQASectionOfHouseStr, mLastActionDateStr, "",mLobTitleStr,
                                    mExecSellerName, mQaActivityName, mRejectedBy, "",
                                    "");
                            mQATaskList.add(qaTask);
                        }

                        if (mAdapter == null) {
                            mAdapter = new RejectedTaskAdpater(mQATaskList);
                        }
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                } else if (mMsgStr.equals("550")) {
                    Toast.makeText(getActivity(), "550", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }
            pd.dismiss();
        }
    }

    public class RejectedTaskAdpater extends RecyclerView.Adapter<RejectedTaskAdpater.MyViewHolder> {

        private List<QATask> qaTaskList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tvApartmentAddress, tvPlannedDateTextview, tvCustomerName, tvSectionofHouse, tvPlannedDate, tvActivityName;
            ImageView imMilestoneImage;

            public MyViewHolder(View rootView) {
                super(rootView);
                tvCustomerName = (TextView) rootView.findViewById(R.id.tvCustomerName);
                tvApartmentAddress = (TextView) rootView.findViewById(R.id.tvApartmentAddress);
                tvSectionofHouse = (TextView) rootView.findViewById(R.id.tvSectionofHouse);
                tvPlannedDate = (TextView) rootView.findViewById(R.id.tvPlannedDate);
                tvPlannedDateTextview = (TextView) rootView.findViewById(R.id.planned_date_textView);
                tvActivityName = (TextView) rootView.findViewById(R.id.tvActivityName);
                imMilestoneImage = (ImageView) rootView.findViewById(R.id.imMilestoneImage);

            }
        }

        public RejectedTaskAdpater(List<QATask> milestonesList) {
            this.qaTaskList = milestonesList;
            if (getActivity() != null) {
                getActivity().invalidateOptionsMenu();
            }
        }

        @Override
        public RejectedTaskAdpater.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.rejected_task_listitem, parent, false);

            return new RejectedTaskAdpater.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final RejectedTaskAdpater.MyViewHolder holder, final int position) {

            final QATask qaTask = qaTaskList.get(position);

            if (qaTask.getPropertyAddress().equalsIgnoreCase("null") ||
                    qaTask.getPropertyAddress().trim().isEmpty())
                holder.tvApartmentAddress.setText("No address available");
            else
                holder.tvApartmentAddress.setText(qaTask.getPropertyAddress());
            holder.tvCustomerName.setText(qaTask.getCustomerName());
            holder.tvSectionofHouse.setText(qaTask.getQASectionOfHouse());
            holder.tvPlannedDateTextview.setText("Rejection date :");
            holder.tvPlannedDate.setText(Utility_functions.GetFormatedDate(qaTask.getQAPlannedDate()));

            holder.tvActivityName.setText(qaTask.getQAActivityName());
            String lobTitle = qaTask.getQALobTitle();

            if (lobTitle == null)
                holder.imMilestoneImage.setImageResource(R.mipmap.ic_residential);
            else if (lobTitle.equalsIgnoreCase("Institutional"))
                holder.imMilestoneImage.setImageResource(R.drawable.instituional);
            else if (lobTitle.equalsIgnoreCase("Project"))
                holder.imMilestoneImage.setImageResource(R.mipmap.ic_residential);
            else holder.imMilestoneImage.setImageResource(R.mipmap.ic_residential);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), Rejected_Task_Detail_activity.class);
                    intent.putExtra(AppConsts.QA_ORDER_ID, qaTask.getOrderId());
                    intent.putExtra(AppConsts.QA_PROPERTY_ADDRESS, qaTask.getPropertyAddress());
                    intent.putExtra(AppConsts.QA_WORK_ORDER_ID, qaTask.getWorkOrderId());
                    intent.putExtra(AppConsts.QA_MILESTONE_ID, qaTask.getQAMilestoneId());
                    intent.putExtra(AppConsts.QA_CUSTOMER_NAME, qaTask.getCustomerName());
                    intent.putExtra(AppConsts.QA_CUSTOMER_EAILID, mEmailIdStr);
                    pref.saveCurrentTaskRejectionDate(qaTask.getQAPlannedDate());
                    pref.saveCurrentTaskRejectedBy(qaTask.getLastActionBy());

                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return qaTaskList.size();
        }

        public void clear() {
            // TODO Auto-generated method stub
            qaTaskList.clear();
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_sort_menu, menu);

        final MenuItem item = menu.findItem(R.id.search_menuItem);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(taskSearchLstener);
        searchView.setOnCloseListener(searchCloseListener);

        MenuItem miRejectionDate = menu.findItem(R.id.SortByQADate);
        miRejectionDate.setTitle("Rejection Date");

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        mQATaskList.clear();
                        searchText = "";
                        new RetrieveRejectedTaskList(true).execute("0");
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        } else if (item.getItemId() == R.id.SortByCustomerName) {
            if (ischecked == true) {
                selectedMenuItem = "CustomerName";
                sortBy = "NAME";
                mRejectedTaskOffsetStr = "0";
                new RetrieveRejectedTaskList(true).execute();
                scrollview.scrollTo(0, scrollview.getTop());
                ischecked = false;
            }


        } else if (item.getItemId() == R.id.SortByQADate) {
            if (ischecked == false) {
                sortBy = "DATE";
                selectedMenuItem = "QADate";
                mRejectedTaskOffsetStr = "0";
                new RetrieveRejectedTaskList(true).execute();
                scrollview.scrollTo(0, scrollview.getTop());
                ischecked = true;
            }

        }
        ActivityCompat.invalidateOptionsMenu(getActivity());
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        SpannableString s;
        menu.findItem(R.id.Title).setEnabled(false);
        if (selectedMenuItem.equals("CustomerName")) {
            MenuItem miCustName = menu.findItem(R.id.SortByCustomerName);
            String miCustNameStr = miCustName.getTitle().toString();
            s = new SpannableString(miCustNameStr);
            s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), 0, s.length(), 0);
            miCustName.setTitle(s);
        } else {
            MenuItem miQADate = menu.findItem(R.id.SortByQADate);
            miQADate.setTitle("Rejection Date");
            String miQADateStr = miQADate.getTitle().toString();
            s = new SpannableString(miQADateStr);
            s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), 0, s.length(), 0);
            miQADate.setTitle(s);
        }

        return;
    }

    SearchView.OnCloseListener searchCloseListener = new SearchView.OnCloseListener() {
        @Override
        public boolean onClose() {
            mQATaskList.clear();
            searchText = "";
            mRejectedTaskOffsetStr = "0";
            new RetrieveRejectedTaskList(true).execute();
            return false;
        }
    };


    SearchView.OnQueryTextListener taskSearchLstener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            mQATaskList.clear();
            query = query.toLowerCase();
            searchText = query;
            mRejectedTaskOffsetStr = "0";
            new RetrieveRejectedTaskList(true).execute();

            InputMethodManager inputMethodManager = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }

    };


}
