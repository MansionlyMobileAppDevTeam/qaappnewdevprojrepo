package com.mansionly.qaapp.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.VolleyMultipartRequest;
import com.mansionly.qaapp.client.VolleySingleton;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.Utility_functions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class Task_Approve_Step_2_Activity extends AppCompatActivity {
    Toolbar toolbar;
    EditText etRejectComment;
    TextView btnApproval, btnCancel, tvAdd_comment_txt;
    AlertDialog alertDialog;
    AppPreferences pref;
    String mUserIdStr, mUserEmailIdStr, mOidStr, mMilestonePlanIdStr, mQuoteIdStr, mStatusLogStr, commentStr;
    Bundle bundle;
    Map<String, String> milestonePicIdMap = new HashMap<String, String>();
    LinearLayout llNoInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_approval_comment);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        llNoInternet = (LinearLayout) findViewById(R.id.internetCheck);
        new Handler().post(internetCheck);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Approve");
        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + "STEP 2 OF 2 - ADD COMMENT" + "</small>"));

        pref = new AppPreferences(getApplicationContext());
        mUserIdStr = pref.getUserIdPref();
        mUserEmailIdStr = pref.getUserEmail();
        mOidStr = pref.getOrderIdPref();
        mMilestonePlanIdStr = pref.getMileStoneId();
        mQuoteIdStr = pref.getQuoteIDpref();
        milestonePicIdMap = pref.getMilestonePicIdMapPref();

        bundle = getIntent().getExtras();
        mStatusLogStr = bundle.getString("activity_log_status");

        etRejectComment = (EditText) findViewById(R.id.approve_comment_editText);
        btnApproval = (TextView) findViewById(R.id.approval_textView);
        btnCancel = (TextView) findViewById(R.id.btnCancel);
        tvAdd_comment_txt = (TextView) findViewById(R.id.add_comment_txt);

        etRejectComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvAdd_comment_txt.setTextColor(Color.parseColor("#00786c"));
                etRejectComment.setBackgroundResource(R.drawable.background_comment_box);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etRejectComment.getText().toString().equalsIgnoreCase("")) {
                    AlertDialog.Builder taskRejectCancelAlertDlg = new AlertDialog.Builder(Task_Approve_Step_2_Activity.this);
                    taskRejectCancelAlertDlg.setTitle("Do you want to Cancel out of this step?");
                    taskRejectCancelAlertDlg.setMessage("On Cancel, your comments will be discarded and you will return to the previous step.");
                    taskRejectCancelAlertDlg.setNegativeButton("DON'T CANCEL",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    alertDialog.dismiss();
                                }
                            });

                    taskRejectCancelAlertDlg.setPositiveButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alertDialog = taskRejectCancelAlertDlg.create();
                    alertDialog.show();
                } else {
                    finish();
                }
            }
        });

        btnApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean cancel = false;
                UploadMilestoneInfo();
               /* etRejectComment.setError(null);
                commentStr = etRejectComment.getText().toString();

                if (TextUtils.isEmpty(commentStr)) {
                    tvAdd_comment_txt.setTextColor(Color.parseColor("#FF0000"));
                    etRejectComment.setBackgroundResource(R.drawable.background_comment_error_box);
                    cancel = true;
                }

                if (cancel) {

                } else {
                    UploadMilestoneInfo();

                   *//* String currTaskType = pref.getCurrentTaskTypePref();

                    My_Tasks_Fragment nextFrag= new My_Tasks_Fragment();
                    *//**//*getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frame, nextFrag,"findThisFragment")
                            .addToBackStack(null)
                            .commit();

                    Intent intent = new Intent(getApplicationContext(), Menu_Activity.class);
*//**//*
                    if (currTaskType.equals("TodayTask")) {
                        //intent.putExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 0);

                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.frame, nextFrag,"findThisFragment")
                                .addToBackStack(null)
                                .commit();
                    }
                    else if (currTaskType.equals("UpcomingTask")) {
                        //intent.putExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 1);

                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.frame, nextFrag,"findThisFragment")
                                .addToBackStack(null)
                                .commit();
                    }
                   // startActivity(intent);
*//*
                }*/

            }
        });

    }

    Runnable internetCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isOnline = Utility_functions.internetConnected(getApplicationContext());
            if (isOnline) {
                llNoInternet.setVisibility(View.GONE);
            } else {
                llNoInternet.setVisibility(View.VISIBLE);
            }
            new Handler().postDelayed(internetCheck, 5000);
        }
    };

    private void DeleteAllTempMilestonePicFile() throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp");
        if (!picFolderInSDCard.exists()) {
            try {
                if (picFolderInSDCard.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (picFolderInSDCard.listFiles().length != 0) {
                for (File eachFile : picFolderInSDCard.listFiles()) {
                    if (eachFile.getName().contains("temp_approve")) {
                        eachFile.delete();
                    }
                }
            }
        }
        return;
    }

    private void UploadMilestoneInfo() {
        String url = Webservice.TASK_STATUS_CHANGE;

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    JSONObject jobj = new JSONObject(resultResponse);
                    JSONObject gg = jobj.getJSONObject("result");
                    String msg = gg.getString("msg");
                    String msg_string = gg.getString("msg_string");

                    if (msg.equalsIgnoreCase("1")) {
                        try {
                            DeleteAllTempMilestonePicFile();
                            pref.clearMapPref();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        //finish();

                        Intent intent = new Intent(getApplicationContext(), Menu_Activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                        if (pref.getCurrentTaskTypePref().equals("TodayTask")) {
                            intent.putExtra(AppConsts.CURRENT_TAB_INDEX, 0);
                            intent.putExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 0);
                        } else if (pref.getCurrentTaskTypePref().equals("AllTodayTask")) {
                            intent.putExtra(AppConsts.CURRENT_TAB_INDEX, 0);
                            intent.putExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 1);
                        }
                        startActivity(intent);

                    } else {
                        Log.i("Unexpected", msg);
                    }
                } catch (
                        JSONException e)

                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        })

        {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put(DBTableColumnName.USER_ID, mUserIdStr);
                params.put(DBTableColumnName.QA_MILESTONE_ORDER_ID, mOidStr);
                params.put(DBTableColumnName.QA_EAILID, mUserEmailIdStr);
                params.put(DBTableColumnName.QA_QUOTATION_ID, mQuoteIdStr);
                params.put(DBTableColumnName.QA_MILESTONE_ID, mMilestonePlanIdStr);
                params.put(DBTableColumnName.QA_ACTIVITY_RESULT_STATUS, mStatusLogStr);
                params.put(DBTableColumnName.QA_ACTIVITY_RESULT_COMMENT, etRejectComment.getText().toString());

                StringBuilder sb = new StringBuilder();
                for (String key : milestonePicIdMap.keySet()) {
                    if (sb.length() > 0) {
                        sb.append("&");
                    }
                    String value = milestonePicIdMap.get(key);
                    try {

                        sb.append(value != null ? URLEncoder.encode(value, "UTF-8") : "");
                    } catch (UnsupportedEncodingException e) {
                        throw new RuntimeException("This method requires UTF-8 encoding support", e);
                    }
                }

                params.put(DBTableColumnName.QA_ACTIVITY_PIC_IDS, sb.toString());

                return params;
            }


        };

        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext(), Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }
}
