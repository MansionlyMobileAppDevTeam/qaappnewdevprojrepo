package com.mansionly.qaapp.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.VolleyMultipartRequest;
import com.mansionly.qaapp.client.VolleySingleton;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.Utility_functions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Approve_Rejected_Task_Activity extends AppCompatActivity {

    String mCustomerNameStr, mCustomerMailIdStr, mOidStr, mUserIdStr, mMilestonePlanIdStr;
    AppPreferences pref;
    TextView tvRejectionDate, tvRejectionBy, tvApprove, tvCancel,tvApprove_comment_textView;
    EditText etApproveComment;
    LinearLayout llNoInternet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.approve_rejected_task_layout);

        llNoInternet = (LinearLayout) findViewById(R.id.internetCheck);
        new Handler().post(internetCheck);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvRejectionDate = (TextView) findViewById(R.id.rejection_date_value_textView);
        tvRejectionBy = (TextView) findViewById(R.id.rejection_by_value_textView);
        etApproveComment = (EditText) findViewById(R.id.approve_comment_editText);
        tvApprove = (TextView) findViewById(R.id.approve_textView);
        tvCancel = (TextView) findViewById(R.id.cancel_textVew);
        tvApprove_comment_textView= (TextView) findViewById(R.id.approve_comment_textView);

        pref = new AppPreferences(getApplicationContext());
        mUserIdStr = pref.getUserIdPref();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mOidStr = extras.getString(AppConsts.QA_ORDER_ID);
            mMilestonePlanIdStr = extras.getString(AppConsts.QA_MILESTONE_ID);
            mCustomerNameStr = extras.getString(AppConsts.QA_CUSTOMER_NAME);
            mCustomerMailIdStr = extras.getString(AppConsts.QA_CUSTOMER_MAILID);
        }

        getSupportActionBar().setTitle("Approve");
        getSupportActionBar().setSubtitle("Override the rejection status ?");

        tvApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean cancel = false;

                etApproveComment.setError(null);
                String rejectionComment = etApproveComment.getText().toString();

                if (TextUtils.isEmpty(rejectionComment)) {
                    tvApprove_comment_textView.setTextColor(Color.parseColor("#FF0000"));
                    etApproveComment.setBackgroundResource(R.drawable.background_comment_error_box);
                    cancel = true;
                }

                if (!cancel) {
                    ApproveRejectedTask();
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });

        etApproveComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvApprove_comment_textView.setTextColor(Color.parseColor("#00786c"));
                etApproveComment.setBackgroundResource(R.drawable.background_comment_box);
            }
        });


        InitView();
    }

    Runnable internetCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isOnline = Utility_functions.internetConnected(getApplicationContext());
            if (isOnline) {
                llNoInternet.setVisibility(View.GONE);
            } else {
                llNoInternet.setVisibility(View.VISIBLE);
            }
            new Handler().postDelayed(internetCheck, 5000);
        }
    };

    private void InitView() {
        tvRejectionDate.setText(pref.getCurrentTaskRejectionDate());
        tvRejectionBy.setText(pref.getCurrentTaskRejectedBy());
    }

    private void ApproveRejectedTask() {
        String url = Webservice.TASK_STATUS_CHANGE;

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    JSONObject jobj = new JSONObject(resultResponse);
                    JSONObject gg = jobj.getJSONObject("result");
                    String msg = gg.getString("msg");
                    String msg_string = gg.getString("msg_string");

                    if (msg.equalsIgnoreCase("1")) {
                        try {
                            pref.clearMapPref();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(getApplicationContext(), Menu_Activity.class);
                        intent.putExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 2);
                        startActivity(intent);

                    } else {
                        Log.i("Unexpected", msg);
                    }
                } catch (
                        JSONException e)

                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        })

        {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(DBTableColumnName.USER_ID, mUserIdStr);
                params.put(DBTableColumnName.QA_MILESTONE_ORDER_ID, mOidStr);
                params.put(DBTableColumnName.QA_EAILID, mCustomerMailIdStr);
                params.put(DBTableColumnName.QA_MILESTONE_ID, mMilestonePlanIdStr);
                params.put(DBTableColumnName.QA_ACTIVITY_RESULT_STATUS, "1");
                params.put(DBTableColumnName.TASK_STATUS, "Approved");
                params.put(DBTableColumnName.QA_ACTIVITY_RESULT_COMMENT, etApproveComment.getText().toString());

                return params;
            }


        };

        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext() , Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }

}
