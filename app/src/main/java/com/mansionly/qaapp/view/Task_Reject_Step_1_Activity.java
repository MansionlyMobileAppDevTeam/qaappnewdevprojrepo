package com.mansionly.qaapp.view;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.os.StatFs;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.mansionly.qaapp.App;
import com.mansionly.qaapp.BackgroundServices.BackgroundMileStoneDataUploader;
import com.mansionly.qaapp.BackgroundServices.RebootServiceReceiver;
import com.mansionly.qaapp.Constants;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.AndroidMultipartEntity;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.VolleyMultipartRequest;
import com.mansionly.qaapp.client.VolleySingleton;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.localDb.SqliteDatabaseHandler;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.MediaCaptureHelper;
import com.mansionly.qaapp.util.Utility_functions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Task_Reject_Step_1_Activity extends AppCompatActivity {
    Toolbar toolbar;
    GridView imGridImageView;
    AppPreferences pref;
    Bitmap milestonePicBmp;
    String mUserIdStr, mUserEmailIdStr, mOidStr, mSectionOfHouse, mMilestonePlanIdStr, mQuoteIdStr, mStatusLogStr, mRevisedDate;
    File picFile;
    String picFilePath, pic_id = "", id, userid, milestone_plan_id;
    Map<String, String> milestoneViewableCheckMap = new HashMap<String, String>();
    Map<String, String> milestonePicCommentsMap = new HashMap<String, String>();
    Map<String, String> milestonePicLabelsMap = new HashMap<String, String>();
    Map<String, String> milestonePicIdMap = new HashMap<String, String>();
    ArrayList<String> milestoneLocalPicIdList = new ArrayList<String>();
    ArrayList<Uri> galleryImages = new ArrayList<Uri>();
    Task_Reject_Step_1_Activity.MilestonePicsAdapter mAddImageAdapter;
    ArrayList<Uri> picUriList = new ArrayList<>();
    ArrayList<String> picFilePathList = new ArrayList<>();
    //    ArrayList<Bitmap> picBmpList = new ArrayList<Bitmap>();
//    ArrayList<File> picFileList = new ArrayList<File>();
//    Bitmap picBmp = null;
    Bundle bundle;
    TextView tvSectionofHouse, tvActivityName, btnUpload, btnCancel, tvimagesAdded;
    LinearLayout bMilestoneCapturePic, bMilestoneUploadPic;
    private static final int UPLOAD_PIC_REQUEST = 100;
    int flag;
    List<Uri> mAdapterImagesUriList = new ArrayList<>();
    //    List<Bitmap> mImages;
    List<String> imageItem = new ArrayList();
    List<String> picIds = new ArrayList();
    List<Integer> imagePositions = new ArrayList();
    int mImagePositionStr, size, position;
    Menu mMenu;
    LinearLayout llNoInternet;
    Uri mProfilePicUri;
    private static final int CAMERA_PIC_REQUEST = 101;
    SqliteDatabaseHandler sqliteDatabaseHandler;
    public static int imagePosition = -1;
    MediaCaptureHelper mediaCaptureHelper;
    private RebootServiceReceiver myReceiver;
    public static void setImagePosition(int value) {
        imagePosition = value;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reject_approve_milestone);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        sqliteDatabaseHandler = new SqliteDatabaseHandler(Task_Reject_Step_1_Activity.this);
        llNoInternet = (LinearLayout) findViewById(R.id.internetCheck);
        new Handler().post(internetCheck);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Reject");
        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + "STEP 1 OF 2 - UPLOAD IMAGES" + "</small>"));

        pref = new AppPreferences(getApplicationContext());
        milestoneViewableCheckMap = pref.getMilestoneViewCheckMapPref();
        milestonePicCommentsMap = pref.getMilestonePicCommentsMapPref();
        milestonePicLabelsMap = pref.getMilestonePicLabelsMapPref();
        milestonePicIdMap = pref.getMilestonePicIdMapPref();
        milestoneLocalPicIdList = pref.getTaskLocalPicIdPref();
        userid = pref.getUserIdPref();
        milestone_plan_id = pref.getMileStoneId();

        if (milestonePicIdMap.size() == 0)
            pref.clearImageCount();
/*
        bundle = getIntent().getExtras();
        pic_id = bundle.getString("picId");*/


        tvSectionofHouse = (TextView) findViewById(R.id.tvSectionofHouse);
        tvActivityName = (TextView) findViewById(R.id.tvActivityName);
        tvimagesAdded = (TextView) findViewById(R.id.tvimagesAdded);

        bMilestoneCapturePic = (LinearLayout) findViewById(R.id.capture_image);
        bMilestoneUploadPic = (LinearLayout) findViewById(R.id.upload_image);
        imGridImageView = (GridView) findViewById(R.id.imGridImageView);
        btnUpload = (TextView) findViewById(R.id.btnUpload);
        btnUpload.setText("REJECT");
        btnCancel = (TextView) findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(onCancelClickListener);
        bMilestoneCapturePic.setOnClickListener(onCapturePicClickListener);
        bMilestoneUploadPic.setOnClickListener(onUploadPicClickListener);
        btnUpload.setOnClickListener(onSubmitClickListener);

        mediaCaptureHelper = new MediaCaptureHelper(this, null);
        mediaCaptureHelper.setMediaCaptureStartTrigger(bMilestoneCapturePic);

        try {
            RetrieveTempPicFiles();
        } catch (Exception e) {
            e.printStackTrace();
        }

        setGridAdapter();
        tvimagesAdded.setText("Images Added" + " " + "(" + pref.getTaskPicCount() + ")");

        InitView();
    }

    private void processImageFile(String picFilePaths, boolean isCameraCaptured) {
       /* Intent intent;
        if (isCameraCaptured) {
            intent = new Intent(Task_Reject_Step_1_Activity.this,
                    Reject_Capture_Pic_Detail_Activity.class);
        } else {
            intent = new Intent(Task_Reject_Step_1_Activity.this,
                    Reject_Upload_Pic_Detail_Activity.class);
        }
        // intent.putExtra("milestonePicByteArray", byteArray);
        intent.putExtra("activity_log_status", "0");
        intent.putExtra("picFilePath", picFilePath);

        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);*/

        if (isCameraCaptured) {
            picFilePath = App.getCapturedImageAbsolutePath();
        }
        milestonePicBmp = App.getCurrentImage();
        try {
            CreateMilestoteUploadTempFile();
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            RetrieveTempPicFiles();
        } catch (Exception e) {
            e.printStackTrace();
        }
        pref.saveTaskLocalPicIdPref(milestoneLocalPicIdList);
        pref.saveTaskPicCount(pref.getTaskPicCount() + 1);

        setGridAdapter();
        tvimagesAdded.setText("Images Added" + " " + "(" + pref.getTaskPicCount() + ")");
        //new AddMilestonePicDataTask(picFilePaths).execute();
    }

    Runnable internetCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isOnline = Utility_functions.internetConnected(getApplicationContext());
            if (isOnline) {
                llNoInternet.setVisibility(View.GONE);
            } else {
                llNoInternet.setVisibility(View.VISIBLE);
            }
            new Handler().postDelayed(internetCheck, 5000);
        }
    };

    private void InitView() {
        /*if (pref.getFlagInternalWork().contentEquals("1") && pref.getTaskPicCount() == 0) {
            btnUpload.setEnabled(true);
            btnUpload.setText("REJECT");
            btnUpload.setTextColor(Color.parseColor("#FF0000"));
        } else if (pref.getFlagInternalWork().contentEquals("1") && pref.getTaskPicCount() > 0) {
            btnUpload.setEnabled(true);
            btnUpload.setTextColor(Color.parseColor("#FF0000"));
        } else if ((pref.getFlagInternalWork().contentEquals("0") ||
                pref.getFlagInternalWork().contentEquals("")) && pref.getTaskPicCount() == 0) {
            btnUpload.setEnabled(false);
            btnUpload.setTextColor(Color.GRAY);
        }*/

        tvSectionofHouse.setText(pref.getSectionOfHousePref());
        tvActivityName.setText(pref.getActivity());

    }

    View.OnClickListener onSubmitClickListener = new View.OnClickListener() {
        public void onClick(View v) {

           /* Intent intent = new Intent(Task_Reject_Step_1_Activity.this,
                    Task_Reject_Step_2_Activity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.putExtra("activity_log_status", "0");
            startActivity(intent);*/
            mUserIdStr = pref.getUserIdPref();
            mUserEmailIdStr = pref.getUserEmail();
            mOidStr = pref.getOrderIdPref();
            mMilestonePlanIdStr = pref.getMileStoneId();
            mQuoteIdStr = pref.getQuoteIDpref();
            milestonePicIdMap = pref.getMilestonePicIdMapPref();
            mSectionOfHouse = pref.getSectionOfHouseIDPref();
            mRevisedDate = null;
            //RejectTask();

            StringBuilder sb = new StringBuilder();
            for (String key : milestonePicIdMap.keySet()) {
                if (sb.length() > 0) {
                    sb.append("&");
                }
                String value = milestonePicIdMap.get(key);
                try {

                    sb.append(value != null ? URLEncoder.encode(value, "UTF-8") : "");
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException("This method requires UTF-8 encoding support", e);
                }
            }
            if (picFilePathList.isEmpty()) {
                RejectTask();
            } else {
                RejectTask();
                for (int i = 0; i < picFilePathList.size(); i++) {
                    sqliteDatabaseHandler.insertTempData(userid, mOidStr, mMilestonePlanIdStr,
                            "Rejected", mSectionOfHouse,
                            picFilePathList.get(i).toString(), "0");
                }
                picFilePathList.clear();
                try {
                    DeleteAllTempMilestonePicFile();
                    pref.saveTaskPicCount(pref.getTaskPicCount() - 1);
                    pref.clearMapPref();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                startService(new Intent(Task_Reject_Step_1_Activity.this, BackgroundMileStoneDataUploader.class));
                Intent intent = new Intent(getApplicationContext(), Menu_Activity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                if (pref.getCurrentTaskTypePref().equals("TodayTask")) {
                    intent.putExtra(AppConsts.CURRENT_TAB_INDEX, 0);
                    intent.putExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 0);
                } else if (pref.getCurrentTaskTypePref().equals("AllTodayTask")) {
                    intent.putExtra(AppConsts.CURRENT_TAB_INDEX, 0);
                    intent.putExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 1);
                }
                startActivity(intent);
                finish();
            }
        }
    };
    private void setReceiver() {
        myReceiver = new RebootServiceReceiver();
        IntentFilter intentFilter = new IntentFilter();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver, intentFilter);
    }
    View.OnClickListener onCancelClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final AlertDialog.Builder taskRejectCancelAlertDlg =
                    new AlertDialog.Builder(Task_Reject_Step_1_Activity.this);
            taskRejectCancelAlertDlg.setTitle("Do you want to discard this task ?");
            taskRejectCancelAlertDlg.setMessage("On Discard, you will lose any images uploaded " +
                    "during the approval process");
            taskRejectCancelAlertDlg.setCancelable(true);

            taskRejectCancelAlertDlg.setPositiveButton(
                    "DISCARD",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                DeleteAllTempMilestonePicFile();
                                pref.saveTaskPicCount(pref.getTaskPicCount() - 1);
                                pref.clearMapPref();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            finish();
                        }
                    });

            taskRejectCancelAlertDlg.setNegativeButton(
                    "DON'T DISCARD",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            taskRejectCancelAlertDlg.create().show();

        }
    };

    private void RetrieveTempPicFiles() throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp" + File.separator + "Temp");
        if (!picFolderInSDCard.exists()) {
            try {
                if (picFolderInSDCard.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (picFolderInSDCard.listFiles().length != 0) {
                picUriList.clear();
                for (File eachFile : picFolderInSDCard.listFiles()) {
                    if (eachFile.getName().contains("temp_reject")) {
                        Uri tempUri = Uri.fromFile(eachFile);
                        picUriList.add(tempUri);
                    }
                }
                Collections.reverse(picUriList);
            }
        }
    }

    private void DeleteAllTempMilestonePicFile() throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp" + File.separator + "Temp");
        if (!picFolderInSDCard.exists()) {
            try {
                if (picFolderInSDCard.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (picFolderInSDCard.listFiles().length != 0) {
                for (File eachFile : picFolderInSDCard.listFiles()) {
                    if (eachFile.getName().contains("temp_reject")) {
                        eachFile.delete();
                    }
                }
            }
        }
        return;
    }


    private void setGridAdapter() {
        mAddImageAdapter = new Task_Reject_Step_1_Activity
                .MilestonePicsAdapter(getApplicationContext(), picUriList);
        imGridImageView.setAdapter(mAddImageAdapter);

    }

    View.OnClickListener onCapturePicClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            flag = 0;
            OnCapturePicClick();
        }
    };


    View.OnClickListener onUploadPicClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            App.setCameraSession(false);
            flag = 1;
            OnUploadPicClick();
        }
    };

    public void OnCapturePicClick() {

        List<Intent> targetedIntents = new ArrayList<Intent>();

        Intent capturePicIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        List<ResolveInfo> resInfoCapturePic = this.getPackageManager().queryIntentActivities(capturePicIntent, 0);

        for (ResolveInfo eachResInfoCapturePic : resInfoCapturePic) {
            String packageNameCapturePic = eachResInfoCapturePic.activityInfo.packageName;
            Intent eachCaptureIntent = new Intent("android.media.action.IMAGE_CAPTURE");

            File photo = null;
            try {
                // place where to store camera taken picture
                photo = createTempProfilePicFile();
                photo.delete();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Please check SD card! Image shot is impossible!",
                        Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            mProfilePicUri = Uri.fromFile(photo);
            eachCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mProfilePicUri);
            eachCaptureIntent.putExtra("return-data", true);
            eachCaptureIntent.setPackage(packageNameCapturePic);
            targetedIntents.add(eachCaptureIntent);
        }

        Intent chooserIntent = Intent.createChooser(targetedIntents.remove(0), "Profile Pic");

        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedIntents.toArray(new Parcelable[]{}));

        startActivityForResult(chooserIntent, UPLOAD_PIC_REQUEST);

    }

    private File createTempProfilePicFile() throws Exception {
        File mProfilePicTempFile = null;
        File tempDir = Environment.getExternalStorageDirectory();

        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp");

        if (!picFolderInSDCard.exists()) {
            try {
                if (picFolderInSDCard.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            mProfilePicTempFile = File.createTempFile("temp_reject" + "_" + new Date().getTime(), /* prefix */
                    ".jpg", /* suffix */
                    picFolderInSDCard);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return mProfilePicTempFile;
    }

    public void OnUploadPicClick() {
        Intent intent = new Intent();
        // Show only images, no videos or anything else
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), UPLOAD_PIC_REQUEST);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
            if (data == null) {
                File myFile = new File(mProfilePicUri.getPath());
                picFilePath = myFile.getAbsolutePath();

                Intent intent = new Intent(Task_Reject_Step_1_Activity.this,
                        Reject_Capture_Pic_Detail_Activity.class);
                intent.putExtra("activity_log_status", "0");
                intent.putExtra("picFilePath", picFilePath);
                intent.putExtra("picUri", mProfilePicUri);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        } else if (requestCode == UPLOAD_PIC_REQUEST && resultCode == RESULT_OK) {
            galleryImages.clear();

            ClipData clip = data.getClipData();

            if (clip != null) {
                for (int i = 0; i < clip.getItemCount(); i++) {
                    ClipData.Item item = clip.getItemAt(i);
                    Uri uri = item.getUri();
                    galleryImages.add(uri);
                }
            } else {
                Uri selectedImageUri = data.getData();
                galleryImages.add(selectedImageUri);
            }

            for (int i = 0; i < galleryImages.size(); i++) {
                String[] projection = {MediaStore.MediaColumns.DATA};

                CursorLoader cursorLoader = new CursorLoader(getApplicationContext(), galleryImages.get(i),
                        projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                picFilePath = cursor.getString(column_index);
                // picFile = new File(picFilePath);
                cursor.close();
                App.setCapturedImageAbsolutePath(picFilePath);

                //decoding should be done considering memory constraints.
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int width = displayMetrics.widthPixels;
                int height = width * 16 / 9;
                Bitmap bitmap = Utility_functions.decodeSampledBitmapFromFile(picFilePath, width, height);

                if (bitmap == null) {
                    Utility_functions.showToastOnMainThread(Task_Reject_Step_1_Activity.this,
                            "The image file is invalid.");
                    return;
                }
                App.setCurrentImage(bitmap);
                processImageFile(picFilePath, false);
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public class MilestonePicsAdapter extends BaseAdapter {

        private GestureDetector gestureDetector;
        Context mContext;

        public MilestonePicsAdapter(Context context, ArrayList<Uri> milestoneTempPicList) {
            mAdapterImagesUriList = milestoneTempPicList;
            mContext = context;
            gestureDetector = new GestureDetector(Task_Reject_Step_1_Activity.this,
                    new Task_Reject_Step_1_Activity.SingleTapConfirm());
        }

        @Override
        public int getCount() {
            return mAdapterImagesUriList.size();
        }

        @Override
        public Object getItem(int position) {
            return mAdapterImagesUriList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View itemView, ViewGroup parent) {
            final Task_Reject_Step_1_Activity.MilestonePicsAdapter.ViewHolder holder;
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (itemView == null) {
                itemView = inflater.inflate(R.layout.task_pics_layout, null);

                holder = new Task_Reject_Step_1_Activity.MilestonePicsAdapter.ViewHolder();
                holder.ivMilestonePic = (ImageView) itemView.findViewById(R.id.gridview_image);
                holder.ivMilestonePic.setPadding(5, 5, 5, 5);
                holder.ivMilestonePic.setScaleType(ImageView.ScaleType.CENTER_CROP);

                itemView.setTag(holder);
            } else {
                holder = (ViewHolder) itemView.getTag();
            }

            //set properties on views here.
            Glide.with(mContext)
                    .load(mAdapterImagesUriList.get(mAdapterImagesUriList.size() - position - 1))
                    .into(holder.ivMilestonePic);

            // Set listener to item image
            imGridImageView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // Use lazy initialization for the gestureDetector
                    gestureDetector.onTouchEvent(event);
                    // At least the onTouch-callback gets called with the correct position
                    return true;
                }
            });

            return itemView;
        }

        public void refreshList(List<Uri> list) {
            mAdapterImagesUriList = list;
            this.notifyDataSetChanged();
        }

        class ViewHolder {
            public ImageView ivMilestonePic;
        }
    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDoubleTap(MotionEvent event) {
           /* position = imGridImageView.pointToPosition((int) event.getX(), (int) event.getY());
            pic_id = milestonePicIdMap.get(String.valueOf(mAdapterImagesUriList.size() - position - 1));

            String selectedItem = String.valueOf(picUriList.get(position));

            if (imageItem.contains(selectedItem)) {
                if (imageItem.size() <= position) {
                    imageItem.remove(position - 1);
                    picIds.remove(position - 1);
                    imagePositions.remove(position - 1);
                } else {
                    imageItem.remove(position);
                    picIds.remove(position);
                    imagePositions.remove(position);
                }
            } else {
                imageItem.add(selectedItem);
                picIds.add(pic_id);
                imagePositions.add(mAdapterImagesUriList.size() - position - 1);
            }

            if (imageItem.size() == 0) {
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.left_arrow);
                getSupportActionBar().setTitle("Reject");
                getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + "STEP 1 OF 2 - UPLOAD IMAGES" + "</small>"));
                mMenu.findItem(R.id.menuItem).setVisible(false);
            } else {
                getSupportActionBar().setHomeAsUpIndicator(android.R.drawable.ic_menu_close_clear_cancel);
                toolbar.setTitle(String.valueOf(imageItem.size()) + " " + "Selected");
                toolbar.setSubtitle("");
                mMenu.findItem(R.id.menuItem).setVisible(true);
            }*/
            // pref.saveListSize(imageItem.size());

            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent event) {
            int position = imGridImageView.pointToPosition((int) event.getX(), (int) event.getY());

//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            mAdapterImagesUriList.get(mAdapterImagesUriList.size() - position - 1)
//                    .compress(Bitmap.CompressFormat.JPEG, 70, stream);
//            byte[] byteArray = stream.toByteArray();

            Uri tempUri = mAdapterImagesUriList.get(mAdapterImagesUriList.size() - position - 1);

            id = String.valueOf(mAdapterImagesUriList.size() - 1 - position);
            pic_id = milestonePicIdMap.get(id);

            Intent intent = new Intent(Task_Reject_Step_1_Activity.this,
                    Display_Reject_Task_pic_detail_activity.class);
            intent.putExtra("image", tempUri.toString());
            intent.putExtra("picId", pic_id);
            intent.putExtra("image_position", mAdapterImagesUriList.size() - position - 1);
            intent.putExtra("comment", "");
            intent.putExtra("label", "");
            intent.putExtra("viewableCheck", "1");
            intent.putExtra("flag", "remove");

            startActivity(intent);
            return super.onDoubleTap(event);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_delete, menu);
        mMenu = menu;
        mMenu.findItem(R.id.menuItem).setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menuItem) {

            for (int ii = 0; ii < picIds.size(); ii++) {

                pic_id = picIds.get(ii);

                milestonePicCommentsMap.remove(pic_id);
                milestonePicLabelsMap.remove(pic_id);
                milestoneViewableCheckMap.remove(pic_id);

                pref.saveMilestonePicCommentMapPref(milestonePicCommentsMap);
                pref.saveMilestonePicLabelMapPref(milestonePicLabelsMap);
                pref.saveMilestoneViewCheckMapPref(milestoneViewableCheckMap);

            }
            for (int jj = 0; jj < imagePositions.size(); jj++) {

                int i = imagePositions.get(jj);
                new Task_Reject_Step_1_Activity.RemoveMilestonePicDataTask().execute(i);
                milestonePicIdMap.remove(String.valueOf(i));
                pref.saveMilestonePicIdMapPref(milestonePicIdMap);
            }

            getSupportActionBar().setHomeAsUpIndicator(R.drawable.left_arrow);
            getSupportActionBar().setTitle("Reject");
            getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + "STEP 1 OF 2 - UPLOAD IMAGES" + "</small>"));
            mMenu.findItem(R.id.menuItem).setVisible(false);
            if (milestonePicIdMap.size() == 0)
                pref.clearImageCount();

        }
        if (item.getItemId() == android.R.id.home) {
            if (milestonePicIdMap.size() != 0) {
                final AlertDialog.Builder taskRejectCancelAlertDlg =
                        new AlertDialog.Builder(Task_Reject_Step_1_Activity.this);
                taskRejectCancelAlertDlg.setTitle("Do you want to discard this task ?");
                taskRejectCancelAlertDlg.setMessage("On Discard, you will lose any images " +
                        "uploaded during the approval process");
                taskRejectCancelAlertDlg.setCancelable(true);

                taskRejectCancelAlertDlg.setPositiveButton(
                        "Discard",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    DeleteAllTempMilestonePicFile();
                                    pref.saveTaskPicCount(pref.getTaskPicCount() - 1);
                                    pref.clearMapPref();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                pref.clearImageCount();
                                tvimagesAdded.setText("Images Added");
                                finish();
                            }
                        });

                taskRejectCancelAlertDlg.setNegativeButton(
                        "DON'T DISCARD",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                taskRejectCancelAlertDlg.create().show();
            } else finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public class RemoveMilestonePicDataTask extends AsyncTask<Integer, String, String> {
        int position;
        ProgressDialog pd;

        protected void onPreExecute() {
            if (pd == null) {
                pd = new ProgressDialog(Task_Reject_Step_1_Activity.this);
                pd.setMessage("Loading...");
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(Integer... args0) {
            position = args0[0];

            String url = Webservice.MILESTONE_PIC_ADD_REMOVE_ACTION;

            String responseString = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            try {
                AndroidMultipartEntity entity = new AndroidMultipartEntity(
                        new AndroidMultipartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                            }
                        });

                entity.addPart(DBTableColumnName.USER_ID, new StringBody(userid));
                entity.addPart(DBTableColumnName.QA_MILESTONE_ID, new StringBody(milestone_plan_id));
                entity.addPart(DBTableColumnName.QA_MILESTONE_PIC_ADD_REMOVE_FLAG, new StringBody("delImg"));

                if (pic_id == null)
                    entity.addPart(DBTableColumnName.QA_ACTIVITY_PIC_IDS, new StringBody(""));
                else
                    entity.addPart(DBTableColumnName.QA_ACTIVITY_PIC_IDS, new StringBody(pic_id));

                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
                e.printStackTrace();
            } catch (IOException e) {
                responseString = e.toString();
                e.printStackTrace();
            }
            return responseString;
        }

        protected void onPostExecute(String result) {

            try {

                DeleteTempMilestonePicFile();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //picFilePath = picFile.getAbsolutePath();

//            picFileList.remove(position);
//            picBmpList.remove(position);

            //picUriList.remove((int) position);
            RemoveImageFromGrid(position);
            mAddImageAdapter = new Task_Reject_Step_1_Activity
                    .MilestonePicsAdapter(getApplicationContext(), picUriList);
            imGridImageView.setAdapter(mAddImageAdapter);


            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject jsonResult = jobj.getJSONObject("result");
                String msg = jsonResult.getString("msg");

                if (msg.equals("0")) {

                } else if (msg.equals("1")) {
                    try {
                        String picId = jsonResult.getString(DBTableColumnName.QA_ACTIVITY_PIC_IDS);
                        if (milestonePicIdMap.size() == 0)
                            pref.clearImageCount();
                        tvimagesAdded.setText("Images Added" + " " + "(" + pref.getTaskPicCount() + ")");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void DeleteTempMilestonePicFile() throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp" + File.separator + "Temp");

        File fList[] = picFolderInSDCard.listFiles();

        for (int i = 0; i < fList.length; i++) {
            File eachFile = fList[mImagePositionStr];
            String selectedFilePrefix = "temp" + "_" + "reject" + "_";
            if (eachFile.getName().contains(selectedFilePrefix)) {
                eachFile.delete();
            }
        }
        return;
    }

    public void RemoveImageFromGrid(int positions) {
        if (picUriList.size() != 0) {
            picUriList.remove(positions);
            picFilePathList.remove(positions);
            MilestonePicsAdapter mAddImageAdapter =
                    new MilestonePicsAdapter(getApplicationContext(), picUriList);
            imGridImageView.setAdapter(mAddImageAdapter);
            mAddImageAdapter.refreshList(picUriList);
            setGridAdapter();
        }
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder taskRejectCancelAlertDlg =
                new AlertDialog.Builder(Task_Reject_Step_1_Activity.this);
        taskRejectCancelAlertDlg.setTitle("Do you want to discard this task ?");
        taskRejectCancelAlertDlg.setMessage("On Discard, you will lose any images " +
                "uploaded during the approval process");
        taskRejectCancelAlertDlg.setCancelable(true);

        taskRejectCancelAlertDlg.setPositiveButton(
                "Discard",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            DeleteAllTempMilestonePicFile();
                            pref.saveTaskPicCount(pref.getTaskPicCount() - 1);
                            pref.clearMapPref();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        pref.clearImageCount();
                        tvimagesAdded.setText("Images Added");
                        finish();
                    }
                });

        taskRejectCancelAlertDlg.setNegativeButton(
                "DON'T DISCARD",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        taskRejectCancelAlertDlg.create().show();


    }

    @Override
    protected void onStart() {
        setReceiver();
        super.onStart();
        /*if (pref.getFlagInternalWork().contentEquals("1") && pref.getTaskPicCount() == 0) {
            btnUpload.setEnabled(true);
            btnUpload.setText("REJECT");
            btnUpload.setTextColor(Color.parseColor("#FF0000"));
        } else if (pref.getFlagInternalWork().contentEquals("1") && pref.getTaskPicCount() > 0) {
            btnUpload.setEnabled(true);
            btnUpload.setTextColor(Color.parseColor("#FF0000"));
        } else if ((pref.getFlagInternalWork().contentEquals("0") ||
                pref.getFlagInternalWork().contentEquals("")) && pref.getTaskPicCount() == 0) {
            btnUpload.setEnabled(false);
            btnUpload.setTextColor(Color.GRAY);
        }*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == Constants.CAMERA_PERMISSION_REQUEST) {
            mediaCaptureHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onDestroy() {
//        mediaCaptureHelper.clearCache();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (imagePosition >= 0) {
            mImagePositionStr = imagePosition;
            setImagePosition(-1);
            /*milestonePicCommentsMap.remove(pic_id);
            milestonePicLabelsMap.remove(pic_id);
            milestoneViewableCheckMap.remove(pic_id);

            milestonePicIdMap.remove(milestoneLocalPicIdList.get(mImagePositionStr));

            milestoneLocalPicIdList.remove(milestoneLocalPicIdList.get(mImagePositionStr));

            pref.saveMilestonePicCommentMapPref(milestonePicCommentsMap);
            pref.saveMilestonePicLabelMapPref(milestonePicLabelsMap);
            pref.saveMilestoneViewCheckMapPref(milestoneViewableCheckMap);
            pref.saveMilestonePicIdMapPref(milestonePicIdMap);
            pref.saveTaskLocalPicIdPref(milestoneLocalPicIdList);

            new Task_Reject_Step_1_Activity.RemoveMilestonePicDataTask().execute(mImagePositionStr);
            */
            try {
                DeleteTempMilestonePicFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
            RemoveImageFromGrid(mImagePositionStr);
            tvimagesAdded.setText("Images Added" + " " + "(" + picUriList.size() + ")");
        }


        String picFilePath = App.getCapturedImageAbsolutePath();
        if (picFilePath != null && App.isCameraSession()) {
            App.setCameraSession(false);
            processImageFile(picFilePath, true);
        }
        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext(), Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }

    private void CreateMilestoteUploadTempFile() throws Exception {
        File picFolderInSDCard = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()
                + File.separator + "Mansionly" + File.separator + "QAApp");
        if (!picFolderInSDCard.exists()) {
            try {
                if (picFolderInSDCard.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        File picFolderInSDCard1 = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() +
                File.separator + "Mansionly" + File.separator + "QAApp" + File.separator + "Temp");
        if (!picFolderInSDCard1.exists()) {
            try {
                if (picFolderInSDCard1.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            // picBmp = Utility_functions.compressImage(picFilePath);
            picFile = Utility_functions.GeneratePicFileFromBmp(picFilePath,
                    milestonePicBmp, 80);
            //picFileList.add(picFile);

            File tempFile = File.createTempFile("temp" + "_" + "reject" + "_", /* prefix */
                    ".jpg", /* suffix */
                    picFolderInSDCard); /* directory */
            File tempFile1 = File.createTempFile("temp" + "_" + "reject" + "_", /* prefix */
                    ".jpg", /* suffix */
                    picFolderInSDCard1); /* directory */
            Utility_functions.CopyFile(picFile, tempFile);
            Utility_functions.CopyFile(picFile, tempFile1);

            picFile = tempFile;
            picFilePathList.add(tempFile.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public class AddMilestonePicDataTask extends AsyncTask<Void, Void, String> {
        ProgressDialog asyncDialog = new ProgressDialog(Task_Reject_Step_1_Activity.this);
        String picfilepath;

        AddMilestonePicDataTask(String picfpath) {
            picfilepath = picfpath;
        }

        protected void onPreExecute() {

            asyncDialog.setMessage("Uploading...");
            //show dialog
            asyncDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String url = Webservice.MILESTONE_PIC_ADD_REMOVE_ACTION;

            String responseString = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            try {
                AndroidMultipartEntity entity = new AndroidMultipartEntity(
                        new AndroidMultipartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                            }
                        });

                picFile = new File(picfilepath);

                entity.addPart(DBTableColumnName.USER_ID, new StringBody(userid));
                entity.addPart(DBTableColumnName.QA_MILESTONE_ID, new StringBody(milestone_plan_id));
                entity.addPart(DBTableColumnName.QA_MILESTONE_PIC_ADD_REMOVE_FLAG, new StringBody("addImg"));
                entity.addPart(DBTableColumnName.QA_ACTIVITY_PIC, new FileBody(picFile));
                entity.addPart("picComment", new StringBody(""));
                entity.addPart("picLable", new StringBody(""));
                entity.addPart(DBTableColumnName.QA_IMAGE_CUSTOMER_VIEWABLE, new StringBody("1"));


                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
                e.printStackTrace();
            } catch (IOException e) {
                responseString = e.toString();
                e.printStackTrace();
            }
            return responseString;
        }

        protected void onPostExecute(String result) {
            asyncDialog.dismiss();

            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject jsonResult = jobj.getJSONObject("result");
                String msg = jsonResult.getString("msg");

                if (msg.equals("0")) {

                } else if (msg.equals("1")) {
                    try {
                        String picId = jsonResult.getString(DBTableColumnName.QA_ACTIVITY_PIC_IDS);
                        // milestonePicList.add(picId);

                       /* Intent intent = new Intent(Reject_Upload_Pic_Detail_Activity.this,
                                Task_Reject_Step_1_Activity.class);
                        intent.putExtra("milestone_plan_id", milestoneid);
                        intent.putExtra("o_id", o_id);
                        intent.putExtra("userid", userid);
                        intent.putExtra("quote_id", quote_id);
                        intent.putExtra("activity_log_status", "0");
                        intent.putExtra("milestonePicId", picId);*/

                        milestonePicCommentsMap = pref.getMilestonePicCommentsMapPref();
                        milestonePicCommentsMap.put(picId, "");
                        pref.saveMilestonePicCommentMapPref(milestonePicCommentsMap);

                        milestonePicLabelsMap = pref.getMilestonePicLabelsMapPref();
                        milestonePicLabelsMap.put(picId, "");
                        pref.saveMilestonePicLabelMapPref(milestonePicLabelsMap);

                        milestoneViewableCheckMap = pref.getMilestoneViewCheckMapPref();
                        milestoneViewableCheckMap.put(picId, "1");
                        pref.saveMilestoneViewCheckMapPref(milestoneViewableCheckMap);

                        milestonePicIdMap = pref.getMilestonePicIdMapPref();
                        milestonePicIdMap.put(String.valueOf(milestonePicIdMap.size()), picId);
                        pref.saveMilestonePicIdMapPref(milestonePicIdMap);

                        milestoneLocalPicIdList = pref.getTaskLocalPicIdPref();
                        milestoneLocalPicIdList.add(0, String.valueOf(milestonePicIdMap.size() - 1));
                        pref.saveTaskLocalPicIdPref(milestoneLocalPicIdList);

                        pref.saveTaskPicCount(pref.getTaskPicCount() + 1);

                        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        App.setCapturedImageAbsolutePath(null);

                        //startActivity(intent);
                        Toast toast = Toast.makeText(Task_Reject_Step_1_Activity.this,
                                "Image added successfully", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                        try {
                            RetrieveTempPicFiles();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        setGridAdapter();
                        tvimagesAdded.setText("Images Added" + " " + "(" + pref.getTaskPicCount() + ")");
                        InitView();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void RejectTask() {
        String url = Webservice.TASK_STATUS_CHANGE;

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    JSONObject jobj = new JSONObject(resultResponse);
                    JSONObject gg = jobj.getJSONObject("result");
                    String msg = gg.getString("msg");
                    String msg_string = gg.getString("msg_string");

                    if (msg.equalsIgnoreCase("1")) {
                        Toast.makeText(getApplicationContext(), msg_string, Toast.LENGTH_LONG).show();
                        try {
                            DeleteAllTempMilestonePicFile();
                            pref.clearMapPref();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(getApplicationContext(), Menu_Activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                        if (pref.getCurrentTaskTypePref().equals("TodayTask")) {
                            intent.putExtra(AppConsts.CURRENT_TAB_INDEX, 0);
                            intent.putExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 0);
                        } else if (pref.getCurrentTaskTypePref().equals("AllTodayTask")) {
                            intent.putExtra(AppConsts.CURRENT_TAB_INDEX, 0);
                            intent.putExtra(AppConsts.CURRENT_NAV_ITEM_INDEX, 1);
                        }
                        startActivity(intent);

                    } else {
                        Log.i("Unexpected", msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(DBTableColumnName.USER_ID, mUserIdStr);
                params.put(DBTableColumnName.QA_MILESTONE_ORDER_ID, mOidStr);
                params.put(DBTableColumnName.QA_EAILID, mUserEmailIdStr);
                params.put(DBTableColumnName.QA_QUOTATION_ID, mQuoteIdStr);
                params.put(DBTableColumnName.QA_MILESTONE_ID, mMilestonePlanIdStr);
                params.put(DBTableColumnName.QA_ACTIVITY_RESULT_STATUS, "0");
                params.put(DBTableColumnName.QA_PLAN_REVISED_DATE, "");
                params.put(DBTableColumnName.QA_ACTIVITY_RESULT_COMMENT, "");
                params.put(DBTableColumnName.QA_ACTIVITY_PIC_IDS, "");

                return params;
            }
        };

        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }
}
