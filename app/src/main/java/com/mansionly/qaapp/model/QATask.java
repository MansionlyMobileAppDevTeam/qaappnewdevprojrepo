package com.mansionly.qaapp.model;

public class QATask {

    String mOrderId ;
    String mWorkOrderId ;
    String mApartment ;
    String mCustomerName ;
    String mQAMobile;
    String mQAActivityName ;
    String mQASectionOfHouse ;
    String mQAMilestoneId ;
    String mPplanedDate;
    String mActualCompletionDate;
    String mLobTitle;
    String mExecSellerName;
    String mQAActionBy;
    String mQuoteIdStr, mSectionOfHouseID;

    public QATask(String mOidStr, String uniqueId, String workOrderId, String apartment,
                  String CustomerName, String qaMilestoneId, String QASectionOfHouse,
                  String planedDate, String actualCompletionDate, String lobTitle, String execSellerName, String qaActivityName,
                  String taskRejectedBy, String mQuoteIdStr, String mSectionOfHouseID) {
        mOrderId = mOidStr ;
        mWorkOrderId = workOrderId ;
        mApartment = apartment;
        mCustomerName = CustomerName ;
        mQASectionOfHouse = QASectionOfHouse ;
        mQAMilestoneId=qaMilestoneId;
        mPplanedDate=planedDate;
        mActualCompletionDate=actualCompletionDate;
        mLobTitle=lobTitle;
        mExecSellerName=execSellerName;
        mQAActivityName=qaActivityName;
        mQAActivityName=qaActivityName;
        mQAActionBy=taskRejectedBy;
        this.mQuoteIdStr = mQuoteIdStr;
        this.mSectionOfHouseID = mSectionOfHouseID;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public String getWorkOrderId() {
        return mWorkOrderId;
    }

    public String getPropertyAddress() {
        return mApartment;
    }
    public String getCustomerName() {
        return mCustomerName;
    }
    public String getQAMobile() {
        return mQAMobile;
    }
    public String getQAsellerName() {
        return mExecSellerName;
    }
    public String getQAActivityName() {
        return mQAActivityName;
    }
    public String getQASectionOfHouse() {
        return mQASectionOfHouse;
    }
    public String getQAMilestoneId() {
        return mQAMilestoneId;
    }
    public String getQAPlannedDate() {
        return mPplanedDate;
    }
    public String getActualCompletionDate() {
        return mActualCompletionDate;
    }
    public String getQALobTitle() {
        return mLobTitle;
    }

    public String getExecSellerName() {
        return mExecSellerName;
    }

    public String getLastActionBy() {
        return mQAActionBy;
    }

    public String getmQuoteIdStr() {
        return mQuoteIdStr;
    }

    public String getmSectionOfHouseID() {
        return mSectionOfHouseID;
    }
};

