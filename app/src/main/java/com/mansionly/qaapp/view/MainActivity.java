package com.mansionly.qaapp.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.mansionly.qaapp.localDb.SqliteDatabaseHandler;
import com.mansionly.qaapp.login.LogIn_Activity;
import com.mansionly.qaapp.util.AppPreferences;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    AppPreferences prefs;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    SqliteDatabaseHandler sqliteDatabaseHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        prefs = new AppPreferences(getApplicationContext());
        if (prefs.isUserLoggedIn()) {
            Intent intent = new Intent(getApplicationContext(), Menu_Activity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getApplicationContext(), LogIn_Activity.class);
            startActivity(intent);
        }
        //startService(new Intent(MainActivity.this,BackgroundMileStoneDataUploader.class));
        InitApp();

        finish();

        verifyStoragePermissions(MainActivity.this);
    }

    private void InitApp() {

        prefs.saveTaskPicCount(0);

        try {
            DeleteTempPicFile();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void DeleteTempPicFile() throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();

        File siteAlbumFolder = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp" + File.separator + "MansionlyProjectAlbum");

        File siteAlbumList[] = siteAlbumFolder.listFiles();

        if (siteAlbumList != null) {
            if (siteAlbumList.length != 0) {

                for (int j = 0; j < siteAlbumList.length; j++) {
                    siteAlbumList[j].delete();
                }
            }
        }

        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp"+ File.separator + "Temp");

        File fList[] = picFolderInSDCard.listFiles();

        if (fList != null) {
            if (fList.length != 0) {

                for (int i = 0; i < fList.length; i++) {
                    fList[i].delete();
                }
            }
        }


        return;
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
}