package com.mansionly.qaapp.model;

public class ProjectAlbum {
    String mOidStr,mUniqueOidStr,mApartmentStr,mCustomerName,mProjectCreationDateStr,mLobTitleStr;

    public ProjectAlbum(String OidStr, String uniqueOidStr, String apartmentStr, String nameStr, String projectCreationDateStr, String lobTitleStr) {
        mOidStr =OidStr;
        mUniqueOidStr =uniqueOidStr;
        mApartmentStr =apartmentStr;
        mCustomerName =nameStr;
        mProjectCreationDateStr =projectCreationDateStr;
        mLobTitleStr =lobTitleStr;

    }

    public String getPropertyAddress() {
        return mApartmentStr;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public String getProjectCreationDate() {
        return mProjectCreationDateStr;
    }

    public String getQALobTitle() {
        return mLobTitleStr;
    }

    public String getOrderId() {
        return mOidStr;
    }
}
