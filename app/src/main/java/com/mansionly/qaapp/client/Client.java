package com.mansionly.qaapp.client;

import android.content.Context;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;


public class Client {

    Context mContext;

    private static Client instance;

    private String mRequestUrl;

    private Client(Context context, String requestUrl) throws Exception {
        mRequestUrl = requestUrl;
        mContext = context;
    }

    public static Client getClientInstance(Context context, String requestUrl) throws Exception {
        if (instance == null) {
            if (context != null)
                instance = new Client(context, requestUrl);
        }
        return instance;
    }

    synchronized private String sendRequest(String requestUrl) throws Exception {
        try {
            URL url = new URL(requestUrl);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);

            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            // Read Server Response
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                break;
            }
            return sb.toString();
        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());
        }
    }

    public boolean isAppUpdateRequired(String currentVersion) throws Exception {
        String currVersionCode = null;
        boolean isUpdateRequired = false;
        if (currentVersion == null || currentVersion.trim().length() == 0)
            return isUpdateRequired;

        String serverResponse = sendRequest(mRequestUrl);

        JSONObject jobj = new JSONObject(serverResponse);
        JSONObject result = jobj.getJSONObject(ServerResponseType.SERVER_RESPONSE_RESULT);
        String responseMsg = result.getString(ServerResponseType.SERVER_RESPONSE_MSG);

        if (responseMsg == null)
            return isUpdateRequired;

        if (responseMsg.equals(ServerResponseType.SERVER_RESPONSE_SUCCESS)) {
            currVersionCode = result.getString(ServerResponseType.SERVER_RESPONSE_VERSION_CODE);
        }

        if (!currVersionCode.equals(currentVersion))
            isUpdateRequired = true;
        return isUpdateRequired;
    }
}
