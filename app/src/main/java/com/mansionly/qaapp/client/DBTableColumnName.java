package com.mansionly.qaapp.client;

public class DBTableColumnName {

    public static final String USER_ID = "user_id";
    public static final String USER_PASSWORD = "password";
    public static final String USER_MAIL_ID = "mail_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_TYPE = "user_type";
    public static final String USER_EMAIL_ID = "user_email_id";
    public static final String USER_PRIVILEGE = "user_privilege";
    public static final String USER_PROFILE_QA_SUPERVISOR= "user_profile_qa_supervisor";
    public static final String ORDER_ID= "order_id";

    public static final String DB_QUERY_NUM = "msg";
    public static final String DB_QUERY_RESULT = "msg_string";
    public static final String QA_MILESTONE_ORDER_ID = "o_id";
    public static final String QA_WORK_ORDER_ID = "unique_workorder_id";
    public static final String QA_UNIQUE_ORDER_ID = "unique_oid";
    public static final String QA_APARTMENT = "customer_address";
    public static final String QA_NAME = "name";
    public static final String QA_EXECSELLER_NAME = "exec_seller_name";
    public static final String QA_SECTION_OF_HOUSE = "section_of_house_title";
    public static final String QA_MILESTONE_ID = "milestone_plan_id";
    public static final String QA_PLANNED_DATE = "planedDate";
    public static final String QA_ACTUAL_COMPLETION_DATE = "actual_completion_date";
    public static final String QA_LOBTITLE = "lobTitle";
    public static final String QA_ACTIVITY_NAME = "milestone_activity";

    public static final String QA_MOBILE = "mobile";
    public static final String QA_EAILID = "user_email";
    public static final String QA_QUOTATION_ID = "quote_id";
    public static final String QA_ACTIVITY_RESULT_STATUS = "activity_log_status";
    public static final String QA_ACTIVITY_RESULT_COMMENT = "qa_comment";
    public static final String QA_ACTIVITY_RESULT_PICS = "orderQAImgs";
    public static final String QA_ACTIVITY_PIC = "orderQAImages";
    public static final String QA_ACTIVITY_PIC_IDS = "img_ids";
    public static final String QA_IMAGE_PIC_ID = "picId";
    public static final String QA_IMAGE_CUSTOMER_VIEWABLE = "flag_customer_viewable";


    public static final String TASK_STATUS = "log_status";
    public static final String PROJECT_SITE_IMAGES = "projectSiteImages";

    public static final String QA_PLAN_REVISED_DATE = "plan_revised_date";

    public static final String QA_FOLLOW_UP_DATE = "followup_date_time";
    public static final String QA_FOLLOW_UP_LAST_ACTION = "flag_follow_up_and_last_action";
    public static final String QA_ORDER_TYPE = "ordtype";
    public static final String QA_OFFSET = "offset";
    public static final String QA_LIMIT = "limit";
    public static final String QA_MILESTONE_PIC_ADD_REMOVE_FLAG = "flag_img_post";

    public static final String CURRENT_VERSION = "currVersion";
    public static final String TASK_REJECTION_DATE = "last_action_date";
    public static final String TASK_REJECTED_BY = "last_action_by";
    public static final String PROJECT_CREATION_DATE = "project_creation_date";

    public static final String COLUMN_USER_ID = "actor_user_id";
    public static final String COLUMN_QA_ORDER_ID = "order_id";
    public static final String COLUMN_QA_MILESTONE_PLAN_ID = "milestone_plan_id";
    public static final String COLUMN_QA_ACTIVITY_LOG_STATUS = "activity_log_status";
    public static final String COLUMN_QA_SECTION_OF_HOUSE = "section_of_house";
    public static final String COLUMN_QA_FILE_NAME = "file_name";
    public static final String COLUMN_QA_IMAGE_CUSTOMER_VIEWABLE = "customer_viewable";


}
