package com.mansionly.qaapp.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mansionly.qaapp.R;

public class QA_Album_Fragment extends Fragment {

    View rootView;
    TextView btnAddImage;
    LinearLayout tvNoImagesLyout;
    ImageView imNoimages;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container1, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_qa_album_gallery, container1, false);
        setHasOptionsMenu(true);

        tvNoImagesLyout = (LinearLayout) rootView.findViewById(R.id.tvNoImagesLyout);
        imNoimages = (ImageView) rootView.findViewById(R.id.imNoimages);
        btnAddImage = (TextView) rootView.findViewById(R.id.btnAddImage);
        btnAddImage.setVisibility(View.GONE);

        tvNoImagesLyout.setVisibility(View.VISIBLE);
        imNoimages.setVisibility(View.VISIBLE);
        return rootView;
    }
}
