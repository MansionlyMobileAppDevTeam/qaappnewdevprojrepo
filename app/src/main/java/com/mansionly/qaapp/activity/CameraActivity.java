package com.mansionly.qaapp.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.mansionly.qaapp.App;
import com.mansionly.qaapp.CameraRotationManager;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.util.Utility_functions;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;

public class CameraActivity extends AppCompatActivity implements View.OnClickListener,
        CameraRotationManager.OnOrientationChangedListener {
    private static final int RETRY_BUTTON = 100;
    private static final int CAPTURE_BUTTON = 101;

    private CameraView cameraView;
    private ImageView ivPreview;
    private Bitmap clickedImage;
    private Button recordButton, btSelect, btCancel;
    private MediaPlayer mediaPlayer;
    private FrameLayout flFlash;
    private long startMillis;
    private ProgressBar progressBar;
    private CameraRotationManager cameraRotationManager;
    private double currentRotation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        initVariables();
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle(getString(R.string.camera_activity_title));
    }

    private void initMediaPlayer() {
        AudioManager meng = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int volume = meng.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
        if (volume != 0) {
            mediaPlayer = MediaPlayer.create(getApplicationContext(),
                    Uri.parse("file:///system/media/audio/ui/camera_click.ogg"));
        }
    }

    private void initVariables() {
        cameraView = findViewById(R.id.camera);
        cameraView.addCameraKitListener(new CameraListener());
        ivPreview = (ImageView) findViewById(R.id.ivPreview);
        btSelect = (Button) findViewById(R.id.btSelect);
        btSelect.setOnClickListener(this);
        btCancel = (Button) findViewById(R.id.btCancel);
        btCancel.setOnClickListener(this);
        recordButton = (Button) findViewById(R.id.record_button);
        recordButton.setOnClickListener(this);
        recordButton.setTag(CAPTURE_BUTTON);
        flFlash = (FrameLayout) findViewById(R.id.flFlash);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        cameraRotationManager = new CameraRotationManager(this);
        cameraRotationManager.setOnOrientationChangedListener(this);
        currentRotation = 0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraView.start();
        initMediaPlayer();
        cameraRotationManager.startListening();
//        checkOrientation(0);
    }

    @Override
    protected void onPause() {
        cameraView.stop();
        mediaPlayer = null;
        cameraRotationManager.stopListening();
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void flashScreen() {
        flFlash.setVisibility(View.VISIBLE);
        AlphaAnimation fade = new AlphaAnimation(1, 0);
        fade.setDuration(250);
        fade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                flFlash.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        flFlash.startAnimation(fade);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.record_button:
                int tag = (int) view.getTag();
                if (tag == CAPTURE_BUTTON) {
                    if (mediaPlayer != null) {
                        mediaPlayer.start();
                    }
                    flashScreen();
                    startMillis = System.currentTimeMillis();
                    progressBar.setVisibility(View.VISIBLE);
                    Log.i("CameraActivity", "Capture started at " + startMillis);
                    cameraView.captureImage();
                } else {
                    showCaptureMode();
                }
                break;
            case R.id.btSelect:
                saveAndUseImage();
                break;
            case R.id.btCancel:
                clickedImage = null;
                finish();
                break;
        }
    }

    private void saveAndUseImage() {
        Utility_functions.saveImageToDevice(getApplicationContext(), clickedImage);
        App.setCurrentImage(clickedImage);
        App.setCameraSession(true);
        finish();
    }

    @Override
    public void orientationChanged(double degrees) {
        checkOrientation(degrees);
    }

    private Bitmap rotateBitmap(Bitmap bitmap) {
        if (currentRotation == 0) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        matrix.setRotate(Float.parseFloat(getInverse(currentRotation) + ""));
        return Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth(), bitmap.getHeight(),
                matrix, true);
    }


    private class CameraListener implements CameraKitEventListener {
        public void onPictureTaken(Bitmap bitmap) {
            Log.i("CameraActivity", "image received in " + (System.currentTimeMillis() - startMillis));
            clickedImage = bitmap;
            clickedImage = rotateBitmap(clickedImage);
            showPreviewMode();
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public void onEvent(CameraKitEvent cameraKitEvent) {

        }

        @Override
        public void onError(CameraKitError cameraKitError) {

        }

        @Override
        public void onImage(CameraKitImage cameraKitImage) {
            onPictureTaken(cameraKitImage.getBitmap());
        }

        @Override
        public void onVideo(CameraKitVideo cameraKitVideo) {

        }
    }

    private void showPreviewMode() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                cameraView.stop();
                ivPreview.setVisibility(View.VISIBLE);
                cameraView.setVisibility(View.INVISIBLE);
                ivPreview.setImageBitmap(clickedImage);
                recordButton.setTag(RETRY_BUTTON);
                recordButton.setBackground(ContextCompat.getDrawable(getApplicationContext(),
                        R.mipmap.ic_retry));
                btSelect.setVisibility(View.VISIBLE);
                btCancel.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showCaptureMode() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                clickedImage = null;
                cameraView.start();
                ivPreview.setVisibility(View.INVISIBLE);
                cameraView.setVisibility(View.VISIBLE);
                recordButton.setTag(CAPTURE_BUTTON);
                recordButton.setBackground(ContextCompat.getDrawable(getApplicationContext(),
                        R.mipmap.ic_recordbutton));
                btSelect.setVisibility(View.INVISIBLE);
                btCancel.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void checkOrientation(double degrees) {
//        Utility_functions.showToastOnMainThread(this, "Rotate " + degrees);
        degrees = getApproximateDegrees(degrees);
        if (degrees == currentRotation) {
            return;
        }
//        rotateDevice(degrees);
        currentRotation = degrees;
    }

    private double getApproximateDegrees(double degrees) {
        if (degrees >= -50 && degrees <= 50) {
            return 0;
        } else if (degrees > -130 && degrees < -50) {
            return -90;
        } else if (degrees > 50 && degrees < 130) {
            return 90;
        } else if (degrees >= 130 && degrees <= -130) {
            return -180;
        } else {
            return currentRotation;
        }

    }

//    private void rotateDevice(double degrees) {
//        if (degrees == 0) {
//            degrees = getInverse(currentRotation);
//        }
////        Utility_functions.showToastOnMainThread(this, "Rotate " + degrees);
//    }

    private double getInverse(double inValue) {
        double outValue = 0;
        if (inValue < 0) {
            outValue = Math.abs(inValue);
        } else if (inValue > 0) {
            outValue = inValue - (inValue * 2);
        }
        return outValue;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        App.setCapturedImageAbsolutePath(null);
        App.setCurrentImage(null);
    }
}
