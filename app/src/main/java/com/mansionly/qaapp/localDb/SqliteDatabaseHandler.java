package com.mansionly.qaapp.localDb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class SqliteDatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "temp_database";

    //Table for activity temp records
    private static final String TEMP_TABLE = "temp_table";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_USER_ID = "actor_user_id";
    private static final String COLUMN_QA_ORDER_ID = "order_id";
    private static final String COLUMN_QA_MILESTONE_PLAN_ID = "milestone_plan_id";
    private static final String COLUMN_QA_ACTIVITY_LOG_STATUS = "activity_log_status";
    private static final String COLUMN_QA_SECTION_OF_HOUSE = "section_of_house";
    private static final String COLUMN_QA_FILE_NAME = "file_name";
    private static final String COLUMN_QA_FLAG_CUSTOMER_VIEWABLE = "flag_customer_viewable";
    private static final String COLUMN_UPLOAD_STATUS = "upload_status";

    //Table for project album tem records
    private static final String PROJECT_ALBUM_TABLE = "project_album_table";
    private static final String COLUMN_PA_ID = "pa_id";
    private static final String COLUMN_PA_USER_ID = "pa_actor_user_id";
    private static final String COLUMN_PA_ORDER_ID = "pa_order_id";
    private static final String COLUMN_PA_PROJECT_SITE_IMAGES = "pa_project_site_images";
    private static final String COLUMN_PA_QA_IMAGE_CUSTOMER_VIEWABLE = "pa_qa_image_customer_viewable";
    private static final String COLUMN_PA_UPLOAD_STATUS = "pa_upload_status";

    private Context mCtx;

    public SqliteDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mCtx = context;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        // QA temp table create query
        String CREATE_TEMP_TABLE = "CREATE TABLE IF NOT EXISTS " + TEMP_TABLE + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_USER_ID + " TEXT ,"
                + COLUMN_QA_ORDER_ID + " TEXT,"
                + COLUMN_QA_MILESTONE_PLAN_ID + " TEXT,"
                + COLUMN_QA_ACTIVITY_LOG_STATUS + " TEXT,"
                + COLUMN_QA_SECTION_OF_HOUSE + " TEXT,"
                + COLUMN_QA_FILE_NAME + " TEXT,"
                + COLUMN_QA_FLAG_CUSTOMER_VIEWABLE + " TEXT,"
                + COLUMN_UPLOAD_STATUS + " TEXT DEFAULT 'false' )";
        db.execSQL(CREATE_TEMP_TABLE);

        // Project album temp table create query
        String CREATE_PROJECT_ALBUM_TABLE = "CREATE TABLE IF NOT EXISTS " + PROJECT_ALBUM_TABLE + "("
                + COLUMN_PA_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_PA_USER_ID + " TEXT ,"
                + COLUMN_PA_ORDER_ID + " TEXT ,"
                + COLUMN_PA_PROJECT_SITE_IMAGES + " TEXT,"
                + COLUMN_PA_QA_IMAGE_CUSTOMER_VIEWABLE + " TEXT,"
                + COLUMN_PA_UPLOAD_STATUS + " TEXT DEFAULT 'false' )";
        db.execSQL(CREATE_PROJECT_ALBUM_TABLE);
    }


    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TEMP_TABLE);

        onCreate(db);
    }


    public void dropTable() {
        // Drop older table if existed
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TEMP_TABLE);

        onCreate(db);
    }

    public boolean insertTempData(String actor_user_id, String order_id, String milestone_plan_id,
                                  String activity_log_status, String section_of_house, String file_name,
                                  String customer_viewable) {
        SQLiteDatabase db = this.getWritableDatabase();
        //String base64=convertImage(bitmap);
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, actor_user_id);//index 0
        values.put(COLUMN_QA_ORDER_ID, order_id);//index 1
        values.put(COLUMN_QA_MILESTONE_PLAN_ID, milestone_plan_id);//index 2
        values.put(COLUMN_QA_ACTIVITY_LOG_STATUS, activity_log_status);//index 3
        values.put(COLUMN_QA_SECTION_OF_HOUSE, section_of_house);//index 4
        values.put(COLUMN_QA_FILE_NAME, file_name);//index 5
        values.put(COLUMN_QA_FLAG_CUSTOMER_VIEWABLE, customer_viewable);//index 6

        // Inserting Row
        if (db.insert(TEMP_TABLE, null, values) > 0) {
            db.close();
            return true;
        }//tableName, nullColumnHack, CotentValues
        db.close(); // Closing database connection
        return false;
    }

    public boolean insertProjectAlbumTempData(String pa_actor_user_id, String pa_project_site_images,
                                              String pa_order_id, String pa_qa_image_customer_viewable) {
        SQLiteDatabase db = this.getWritableDatabase();
        //String base64=convertImage(bitmap);
        ContentValues values = new ContentValues();
        values.put(COLUMN_PA_USER_ID, pa_actor_user_id);//index 0
        values.put(COLUMN_PA_ORDER_ID, pa_order_id);//index 0
        values.put(COLUMN_PA_PROJECT_SITE_IMAGES, pa_project_site_images);//index 1
        values.put(COLUMN_PA_QA_IMAGE_CUSTOMER_VIEWABLE, pa_qa_image_customer_viewable);//index 2


        // Inserting Row
        if (db.insert(PROJECT_ALBUM_TABLE, null, values) > 0) {
            db.close();
            return true;
        }//tableName, nullColumnHack, CotentValues
        db.close(); // Closing database connection
        return false;
    }

    //Get qa temp records
    public JSONArray getTempTableData() {

        String selectQuery = "SELECT * FROM " + TEMP_TABLE + "";
        SQLiteDatabase myDataBase = this.getReadableDatabase();
        Cursor cursor = myDataBase.rawQuery(selectQuery, null);
        JSONArray resultSet = new JSONArray();
        cursor.moveToFirst();

        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }
        cursor.close();
        Log.d("TAG_NAME", resultSet.toString());
        return resultSet;
    }


    //Get project album temp records
    public JSONArray getProjectAlbumTempTableData() {

        String selectQuery = "SELECT * FROM " + PROJECT_ALBUM_TABLE + "";
        SQLiteDatabase myDataBase = this.getReadableDatabase();
        Cursor cursor = myDataBase.rawQuery(selectQuery, null);
        JSONArray resultSet = new JSONArray();
        cursor.moveToFirst();

        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }
        cursor.close();
        Log.d("TAG_NAME", resultSet.toString());
        return resultSet;
    }

    public boolean isExistsActivityInLocalDb(String milestone_plan_id) {
        String selectQuery = "SELECT  * FROM " + TEMP_TABLE + " WHERE " + COLUMN_QA_MILESTONE_PLAN_ID + " = " + milestone_plan_id;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments
        if (cursor.moveToFirst()) {
            return true;
        }

        return false;
    }

    public void clearAllRecordsInTempTable() {
        SQLiteDatabase db = this.getReadableDatabase();

        String DELETE_TABLE = "DELETE FROM " + TEMP_TABLE;

        db.execSQL(DELETE_TABLE);

    }

    //Removes records from qa temp table
    public void removeUploadedRecordInTempTable(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String DELETE_TABLE = "DELETE FROM " + TEMP_TABLE + " WHERE " + COLUMN_ID + " = " + id;

        db.execSQL(DELETE_TABLE);

    }

    //Removes record from projectalbum temp table
    public void removeUploadedRecordInProjectAlbumTable(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String DELETE_RECORD = "DELETE FROM " + PROJECT_ALBUM_TABLE + " WHERE " + COLUMN_PA_ID + " = " + id;

        db.execSQL(DELETE_RECORD);
    }

    public boolean checkDataBase() {
        File dbFile = mCtx.getDatabasePath(DATABASE_NAME);
        return dbFile.exists();
    }

}