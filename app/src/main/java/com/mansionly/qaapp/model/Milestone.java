package com.mansionly.qaapp.model;


public class Milestone {

    String milestone_plan_id;
    String section_of_house_title;
    String mQAactivityName;
    String executioner_seller_name;
    String plan_completion_date;
    String order_id;
    String quote_id;
    String display_status;
    String comment;
    String plan_revised_date;
    String seller_mobile, mFlagInternalWork;
    String workOrderIdStr;
    String mSectionOfHouseID;

    public Milestone(String order_id, String quote_id, String milestone_plan_id,
                     String mWorkOrderIdStr, String section_of_house_title,
                     String executioner_seller_name, String mQAactivityName) {
        this.milestone_plan_id = milestone_plan_id;
        this.section_of_house_title = section_of_house_title;
        this.executioner_seller_name = executioner_seller_name;
        this.workOrderIdStr = mWorkOrderIdStr;
        this.order_id = order_id;
        this.quote_id = quote_id;
        this.mQAactivityName = mQAactivityName;

    }

    public Milestone(String mOrderIdStr, String mQuoteIdStr, String mMilestonePlanIdStr,
                     String mWorkOrderIdStr, String mSectionOfHouseStr,
                     String mExecSellernameStr, String mQAactivityName, String mFlagInternalWork,
                     String mSectionOfHouseID) {

        this.milestone_plan_id = mMilestonePlanIdStr;
        this.section_of_house_title = mSectionOfHouseStr;
        this.executioner_seller_name = mExecSellernameStr;
        this.workOrderIdStr = mWorkOrderIdStr;
        this.order_id = mOrderIdStr;
        this.quote_id = mQuoteIdStr;
        this.mQAactivityName = mQAactivityName;
        this.mFlagInternalWork = mFlagInternalWork;
        this.mSectionOfHouseID = mSectionOfHouseID;

    }

    public String getmSectionOfHouseID() {
        return mSectionOfHouseID;
    }

    public void setmSectionOfHouseID(String mSectionOfHouseID) {
        this.mSectionOfHouseID = mSectionOfHouseID;
    }

    public String getQuote_id() {
        return quote_id;
    }

    public String getSection_house() {
        return section_of_house_title;
    }

    public String getActivity() {
        return mQAactivityName;
    }

    public String getPlanDate() {
        return plan_completion_date;
    }

    public String getExecutioner_seller_name() {
        return executioner_seller_name;
    }

    public String getWorkOrderId() {
        return workOrderIdStr;
    }

    public String getMilestonePlanId() {
        return milestone_plan_id;
    }

    public String getmFlagInternalWork() {
        return mFlagInternalWork;
    }
}
