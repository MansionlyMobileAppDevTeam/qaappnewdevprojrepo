package com.mansionly.qaapp.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mansionly.qaapp.R;

public class Version_control_dialog_fragment extends DialogFragment {

    AlertDialog alertDialog;

    public static Version_control_dialog_fragment newInstance() {
        Version_control_dialog_fragment f = new Version_control_dialog_fragment();
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Create the AlertDialog object and return it*/
        alertDialog = builder.create();
        ShowVersionControlDialog();
        return alertDialog;
    }

    public void ShowVersionControlDialog() {
        View vAlertDialogVersionControl = getActivity().getLayoutInflater().inflate(
                R.layout.version_control_alert_dialog, null);
        TextView dialogTitle = (TextView) vAlertDialogVersionControl
                .findViewById(R.id.version_control_alert_dialog_title_textView);
        TextView dialogContent = (TextView) vAlertDialogVersionControl
                .findViewById(R.id.version_control_alert_dialog_content_textView);
        TextView dialogExit = (TextView) vAlertDialogVersionControl
                .findViewById(R.id.version_control_alert_dialog_exit_option_textView);
        TextView dialogUpdate = (TextView) vAlertDialogVersionControl
                .findViewById(R.id.version_control_alert_dialog_update_option_textView);
        dialogExit.setLayoutParams(new LinearLayout.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT,
                1f));
        dialogUpdate.setLayoutParams(new LinearLayout.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT, 1f));

        String versionName = null;

        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        dialogTitle.setText("This version cannot be used");
        dialogContent.setText("This version of App is old & unusable. Please update the app" +
                System.getProperty("line.separator") + System.getProperty("line.separator") +
                "Version : " + versionName);
        dialogUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                final String appPackageName = getActivity().getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
                            + appPackageName)));
                } catch (android.content.ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri
                            .parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                System.exit(0);
            }
        });

        dialogExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        alertDialog.setView(vAlertDialogVersionControl);
        alertDialog.show();
    }
}
