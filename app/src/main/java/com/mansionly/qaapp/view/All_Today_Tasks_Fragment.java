package com.mansionly.qaapp.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.AndroidMultipartEntity;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.localDb.SqliteDatabaseHandler;
import com.mansionly.qaapp.model.Milestone;
import com.mansionly.qaapp.model.QATask;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.CustomScrollView;
import com.mansionly.qaapp.util.Utility_functions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mansionly.qaapp.R.color.red;


public class All_Today_Tasks_Fragment extends Fragment {

    String mMsgStr, mUserIdStr, mOidStr, mEmailIdStr, mWorkOidStr, mUniqueOidStr, mApartmentStr = "", mNameStr = "", mPlannedDateStr = "", mLobTitleStr = "",
            offset_start, mOrderTypeStr, mTotalRecordsStr, mOffsetStr, mLimitStr, mMilestonePlanIdStr = "", mQaActivityName = "", mQASectionOfHouseStr = "", mExecSellerName = "";
    String mCustomerNameStr, mOidStr1, mMilestonePlanIDStr, mDetailsTypeStr, mUserIdStr1, mMasgStr, mMilestonePlanIdStr1,
            mOrderIdStr, mQuoteIdStr, mWorkOrderIdStr, mSectionOfHouseStr, mExecSellernameStr, mPropertyAddressStr, mQAactivityName,
            mUniqueOidStr1, mHistoryDetailsTypeStr, mMilestonePlanId, mFlagInternalWork, mSectionOfHouseID;
    ArrayList<QATask> mQATaskList = new ArrayList<QATask>();
    static ArrayList<Milestone> taskList;
    static Milestone milestone;
    ProgressBar pbTodayTask;
    long totalSize = 0;
    String emailId;
    AppPreferences pref;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView empty_text;
    RecyclerView recyclerView;
    TodayTaskAdpater mAdapter;
    CustomScrollView scrollview;
    View rootView;
    String sortBy;
    String searchText = "";
    View tab;
    TabLayout tabLayout;
    String selectedMenuItem = "QADate";
    TextView tabOne;
    boolean pullToRefresh = false;
    boolean ischecked = true;
    private SqliteDatabaseHandler sqliteDatabaseHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.all_today_activities_list_layout, container, false);
        setHasOptionsMenu(true);
        sqliteDatabaseHandler = new SqliteDatabaseHandler(getActivity());
        pbTodayTask = (ProgressBar) rootView.findViewById(R.id.today_task_progressBar);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        empty_text = (TextView) rootView.findViewById(R.id.empty_text);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        scrollview = (CustomScrollView) rootView.findViewById(R.id.lead_details_scrollview);
        scrollview.setScrollViewListener(leadDetailsScrollViewListener);
        tabLayout = (TabLayout) getActivity().findViewById(R.id.tabs);
        tab = tabLayout.getTabAt(0).getCustomView(); // fo
        tabOne = (TextView) tab.findViewById(R.id.tab);
        taskList = new ArrayList<>();
        pref = new AppPreferences(getActivity());
        mUserIdStr = pref.getUserIdPref();
        mEmailIdStr = pref.getUserEmail();
        //  mOrderTypeStr = "orderExecutionQAChecklist"; //for all orders
        mOrderTypeStr = "orderExecutionQAChecklist";// orders by user ID
        mOffsetStr = "0";
        mLimitStr = "0";
        sortBy = "DATE";

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                swipeRefreshLayout.setRefreshing(false);
                mQATaskList.clear();
                new UpdateListing(false).execute("0");
                pullToRefresh = true;
            }
        });
        new UpdateListing(true).execute("0");

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    CustomScrollView.ScrollViewListener leadDetailsScrollViewListener = new CustomScrollView.ScrollViewListener() {

        @Override
        public void onScrollChanged(CustomScrollView scrollView, int currLen, int currTop, int prevLen, int prevTop) {

            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (diff == 0) {
                if (mOrderTypeStr != null)
                    new UpdateListing(false).execute(mOffsetStr);
                else
                    scrollview.setEnabled(false);
            }

        }
    };

    private class UpdateListing extends AsyncTask<String, Integer, String> {
        private ProgressDialog pd;
        private boolean showLoader;

        public UpdateListing(boolean showLoader) {
            this.showLoader = showLoader;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            if (pullToRefresh == true) {
                pd.dismiss();
                pullToRefresh = false;
            } else {
                pd.setMessage("Loading...");
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                if (showLoader) {
                    pd.show();
                }
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String responseString = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Webservice.TODAY_TASK_LIST);

            try {
                AndroidMultipartEntity entity = new AndroidMultipartEntity(
                        new AndroidMultipartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                mOffsetStr = params[0];
                entity.addPart(AppConsts.QA_USER_ID, new StringBody(mUserIdStr));
                entity.addPart(AppConsts.QA_ORDER_TYPE, new StringBody(mOrderTypeStr));
                entity.addPart(AppConsts.QA_OFFSET, new StringBody(mOffsetStr));
                entity.addPart(AppConsts.QA_LIMIT, new StringBody(mLimitStr));
                entity.addPart(AppConsts.QA_CUSTOMER_EAILID, new StringBody(mEmailIdStr));
                entity.addPart(AppConsts.QA_TASK_SORT_BY, new StringBody(sortBy));
                entity.addPart(AppConsts.QA_TASK_SEARCH, new StringBody(searchText));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();

                responseString = e.toString();
            } catch (IOException e) {
                e.printStackTrace();
                responseString = e.toString();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            try {

                JSONObject jobj = new JSONObject(result);
                JSONObject gg = jobj.getJSONObject("result");
                mMsgStr = gg.getString(DBTableColumnName.DB_QUERY_NUM);
                String msg_string = gg.getString(DBTableColumnName.DB_QUERY_RESULT);

                if (mMsgStr.equals("0")) {
                    if (mQATaskList.isEmpty()) {
                        if (mAdapter != null)
                            mAdapter.clear();
                        empty_text.setVisibility(View.VISIBLE);
                        swipeRefreshLayout.setEnabled(false);
                    } else {
                        // leadDetailList.clear();
                        empty_text.setVisibility(View.GONE);
                    }

                } else if (mMsgStr.equals("1")) {
                    empty_text.setVisibility(View.GONE);
                    if (mOffsetStr.equalsIgnoreCase("0"))
                        mQATaskList.clear();

                    try {

                        //  mProjectList.clear();
                        JSONArray jarray = gg.getJSONArray("arr_record_list");
                        mOffsetStr = gg.getString("offset");
                        mTotalRecordsStr = gg.getString("total_records");

                        for (int i = 0; i < jarray.length(); i++) {
                            JSONObject c = jarray.getJSONObject(i);

                            //if (!sqliteDatabaseHandler.isExistsActivityInLocalDb(c.getString(DBTableColumnName.QA_MILESTONE_ID))) {
                            mOidStr = c.getString(DBTableColumnName.QA_MILESTONE_ORDER_ID);
                            mWorkOidStr = c.getString(DBTableColumnName.QA_WORK_ORDER_ID);
                            mUniqueOidStr = c.getString(DBTableColumnName.QA_UNIQUE_ORDER_ID);
                            mApartmentStr = c.getString(DBTableColumnName.QA_APARTMENT);
                            mNameStr = c.getString(DBTableColumnName.QA_NAME);
                            mQASectionOfHouseStr = c.getString(DBTableColumnName.QA_SECTION_OF_HOUSE);
                            mMilestonePlanIdStr = c.getString(DBTableColumnName.QA_MILESTONE_ID);
                            mPlannedDateStr = c.getString(DBTableColumnName.QA_PLANNED_DATE);
                            mLobTitleStr = c.getString(DBTableColumnName.QA_LOBTITLE);
                            mExecSellerName = c.getString(DBTableColumnName.QA_EXECSELLER_NAME);
                            mQaActivityName = c.getString(DBTableColumnName.QA_ACTIVITY_NAME);
                            mQuoteIdStr = c.getString("quote_id");
                            mSectionOfHouseID = c.getString("section_of_house_id");

                            Map<String, Object> userInfo = new HashMap<String, Object>();
                            userInfo.put(DBTableColumnName.QA_MILESTONE_ORDER_ID, mOidStr);

                            AppPreferences prefs = new AppPreferences(getActivity());
                            prefs.saveUserLoginStatus(true);
                            prefs.saveUserInfo(userInfo);

                            QATask qaTask = new QATask(mOidStr, mUniqueOidStr, mWorkOidStr,
                                    mApartmentStr, mNameStr, mMilestonePlanIdStr,
                                    mQASectionOfHouseStr, mPlannedDateStr, "", mLobTitleStr,
                                    mExecSellerName, mQaActivityName, "", mQuoteIdStr,
                                    mSectionOfHouseID);
                            mQATaskList.add(qaTask);
                            tabOne.setText("TODAY" + " " + "(" + mTotalRecordsStr + ")");
                            swipeRefreshLayout.setRefreshing(false);
                            //}

                        }

                        mAdapter = new TodayTaskAdpater(mQATaskList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                } else if (mMsgStr.equals("550")) {
                    Toast.makeText(getActivity(), "550", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }
            pd.dismiss();
        }
    }

    public class TodayTaskAdpater extends RecyclerView.Adapter<TodayTaskAdpater.MyViewHolder> implements Filterable {

        private List<QATask> qaTaskList;

        @Override
        public Filter getFilter() {
            return null;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tvApartmentAddress, tvCustomerName, tvSectionofHouse, tvPlannedDate, tvPlannedDateVal, tvActivityName;
            ImageView imMilestoneImage;
            Button btnReject, btnApprove;
            LinearLayout llItem;

            public MyViewHolder(View rootView) {
                super(rootView);

                llItem = (LinearLayout) rootView.findViewById(R.id.llItem);
                tvCustomerName = (TextView) rootView.findViewById(R.id.tvCustomerName);
                tvApartmentAddress = (TextView) rootView.findViewById(R.id.tvApartmentAddress);
                tvSectionofHouse = (TextView) rootView.findViewById(R.id.tvSectionofHouse);
                tvPlannedDate = (TextView) rootView.findViewById(R.id.tvPlannedDate);
                tvPlannedDateVal = (TextView) rootView.findViewById(R.id.planned_date_textView);
                tvActivityName = (TextView) rootView.findViewById(R.id.tvActivityName);
                imMilestoneImage = (ImageView) rootView.findViewById(R.id.imMilestoneImage);
                btnReject = rootView.findViewById(R.id.btnReject);
                btnApprove = rootView.findViewById(R.id.btnApprove);

            }
        }

        public TodayTaskAdpater(List<QATask> milestonesList) {
            this.qaTaskList = milestonesList;
        }

        @Override
        public TodayTaskAdpater.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.today_task_listitem, parent, false);

            return new TodayTaskAdpater.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final TodayTaskAdpater.MyViewHolder holder, final int position) {

            final QATask qaTask = qaTaskList.get(position);
            //Check for record exists in local database or not, if exists then hide it from main list

            if (qaTask.getPropertyAddress().equalsIgnoreCase("null") ||
                    qaTask.getPropertyAddress().trim().isEmpty())
                holder.tvApartmentAddress.setText("No address available");
            else
                holder.tvApartmentAddress.setText(qaTask.getPropertyAddress());
            holder.tvCustomerName.setText(qaTask.getCustomerName());
            holder.tvSectionofHouse.setText(qaTask.getQASectionOfHouse());
            holder.tvPlannedDate.setText(Utility_functions.GetFormatedDate(qaTask.getQAPlannedDate()));

            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);

            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            String todayDateStr = format.format(c.getTime());
            String plannedDate = qaTask.getQAPlannedDate();

            Date strDate = null, srtTodydate = null;
            try {
                strDate = format.parse(plannedDate);
                srtTodydate = format.parse(todayDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (srtTodydate.getTime() > strDate.getTime()) {
                holder.tvPlannedDate.setTextColor(getResources().getColor(red));
                holder.tvPlannedDateVal.setTextColor(getResources().getColor(red));
            }

            holder.tvActivityName.setText(qaTask.getQAActivityName());
            String lobTitle = qaTask.getQALobTitle();

            if (lobTitle == null)
                holder.imMilestoneImage.setImageResource(R.mipmap.ic_residential);
            else if (lobTitle.equalsIgnoreCase("Institutional"))
                holder.imMilestoneImage.setImageResource(R.drawable.instituional);
            else if (lobTitle.equalsIgnoreCase("Project"))
                holder.imMilestoneImage.setImageResource(R.mipmap.ic_residential);
            else holder.imMilestoneImage.setImageResource(R.mipmap.ic_residential);

            holder.btnApprove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pref.saveOrderIdPref(qaTask.getOrderId());
                    pref.saveCurrentTaskType("AllTodayTask");
                    mDetailsTypeStr = "milestonePlan";

                    pref.saveQuoteIdpref(qaTask.getmQuoteIdStr());
                    pref.saveMileStoneId(qaTask.getQAMilestoneId());
                    pref.saveUserIdPref(mUserIdStr);
                    pref.saveSectionOfHousepref(qaTask.getQASectionOfHouse());
                    pref.saveSectionOfHouseIDpref(qaTask.getmSectionOfHouseID());
                    pref.saveActivitypref(qaTask.getQAActivityName());

                    Intent intent = new Intent(getActivity(),
                            Task_Approve_Step_1_Activity.class);
                    intent.putExtra("activity_log_status", "1");
                    startActivity(intent);
                }
            });
            holder.btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pref.saveOrderIdPref(qaTask.getOrderId());
                    pref.saveCurrentTaskType("AllTodayTask");
                    mDetailsTypeStr = "milestonePlan";

                    pref.saveQuoteIdpref(qaTask.getmQuoteIdStr());
                    pref.saveMileStoneId(qaTask.getQAMilestoneId());
                    pref.saveUserIdPref(mUserIdStr);
                    pref.saveSectionOfHousepref(qaTask.getQASectionOfHouse());
                    pref.saveSectionOfHouseIDpref(qaTask.getmSectionOfHouseID());
                    pref.saveActivitypref(qaTask.getQAActivityName());

                    Intent intent = new Intent(getActivity(),
                            Task_Reject_Step_1_Activity.class);
                    intent.putExtra("activity_log_status", "1");
                    startActivity(intent);
                }
            });

        }

        public void removeAt(int position) {
            mQATaskList.remove(position);
            notifyItemRemoved(position);
            notifyItemChanged(position);
            notifyItemRangeChanged(position, mQATaskList.size());
        }

        @Override
        public int getItemCount() {
            return qaTaskList.size();
        }

        public void clear() {
            // TODO Auto-generated method stub
            qaTaskList.clear();
            mAdapter.notifyDataSetChanged();

        }


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_sort_menu, menu);

        final MenuItem item = menu.findItem(R.id.search_menuItem);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(taskSearchLstener);
        searchView.setOnCloseListener(searchCloseListener);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        mQATaskList.clear();
                        searchText = "";
                        mOffsetStr = "0";
                        new UpdateListing(true).execute(mOffsetStr);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        SpannableString s;
        menu.findItem(R.id.Title).setEnabled(false);
        if (selectedMenuItem.equals("CustomerName")) {
            MenuItem miCustName = menu.findItem(R.id.SortByCustomerName);
            String miCustNameStr = miCustName.getTitle().toString();
            s = new SpannableString(miCustNameStr);
            s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)),
                    0, s.length(), 0);
            miCustName.setTitle(s);
        } else {
            MenuItem miQADate = menu.findItem(R.id.SortByQADate);
            String miQADateStr = miQADate.getTitle().toString();
            s = new SpannableString(miQADateStr);
            s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)),
                    0, s.length(), 0);
            miQADate.setTitle(s);
        }

        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        } else if (item.getItemId() == R.id.SortByCustomerName) {
            if (ischecked == true) {
                selectedMenuItem = "CustomerName";
                mOffsetStr = "0";
                new UpdateListing(true).execute(mOffsetStr);
                scrollview.scrollTo(0, scrollview.getTop());
                sortBy = "NAME";
                ischecked = false;
            }
        } else if (item.getItemId() == R.id.SortByQADate) {
            if (ischecked == false) {
                selectedMenuItem = "QADate";
                mOffsetStr = "0";
                new UpdateListing(true).execute(mOffsetStr);
                scrollview.scrollTo(0, scrollview.getTop());
                sortBy = "DATE";
                ischecked = true;
            }
        }

        ActivityCompat.invalidateOptionsMenu(getActivity());
        return super.onOptionsItemSelected(item);
    }

    SearchView.OnCloseListener searchCloseListener = new SearchView.OnCloseListener() {
        @Override
        public boolean onClose() {
            mQATaskList.clear();
            searchText = "";
            mOffsetStr = "0";
            new UpdateListing(true).execute(mOffsetStr);
            return false;
        }
    };

    SearchView.OnQueryTextListener taskSearchLstener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            mQATaskList.clear();
            query = query.toLowerCase();
            searchText = query;
            mOffsetStr = "0";
            new UpdateListing(true).execute(mOffsetStr);

            InputMethodManager inputMethodManager = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }

    };

/*
    public class ActionLogListing extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        String navgiation, uid, milestoneorderid, milestoneid;

        ActionLogListing(String navgiation, String uid, String milestoneid, String milestoneorderid) {
            this.navgiation = navgiation;
            this.uid = uid;
            this.milestoneid = milestoneid;
            this.milestoneorderid = milestoneorderid;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pd == null) {
                pd = new ProgressDialog(getActivity());
                pd.setMessage("Loading...");
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... args0) {
            try {
                String link = Webservice.ORDERDETAILS;
                String data = URLEncoder.encode(DBTableColumnName.USER_ID, "UTF-8") + "=" + URLEncoder.encode(uid, "UTF-8");
                data += "&" + URLEncoder.encode(DBTableColumnName.QA_MILESTONE_ORDER_ID, "UTF-8") + "=" + URLEncoder.encode(milestoneorderid, "UTF-8");
                data += "&" + URLEncoder.encode("details_type", "UTF-8") + "=" + URLEncoder.encode(mDetailsTypeStr, "UTF-8");
                data += "&" + URLEncoder.encode(DBTableColumnName.QA_MILESTONE_ID, "UTF-8") + "=" + URLEncoder.encode(milestoneid, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter
                        (conn.getOutputStream());
                wr.write(data);
                wr.flush();
                BufferedReader reader = new BufferedReader
                        (new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }
                return sb.toString();
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("result", result);

            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject gg = jobj.getJSONObject("result");
                mMasgStr = gg.getString("msg");

                if (mMasgStr.equals("0")) {
                    Toast.makeText(getActivity(), (String) "msg : 1",
                            Toast.LENGTH_LONG).show();

                } else if (mMasgStr.equals("1")) {
                    taskList.clear();

                    mUniqueOidStr = gg.getString("unique_oid");
                    mOidStr = gg.getString("o_id");
                    mCustomerNameStr = gg.getString("customer_name");
                    // mPropertyAddressStr = gg.getString("customer_address");

                    JSONArray jarray = gg.getJSONArray("arrMilestonePlanDtls");

                    try {

                        for (int i = 0; i < jarray.length(); i++) {
                            JSONObject c = jarray.getJSONObject(i);
                            mOrderIdStr = c.getString("order_id");
                            mQuoteIdStr = c.getString("quote_id");
                            mWorkOrderIdStr = c.getString("unique_workorder_id");
                            mMilestonePlanId = c.getString("milestone_plan_id");
                            mSectionOfHouseStr = c.getString("section_of_house_title");
                            mExecSellernameStr = c.getString("executioner_seller_name");
                            mQAactivityName = c.getString("activity");
                            mFlagInternalWork = c.getString("flag_internal_workpart");
                            mSectionOfHouseID = c.getString("section_of_house_id");

                            milestone = new Milestone(mOrderIdStr, mQuoteIdStr, mMilestonePlanId,
                                    mWorkOrderIdStr, mSectionOfHouseStr, mExecSellernameStr,
                                    mQAactivityName, mFlagInternalWork, mSectionOfHouseID);
                            taskList.add(milestone);
                        }

                        pref.saveQuoteIdpref(milestone.getQuote_id());
                        pref.saveMileStoneId(milestone.getMilestonePlanId());
                        pref.saveUserIdPref(mUserIdStr);
                        pref.saveSectionOfHousepref(milestone.getSection_house());
                        pref.saveSectionOfHouseIDpref(milestone.getmSectionOfHouseID());
                        pref.saveActivitypref(milestone.getActivity());
                        pref.saveFlagInternalWork(milestone.getmFlagInternalWork());
                        String milestonePlanIdStr = All_Today_Tasks_Fragment.milestone.getMilestonePlanId();
                        pref.saveMileStoneId(milestonePlanIdStr);
                        Intent intent;
                        if (navgiation.equals("Approve")) {
                            intent = new Intent(getActivity(),
                                    Task_Approve_Step_1_Activity.class);
                        } else {
                            intent = new Intent(getActivity(),
                                    Task_Reject_Step_1_Activity.class);
                        }

                        //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.putExtra(DBTableColumnName.QA_SECTION_OF_HOUSE, milestone.getSection_house());
                        intent.putExtra(DBTableColumnName.QA_ACTIVITY_NAME, milestone.getActivity());
                        intent.putExtra("activity_log_status", "1");
                        startActivity(intent);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // progressBar.setVisibility(View.GONE);
                    pd.dismiss();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
*/
}
