package com.mansionly.qaapp.client;

public class Webservice {

    public static String SERVER_NAME = "https://mansionly.com/selfserve/" ;
   // public static String SERVER_NAME = "http://13.250.6.210/selfserve/";

    public static String UPLOAD_MILESTONE_DATA_IMAGES_BACKGROUND = SERVER_NAME.concat("webapp/qa-app/ws-orders/ws-tmp-upload-orderMilestonePlanActivityQAImg-upload-action.php");

    public static String SOCIAL_LOGIN = SERVER_NAME.concat("webapp/sales/ws-login/ws-login-with-google-mansionly-email-id-action.php");

    /*Login
     *Input Params :“username”,“password"*/
    public static String LOGIN = SERVER_NAME.concat("webapp/qa-app/ws-login/ws-login.php");

    /*Forgot Password
     *Input Params :“email”*/
    public static String FORGOT_PASSWORD = SERVER_NAME + "webapp/qa-app/ws-login/ws-forgot.php";

    /*Change Password
     *Input Params:“user_id”,“old_pass”,“new_pass”*/
    public static String CHANGE_PASSWORD = SERVER_NAME + "webapp/qa-app/ws-login/ws-change-" +
            "password.php";

    /*Today's Task Lst for All and My Task
    *Input Params:“user_id”,“ordtype” = “myQACheckList/orderExecutionQAChecklist”,“offset”,“limit”,
    “email”,“sort_by” (NAME/DATE),“search_word”*/
    public static String TODAY_TASK_LIST = SERVER_NAME + "webapp/qa-app/ws-orders/ws-order-" +
            "today-activity-list.php";

    /*Get Task Details (History, All, My and Rejected Task Details)
     *Input Params:“user_id”,“o_id”,“details_type” = “milestonePlan”,“milestone_plan_id”
     */
    public static String ORDERDETAILS = SERVER_NAME + "webapp/qa-app/ws-orders/ws-get-qa-order" +
            "-details.php";

    /*Upcoming Task Lst for All and My Task
     *Input Params:“user_id”,“ordtype” = “myQACheckList/orderExecutionQAChecklist”,“offset”,“limit”,
      “email”,“sort_by” (NAME/DATE),“search_word”*/
    public static String UPCOMING_TASK_LIST = SERVER_NAME + "webapp/qa-app/ws-orders/ws-order" +
            "-upcoming-activity-list_calender_date_wise.php";

    /*Change Task Status Action (Approve/Reject)
    *Input Params :“user_id”,“o_id”,“user_email”,“quote_id”,“milestone_plan_id”,“activity_log_status”
    = (Approve=”1”/Reject=”0”),“plan_revised_date”,“qa_comment”,“img_ids”
    *Change Rejected Task Status Action to Approve
    *Input Params:“user_id”,“o_id”,“user_email”,“quote_id”,“milestone_plan_id”,“activity_log_status”,
    “log_status” = “Approved”,“qa_comment”
    *Set Rejected Task’s Revised Date
    *Input Params:“user_id”,“o_id”,“user_email”,“quote_id”,“milestone_plan_id”,“activity_log_status” = “0”,
    “plan_revised_date”,“qa_comment”
    */
    public static String TASK_STATUS_CHANGE = SERVER_NAME + "webapp/qa-app/ws-orders/ws-order" +
            "-milestone-plan-activity-status-action.php";

    /*Get History Log (My, All, Rejected, Previous Task history Log)
     *Input Params:“user_id”,“o_id”,“milestone_plan_id”,“details_type” = “orderMilestonePlanActivityHistory”
     */
    public static String HISTORY_LOG = SERVER_NAME + "webapp/qa-app/ws-orders/ws-get-qa-order" +
            "-history-details.php";

    public static String APP_VERSION_INFO = SERVER_NAME + "webapp/qa-app/ws-orders/ws-app-" +
            "version-info.php";

    /*Add Image
    *Input Params:“user_id”,“milestone_plan_id”,“flag_img_post” = (addImg),“orderQAImages”,
    “picComment”,“picLable”,“flag_customer_viewable” = (1/0)
    *Delete Image
    *Input Params:“user_id”,“milestone_plan_id”,“flag_img_post” = (delImg),“img_ids”
    */
    public static String MILESTONE_PIC_ADD_REMOVE_ACTION = SERVER_NAME + "webapp/qa-app/ws-orders/" +
            "ws-orderMilestonePlanActivityQAImgAddRemoveAction.php";

    /*Get History List For My and All Task
     *Input Params:“user_id”,“ordtype” = “myQACheckList/""”,"offset”,“limit”,“email”,
     *“sort_by” =(NAME/DATE),“search_word”,“sort_type” = “DESC” if sort_by= “DATE”*/
    public static String TASK_HISTORY_LIST = SERVER_NAME + "webapp/qa-app/ws-orders/ws-order-" +
            "history-activity-list_calender_date_wise.php";

    /*Get Rejected List
     *Input Params:“user_id”,“ordtype” = “”,“offset”,“limit”,“email”,“sort_by” (NAME/DATE),“search_word”,
     *“sort_type” = “DESC” if sort_by= “DATE”
     */
    public static String REJECTED_TASK_LIST = SERVER_NAME + "webapp/qa-app/ws-orders/ws-order-" +
            "rejected-activity-list_calender_date_wise.php";

    /*Get Project Task List
     *Input Params:“user_id”,“sort_by” (NAME/DATE),“search_word”,“offset”,“limit”*/
    public static String GET_PROJECT_LIST = SERVER_NAME + "webapp/qa-app/ws-orders/ws-qa-app-get-" +
            "project-list.php";

    /*Project Add Image
    *Input Params:“user_id”,“order_id”,“flag_img_post” = (addImg),“projectSiteImages”,
    “flag_customer_viewable” = (1/0)
    * Project Remove Image
    * Input Params	“user_id”,“order_id”,“flag_img_post” = (delImg),“img_ids”
    */
    public static String PROJECT_ADD_REMOVE_IMAGES = SERVER_NAME + "webapp/qa-app/ws-orders/ws-qa-" +
            "app-add-remove-project-site-pic.php";

    /*Project Site Gallery Images
     *Input Params:“user_id”,“order_id”
     */
    public static String PROJECT_SITE_GALLERY_IMAGES = SERVER_NAME + "webapp/qa-app/ws-orders/ws-qa" +
            "-app-project-site-pic-gallery.php";

    /*Site Gallery Add Images
     *Input Params:“user_id”,“order_id”,“flag_img_post” = “Upload”,“img_ids”
     */
    public static String PROJECT_UPLOAD_CANCEL_IMAGES = SERVER_NAME + "webapp/qa-app/ws-orders/ws" +
            "-qa-app-upload-project-site-pic.php";

    /*Site Gallery Remove/Update Images
     *Input Params:“user_id”,“order_id”,“img_ids”,“flag_customer_viewable”
     */
    public static String PROJECT_IMAGES_UPDATE_DETAILS = SERVER_NAME + "webapp/qa-app/ws-orders/ws" +
            "-qa-app-update-project-site-pic-details.php";
}