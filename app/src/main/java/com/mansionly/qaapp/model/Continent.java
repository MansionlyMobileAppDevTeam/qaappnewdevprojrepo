package com.mansionly.qaapp.model;

import java.util.ArrayList;

public class Continent {
    private String name;
    private ArrayList<QATask> mChildList = new ArrayList<QATask>();

    public Continent(String headerTitle, ArrayList<QATask> childList) {
        name=headerTitle;
        mChildList =childList;
    }

    public ArrayList<QATask> getChildList() {
        return mChildList;
    }

    public String getHeaderName() {
        return name;
    }
}
