package com.mansionly.qaapp.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mansionly.qaapp.App;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.AndroidMultipartEntity;
import com.mansionly.qaapp.client.Client;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.Utility_functions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SiteGallery_Upload_Pic_Detail_Activity extends AppCompatActivity {
    String picFilePath, mUseridStr, mO_idStr, mCustomerNameStr;
    Toolbar toolbar;
    LinearLayout llNoInternet;
    AppPreferences pref;
    Bitmap milestonePicBmp;
    ImageView ivMilestonePic;
    TextView btnAdd, btnCancel;
    File tempFile;
    File picFile;
    CheckBox checkBox;
    ArrayList<File> picFileList = new ArrayList<File>();
    String customerViewableStatus;
    Map<String, String> milestoneViewableCheckMap = new HashMap<String, String>();
    private Map<String, String> milestonePicIdMap = new HashMap<String, String>();
    ArrayList<String> localTaskPicIdList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sitegallery_upload_pic);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        llNoInternet = (LinearLayout) findViewById(R.id.internetCheck);
        new Handler().post(internetCheck);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        pref = new AppPreferences(getApplicationContext());
        mUseridStr = pref.getUserIdPref();
        mO_idStr = pref.getOrderIdPref();
        mCustomerNameStr = pref.getCustomerName();

        getSupportActionBar().setTitle(mCustomerNameStr);
        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + "ADD IMAGES" + "</small>"));

        ivMilestonePic = (ImageView) findViewById(R.id.milestone_pic_imageView);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        btnAdd = (TextView) findViewById(R.id.btnAdd);
        btnCancel = (TextView) findViewById(R.id.btnCancel);

        picFilePath = getIntent().getExtras().getString("picFilePath");
        milestonePicBmp = App.getCurrentImage();

        Glide.with(this).load(picFilePath)
                .apply(new RequestOptions().fitCenter())
                .into(ivMilestonePic);

//        ivMilestonePic.setImageBitmap(milestonePicBmp);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkBox.isChecked())
                    customerViewableStatus = "1";
                else
                    customerViewableStatus = "0";

                try {
                    CreateMilestoteUploadTempFile();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new AddMilestonePicDataTask().execute();

            }
        });
    }

    Runnable internetCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isOnline = Utility_functions.internetConnected(getApplicationContext());
            if (isOnline) {
                llNoInternet.setVisibility(View.GONE);
            } else {
                llNoInternet.setVisibility(View.VISIBLE);
            }
            new Handler().postDelayed(internetCheck, 5000);
        }
    };

    private void CreateMilestoteUploadTempFile() throws Exception {
        File picFolderInSDCard = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()
                + File.separator + "Mansionly" + File.separator + "QAApp"
                + File.separator + "MansionlyProjectAlbum");
        if (!picFolderInSDCard.exists()) {
            try {
                if (picFolderInSDCard.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            // picBmp = Utility_functions.compressImage(picFilePath);
            File picFile = Utility_functions.GeneratePicFileFromBmp(picFilePath,
                    milestonePicBmp, 80);
//            picFileList.add(picFile);

            tempFile = File.createTempFile("album" + System.currentTimeMillis() + "_", /* prefix */
                    ".jpg", /* suffix */
                    picFolderInSDCard); /* directory */
            Utility_functions.CopyFile(picFile, tempFile);

            picFile = tempFile;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public class AddMilestonePicDataTask extends AsyncTask<Void, Void, String> {
        ProgressDialog asyncDialog = new ProgressDialog(SiteGallery_Upload_Pic_Detail_Activity.this);

        protected void onPreExecute() {

            asyncDialog.setMessage("Uploading...");
            asyncDialog.setCancelable(false);
            asyncDialog.setCanceledOnTouchOutside(false);
            //show dialog
            asyncDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String url = Webservice.PROJECT_ADD_REMOVE_IMAGES;

            String responseString = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            try {
                AndroidMultipartEntity entity = new AndroidMultipartEntity(
                        new AndroidMultipartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                            }
                        });

                picFile = new File(picFilePath);

                entity.addPart(DBTableColumnName.USER_ID, new StringBody(mUseridStr));
                entity.addPart(DBTableColumnName.ORDER_ID, new StringBody(mO_idStr));
                entity.addPart(DBTableColumnName.QA_MILESTONE_PIC_ADD_REMOVE_FLAG, new StringBody("addImg"));
                entity.addPart(DBTableColumnName.PROJECT_SITE_IMAGES, new FileBody(picFile));
                entity.addPart(DBTableColumnName.QA_IMAGE_CUSTOMER_VIEWABLE, new StringBody(customerViewableStatus));
                httppost.setEntity(entity);
                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
                e.printStackTrace();
            } catch (IOException e) {
                responseString = e.toString();
                e.printStackTrace();
            }
            return responseString;
        }

        protected void onPostExecute(String result) {
            asyncDialog.dismiss();

            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject jsonResult = jobj.getJSONObject("result");
                String msg = jsonResult.getString("msg");

                if (msg.equals("0")) {
                    Utility_functions.showToastOnMainThread(SiteGallery_Upload_Pic_Detail_Activity.this,
                            "Failed to delete image.");
                } else if (msg.equals("1")) {
                    try {
                        String picId = jsonResult.getString(DBTableColumnName.QA_ACTIVITY_PIC_IDS);
                        // milestonePicList.add(picId);

                        Intent intent = new Intent(SiteGallery_Upload_Pic_Detail_Activity.this, SiteGallery_Add_Image_Activity.class);
                        intent.putExtra("o_id", mO_idStr);
                        intent.putExtra("userid", mUseridStr);

                        milestoneViewableCheckMap = pref.getMilestoneViewCheckMapPref();
                        milestoneViewableCheckMap.put(picId, customerViewableStatus);
                        pref.saveMilestoneViewCheckMapPref(milestoneViewableCheckMap);

                        milestonePicIdMap = pref.getMilestonePicIdMapPref();
                        milestonePicIdMap.put(milestonePicIdMap.size() + "", picId);
                        pref.saveMilestonePicIdMapPref(milestonePicIdMap);

                        HashMap<String, String> serverIdToFileNameMap =
                                pref.getServerIdToFileNameMap();
                        serverIdToFileNameMap.put(picId, tempFile.getName());
                        pref.saveServerIdToFileNameMap(serverIdToFileNameMap);

                        localTaskPicIdList = pref.getTaskLocalPicIdPref();
                        localTaskPicIdList.add(0, "" + (milestonePicIdMap.size() - 1));
                        pref.saveTaskLocalPicIdPref(localTaskPicIdList);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        App.setCapturedImageAbsolutePath(null);
                        App.setCurrentImage(null);

                        startActivity(intent);

                        Toast toast = Toast.makeText(SiteGallery_Upload_Pic_Detail_Activity.this, "Image added successfully", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        App.setCapturedImageAbsolutePath(null);
        App.setCurrentImage(null);
        super.onDestroy();
    }
    @Override
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    try {
                        final int thisVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        Client client = Client.getClientInstance(getApplicationContext() , Webservice.APP_VERSION_INFO);
                        boolean status = client.isAppUpdateRequired(Integer.toString(thisVersion));
                        if (status) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Version_control_dialog_fragment dialog = Version_control_dialog_fragment.newInstance();
                                    dialog.show(getSupportFragmentManager(), "Version_control_dialog_fragment");
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }
}
