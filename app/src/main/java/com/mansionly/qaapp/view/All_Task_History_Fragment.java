package com.mansionly.qaapp.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mansionly.qaapp.R;
import com.mansionly.qaapp.client.AndroidMultipartEntity;
import com.mansionly.qaapp.client.DBTableColumnName;
import com.mansionly.qaapp.client.Webservice;
import com.mansionly.qaapp.model.QATask;
import com.mansionly.qaapp.util.AppConsts;
import com.mansionly.qaapp.util.AppPreferences;
import com.mansionly.qaapp.util.CustomScrollView;
import com.mansionly.qaapp.util.Utility_functions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mansionly.qaapp.R.color.red;

public class All_Task_History_Fragment extends Fragment {

    String mMsgStr, mUserIdStr, mOidStr, mEmailIdStr, mWorkOidStr, mUniqueOidStr, mApartmentStr = "", mNameStr = "", mPlannedDateStr = "", mActualCompletionDateStr = "", mLobTitleStr = "",
            mOrderTypeStr, mTotalRecordsStr, mOffsetStr, mLimitStr, mMilestonePlanIdStr = "", mQaActivityName = "", mQASectionOfHouseStr = "", mExecSellerName = "";

    ArrayList<QATask> mQATaskList = new ArrayList<QATask>();
    ProgressBar pbTodayTask;
    long totalSize = 0;
    View rootView;
    AppPreferences pref;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView empty_text;
    RecyclerView recyclerView;
    TaskHistoryAdpater mAdapter;
    CustomScrollView scrollview;
    List<Integer> plannedDateChangesPosList;
    String sortBy;
    String searchText = "";
    List<Integer> customerNameChangesPosList;
    View tab;
    TabLayout tabLayout;
    String selectedMenuItem = "QADate";
    TextView tabThree;
    boolean pullToRefresh = false;
    boolean ischecked = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.my_task_history_list, container, false);
        setHasOptionsMenu(true);

        pbTodayTask = (ProgressBar) rootView.findViewById(R.id.today_task_progressBar);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        empty_text = (TextView) rootView.findViewById(R.id.empty_text);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        scrollview = (CustomScrollView) rootView.findViewById(R.id.lead_details_scrollview);
        scrollview.setScrollViewListener(leadDetailsScrollViewListener);
        tabLayout = (TabLayout) getActivity().findViewById(R.id.tabs);
        tab = tabLayout.getTabAt(2).getCustomView(); // fo
        tabThree = (TextView) tab.findViewById(R.id.tab);

        plannedDateChangesPosList = new ArrayList<>();
        customerNameChangesPosList = new ArrayList<>();

        pref = new AppPreferences(getActivity());
        mUserIdStr = pref.getUserIdPref();
        mEmailIdStr = pref.getUserEmail();
        //  mOrderTypeStr = "orderExecutionQAChecklist"; //for all orders
        mOrderTypeStr = "";
        mOffsetStr = "0";
        mLimitStr = "0";
        sortBy = "DATE";

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                swipeRefreshLayout.setRefreshing(false);
                mQATaskList.clear();
                new UpdateListing().execute("0");
                pullToRefresh = true;
            }
        });
        new UpdateListing().execute("0");

        return rootView;
    }

    CustomScrollView.ScrollViewListener leadDetailsScrollViewListener = new CustomScrollView.ScrollViewListener() {

        @Override
        public void onScrollChanged(CustomScrollView scrollView, int currLen, int currTop, int prevLen, int prevTop) {
            plannedDateChangesPosList.clear();
            customerNameChangesPosList.clear();

            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (diff == 0) {
                if (mOrderTypeStr != null)
                    new UpdateListing().execute(mOffsetStr);
                else
                    scrollview.setEnabled(false);

            }

        }
    };

    private class UpdateListing extends AsyncTask<String, Integer, String> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            if (pullToRefresh == true) {
                pd.dismiss();
                pullToRefresh = false;
            } else {
                pd.setMessage("Loading...");
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {


            String responseString = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Webservice.TASK_HISTORY_LIST);

            try {
                AndroidMultipartEntity entity = new AndroidMultipartEntity(
                        new AndroidMultipartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                mOffsetStr = params[0];

                entity.addPart(AppConsts.QA_USER_ID, new StringBody(mUserIdStr));
                entity.addPart(AppConsts.QA_ORDER_TYPE, new StringBody(mOrderTypeStr));
                entity.addPart(AppConsts.QA_OFFSET, new StringBody(mOffsetStr));
                entity.addPart(AppConsts.QA_LIMIT, new StringBody(mLimitStr));
                entity.addPart(AppConsts.QA_CUSTOMER_EAILID, new StringBody(mEmailIdStr));
                entity.addPart(AppConsts.QA_TASK_SORT_BY, new StringBody(sortBy));
                entity.addPart(AppConsts.QA_TASK_SEARCH, new StringBody(searchText));
                if (sortBy.equalsIgnoreCase("DATE"))
                    entity.addPart(AppConsts.QA_TASK_SORT_TYPE, new StringBody("DESC"));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();

                responseString = e.toString();
            } catch (IOException e) {
                e.printStackTrace();
                responseString = e.toString();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            try {

                JSONObject jobj = new JSONObject(result);
                JSONObject gg = jobj.getJSONObject("result");
                mMsgStr = gg.getString(DBTableColumnName.DB_QUERY_NUM);
                String msg_string = gg.getString(DBTableColumnName.DB_QUERY_RESULT);

                if (mMsgStr.equals("0")) {
                    if (mQATaskList.isEmpty()) {
                        if (mAdapter != null)
                            mAdapter.clear();
                        empty_text.setVisibility(View.VISIBLE);
                        swipeRefreshLayout.setEnabled(false);
                    } else {
                        // leadDetailList.clear();
                        empty_text.setVisibility(View.GONE);
                    }

                } else if (mMsgStr.equals("1")) {
                    empty_text.setVisibility(View.GONE);

                    if (mOffsetStr.equalsIgnoreCase("0"))
                        mQATaskList.clear();

                    try {

                        JSONArray jarray = gg.getJSONArray("arr_record_list");
                        mOffsetStr = gg.getString("offset");
                        mTotalRecordsStr = gg.getString("total_records");

                        for (int i = 0; i < jarray.length(); i++) {
                            JSONObject eachTask = jarray.getJSONObject(i);
                            mOidStr = eachTask.getString(DBTableColumnName.QA_MILESTONE_ORDER_ID);
                            mWorkOidStr = eachTask.getString(DBTableColumnName.QA_WORK_ORDER_ID);
                            mUniqueOidStr = eachTask.getString(DBTableColumnName.QA_UNIQUE_ORDER_ID);
                            mApartmentStr = eachTask.getString(DBTableColumnName.QA_APARTMENT);
                            mNameStr = eachTask.getString(DBTableColumnName.QA_NAME);
                            mQASectionOfHouseStr = eachTask.getString(DBTableColumnName.QA_SECTION_OF_HOUSE);
                            mMilestonePlanIdStr = eachTask.getString(DBTableColumnName.QA_MILESTONE_ID);
                            mPlannedDateStr = eachTask.getString(DBTableColumnName.QA_PLANNED_DATE);
                            mActualCompletionDateStr = eachTask.getString(DBTableColumnName.QA_ACTUAL_COMPLETION_DATE);
                            mLobTitleStr = eachTask.getString(DBTableColumnName.QA_LOBTITLE);
                            mExecSellerName = eachTask.getString(DBTableColumnName.QA_EXECSELLER_NAME);
                            mQaActivityName = eachTask.getString(DBTableColumnName.QA_ACTIVITY_NAME);

                            Map<String, Object> userInfo = new HashMap<String, Object>();
                            userInfo.put(DBTableColumnName.QA_MILESTONE_ORDER_ID, mOidStr);

                            AppPreferences prefs = new AppPreferences(getActivity());
                            prefs.saveUserLoginStatus(true);
                            prefs.saveUserInfo(userInfo);

                            QATask qaTask = new QATask(mOidStr, mUniqueOidStr, mWorkOidStr,
                                    mApartmentStr, mNameStr, mMilestonePlanIdStr,
                                    mQASectionOfHouseStr, mPlannedDateStr, mActualCompletionDateStr, mLobTitleStr,
                                    mExecSellerName, mQaActivityName, "", "",
                                    "");
                            mQATaskList.add(qaTask);
                            tabThree.setText("HISTORY" + " " + "(" + mTotalRecordsStr + ")");
                            swipeRefreshLayout.setRefreshing(false);
                        }

                        if (sortBy == "DATE") {
                            for (int i = 0; i < mQATaskList.size(); i++) {
                                if (mQATaskList.size() >= 2) {
                                    if (i > 0) {
                                        if (!mQATaskList.get(i).getQAPlannedDate().equals(mQATaskList.get(i - 1).getQAPlannedDate())) {
                                            plannedDateChangesPosList.add(i);
                                        }
                                    } else plannedDateChangesPosList.add(0);
                                }
                            }
                        }

                        if (sortBy == "NAME") {
                            for (int i = 0; i < mQATaskList.size(); i++) {
                                if (mQATaskList.size() >= 2) {
                                    if (i > 0) {
                                        String customerNameAti = String.valueOf(mQATaskList.get(i).getCustomerName().charAt(0)).toLowerCase();
                                        String customerNamediff = String.valueOf(mQATaskList.get(i - 1).getCustomerName().charAt(0)).toLowerCase();

                                        if (!customerNameAti.equals(customerNamediff)) {
                                            customerNameChangesPosList.add(i);
                                        }
                                    } else customerNameChangesPosList.add(0);
                                }
                            }
                        }

                        mAdapter = new TaskHistoryAdpater(mQATaskList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                } else if (mMsgStr.equals("550")) {
                    Toast.makeText(getActivity(), "550", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }
            pd.dismiss();
        }
    }

    public class TaskHistoryAdpater extends RecyclerView.Adapter<TaskHistoryAdpater.MyViewHolder> {

        private List<QATask> qaTaskList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            FrameLayout flPlannedDateTitle;
            FrameLayout flCustomerInitialLetter;
            TextView tvPlannedDateTitle, tvCustomerInitialLetter, tvApartmentAddress, tvCustomerName, tvSectionofHouse, tvPlannedDateVal, tvPlannedDate, tvActivityName, tvActualCompletionDate;
            ImageView imMilestoneImage;
            View query_view;

            public MyViewHolder(View rootView) {
                super(rootView);

                flPlannedDateTitle = (FrameLayout) rootView.findViewById(R.id.planned_date_title_frameLayout);
                tvPlannedDateTitle = (TextView) rootView.findViewById(R.id.planned_date_title_textView);

                flCustomerInitialLetter = (FrameLayout) rootView.findViewById(R.id.customer_name_initial_letter_frameLayout);
                tvCustomerInitialLetter = (TextView) rootView.findViewById(R.id.customer_name_initial_letter_title_textView);

                tvCustomerName = (TextView) rootView.findViewById(R.id.tvCustomerName);
                tvApartmentAddress = (TextView) rootView.findViewById(R.id.tvApartmentAddress);
                tvSectionofHouse = (TextView) rootView.findViewById(R.id.tvSectionofHouse);
                tvPlannedDate = (TextView) rootView.findViewById(R.id.tvPlannedDate);
                tvActualCompletionDate = (TextView) rootView.findViewById(R.id.tvActualCompletionDate);
                tvActivityName = (TextView) rootView.findViewById(R.id.tvActivityName);
                imMilestoneImage = (ImageView) rootView.findViewById(R.id.imMilestoneImage);
                query_view = (View) rootView.findViewById(R.id.query_view);
                tvPlannedDateVal = (TextView) rootView.findViewById(R.id.planned_date_textView);
            }
        }

        public TaskHistoryAdpater(List<QATask> milestonesList) {
            this.qaTaskList = milestonesList;
        }

        @Override
        public TaskHistoryAdpater.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.task_detail_listitem, parent, false);

            return new TaskHistoryAdpater.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final TaskHistoryAdpater.MyViewHolder holder, final int position) {

            final QATask qaTask = qaTaskList.get(position);

            if (qaTask.getPropertyAddress().equalsIgnoreCase("null") ||
                    qaTask.getPropertyAddress().trim().isEmpty())
                holder.tvApartmentAddress.setText("No address available");
            else
                holder.tvApartmentAddress.setText(qaTask.getPropertyAddress());
            holder.tvCustomerName.setText(qaTask.getCustomerName());
            holder.tvSectionofHouse.setText(qaTask.getQASectionOfHouse());
            holder.tvPlannedDate.setText(Utility_functions.GetFormatedDate(qaTask.getQAPlannedDate()));
            if (qaTask.getActualCompletionDate().length() > 0) {
                holder.tvActualCompletionDate.setVisibility(View.VISIBLE);
                holder.tvActualCompletionDate.setText(Utility_functions.GetFormatedDate(qaTask.getActualCompletionDate()));

            } else {
                holder.tvActualCompletionDate.setVisibility(View.GONE);
            }
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);

            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            String todayDateStr = format.format(c.getTime());
            String plannedDate = qaTask.getQAPlannedDate();

            Date strDate = null, srtTodydate = null;
            try {
                strDate = format.parse(plannedDate);
                srtTodydate = format.parse(todayDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (srtTodydate.getTime() > strDate.getTime()) {
                holder.tvPlannedDate.setTextColor(getResources().getColor(red));
                holder.tvPlannedDateVal.setTextColor(getResources().getColor(red));
            }

            if (sortBy == "DATE") {
                holder.flCustomerInitialLetter.setVisibility(View.GONE);

                if (plannedDateChangesPosList.contains(position)) {
                    holder.flPlannedDateTitle.setVisibility(View.VISIBLE);
                    holder.query_view.setVisibility(View.GONE);
                    holder.tvPlannedDateTitle.setText(Utility_functions.GetFormatedDate(qaTask.getQAPlannedDate()));
                } else {
                    holder.flPlannedDateTitle.setVisibility(View.GONE);
                }
            }

            if (sortBy == "NAME") {
                holder.flPlannedDateTitle.setVisibility(View.GONE);

                if (customerNameChangesPosList.contains(position)) {
                    String custNameInitial = qaTask.getCustomerName().substring(0, 1).toUpperCase();
                    holder.flCustomerInitialLetter.setVisibility(View.VISIBLE);
                    holder.query_view.setVisibility(View.GONE);
                    holder.tvCustomerInitialLetter.setText(custNameInitial);
                } else {
                    holder.flCustomerInitialLetter.setVisibility(View.GONE);
                }
            }


            holder.tvActivityName.setText(qaTask.getQAActivityName());
            String lobTitle = qaTask.getQALobTitle();

            if (lobTitle == null)
                holder.imMilestoneImage.setVisibility(View.INVISIBLE);
            else if (lobTitle.equalsIgnoreCase("Institutional"))
                holder.imMilestoneImage.setImageResource(R.drawable.instituional);
            else if (lobTitle.equalsIgnoreCase("Project"))
                holder.imMilestoneImage.setImageResource(R.mipmap.ic_residential);
            else holder.imMilestoneImage.setVisibility(View.INVISIBLE);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pref.saveOrderIdPref(qaTask.getOrderId());
                    pref.saveCurrentTaskType("AllPreviousTask");
                    Intent intent = new Intent(getActivity(), Previous_Task_detail_activity.class);
                    intent.putExtra(AppConsts.QA_ORDER_ID, qaTask.getOrderId());
                    intent.putExtra(AppConsts.QA_WORK_ORDER_ID, qaTask.getWorkOrderId());
                    intent.putExtra(AppConsts.QA_MILESTONE_ID, qaTask.getQAMilestoneId());
                    intent.putExtra(AppConsts.QA_CUSTOMER_NAME, qaTask.getCustomerName());
                    intent.putExtra(AppConsts.QA_PROPERTY_ADDRESS, qaTask.getPropertyAddress());

                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return qaTaskList.size();
        }

        public void clear() {
            // TODO Auto-generated method stub
            qaTaskList.clear();
            mAdapter.notifyDataSetChanged();

        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_sort_menu, menu);

        final MenuItem item = menu.findItem(R.id.search_menuItem);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(taskSearchLstener);
        searchView.setOnCloseListener(searchCloseListener);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        mQATaskList.clear();
                        searchText = "";
                        new UpdateListing().execute("0");
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        SpannableString s;
        menu.findItem(R.id.Title).setEnabled(false);
        if (selectedMenuItem.equals("CustomerName")) {
            MenuItem miCustName = menu.findItem(R.id.SortByCustomerName);
            String miCustNameStr = miCustName.getTitle().toString();
            s = new SpannableString(miCustNameStr);
            s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), 0, s.length(), 0);
            miCustName.setTitle(s);
        } else {
            MenuItem miQADate = menu.findItem(R.id.SortByQADate);
            String miQADateStr = miQADate.getTitle().toString();
            s = new SpannableString(miQADateStr);
            s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), 0, s.length(), 0);
            miQADate.setTitle(s);
        }

        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        } else if (item.getItemId() == R.id.SortByCustomerName) {
            if (ischecked == true) {
                selectedMenuItem = "CustomerName";
                mOffsetStr = "0";
                new UpdateListing().execute(mOffsetStr);
                sortBy = "NAME";
                scrollview.scrollTo(0, View.FOCUS_UP);
                ischecked = false;
            }

        } else if (item.getItemId() == R.id.SortByQADate) {
            if (ischecked == false) {
                selectedMenuItem = "QADate";
                mOffsetStr = "0";
                new UpdateListing().execute(mOffsetStr);
                sortBy = "DATE";
                scrollview.scrollTo(0, View.FOCUS_UP);
                ischecked = true;
            }

        }

        ActivityCompat.invalidateOptionsMenu(getActivity());
        return super.onOptionsItemSelected(item);
    }

    SearchView.OnCloseListener searchCloseListener = new SearchView.OnCloseListener() {
        @Override
        public boolean onClose() {
            mQATaskList.clear();
            searchText = "";
            new UpdateListing().execute("0");
            return false;
        }
    };


    SearchView.OnQueryTextListener taskSearchLstener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            mQATaskList.clear();
            query = query.toLowerCase();
            searchText = query;
            new UpdateListing().execute("0");

            InputMethodManager inputMethodManager = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }

    };
}
