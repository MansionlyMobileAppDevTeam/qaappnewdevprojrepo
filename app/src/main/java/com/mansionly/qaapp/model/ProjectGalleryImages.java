package com.mansionly.qaapp.model;

public class ProjectGalleryImages {

String mImageFileName;
    public ProjectGalleryImages(String mPicIdSttr, String mFlagCustomerViewable, String mFileName, String mBigImage) {

        mImageFileName=mFileName;
    }

    public String getProjectImage() {
        return mImageFileName;
    }
}
