package com.mansionly.qaapp.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StatFs;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.mansionly.qaapp.App;
import com.mansionly.qaapp.R;
import com.mansionly.qaapp.view.Task_Approve_Step_1_Activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utility_functions {

    public static boolean internetConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        NetworkInfo mobNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean mobileConnected = false;
        if (mobNetInfo != null)
            mobileConnected = mobNetInfo.isConnected();
        boolean netConnected = false;
        if (activeNetInfo != null)
            netConnected = activeNetInfo.isConnected();

        return (mobileConnected || netConnected);
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public static int GetBitmapSize(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }

    public static void saveImageToDevice(final Context mContext, final Bitmap clickedImage) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + File.separator + mContext.getString(R.string.app_name));
        myDir.mkdirs();
        String fname = System.currentTimeMillis() + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            clickedImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            App.setCapturedImageAbsolutePath(file.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static Bitmap compressImage(Context context, String imageFilePath) {
//
//        Bitmap scaledBitmap = null;
//
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        Bitmap bmp = BitmapFactory.decodeFile(imageFilePath, options);
//
//        int actualHeight = 450;
//        int actualWidth = 400;
//        float maxHeight = 600.0f;
//        float maxWidth = 400.0f;
//        float imgRatio = actualWidth * 2 / actualHeight * 2;
//        float maxRatio = maxWidth / maxHeight;
///*
//        DisplayMetrics displayMetrics = context.getResources()
//                .getDisplayMetrics();
//
//        String screenWidthInPix = String.valueOf(displayMetrics.widthPixels);
//
//        String screenheightInPix = String.valueOf(displayMetrics.heightPixels);*/
//
//        if (actualHeight > maxHeight || actualWidth > maxWidth) {
//            if (imgRatio < maxRatio) {
//                imgRatio = maxHeight / actualHeight;
//                actualWidth = (int) (imgRatio * actualWidth);
//                actualHeight = (int) maxHeight;
//            } else if (imgRatio > maxRatio) {
//                imgRatio = maxWidth / actualWidth;
//                actualHeight = (int) (imgRatio * actualHeight * 2);
//                actualWidth = (int) maxWidth * 2;
//            } else {
//                actualHeight = (int) maxHeight;
//                actualWidth = (int) maxWidth;
//
//            }
//        }
//
//        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
//        options.inJustDecodeBounds = false;
//        options.inDither = false;
//        options.inPurgeable = true;
//        options.inInputShareable = true;
//        options.inTempStorage = new byte[16 * 1024];
//
//        try {
//            bmp = BitmapFactory.decodeFile(imageFilePath, options);
//        } catch (OutOfMemoryError exception) {
//            exception.printStackTrace();
//
//        }
//        try {
//            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
//        } catch (OutOfMemoryError exception) {
//            exception.printStackTrace();
//        }
//
//        float ratioX = actualWidth / (float) options.outWidth;
//        float ratioY = actualHeight / (float) options.outHeight;
//        float middleX = actualWidth / 2.0f;
//        float middleY = actualHeight / 2.0f;
//
//        Matrix scaleMatrix = new Matrix();
//        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
//
//        Canvas canvas = new Canvas(scaledBitmap);
//        canvas.setMatrix(scaleMatrix);
//        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(
//                Paint.FILTER_BITMAP_FLAG));
//
//        ExifInterface exif;
//        try {
//            exif = new ExifInterface(imageFilePath);
//
//            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
//            Log.d("EXIF", "Exif: " + orientation);
//            Matrix matrix = new Matrix();
//            if (orientation == 6) {
//                matrix.postRotate(90);
//                Log.d("EXIF", "Exif: " + orientation);
//            } else if (orientation == 3) {
//                matrix.postRotate(180);
//                Log.d("EXIF", "Exif: " + orientation);
//            } else if (orientation == 8) {
//                matrix.postRotate(270);
//                Log.d("EXIF", "Exif: " + orientation);
//            }
//            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(),
//                    matrix, true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return scaledBitmap;
//
//    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth,
                                             int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static File GeneratePicFileFromBmp(String picFilePath, Bitmap picBmp, int compRatio) {
        FileOutputStream outStream = null;
        File QAPPDirInInternalSDCard = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()
                + File.separator + "Mansionly" + File.separator + "QAApp");
        if (!QAPPDirInInternalSDCard.exists())
            QAPPDirInInternalSDCard.mkdir();
        File file = new File(picFilePath);
        try {
            outStream = new FileOutputStream(file);
            picBmp.compress(Bitmap.CompressFormat.JPEG, compRatio, outStream);
            outStream.flush();
            outStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static int convertPixelsToDp(float px, Context context) {
        int dp = 0;
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        // will either be DENSITY_LOW, DENSITY_MEDIUM or DENSITY_HIGH
        int dpiClassification = metrics.densityDpi;

        if (dpiClassification == DisplayMetrics.DENSITY_MEDIUM) {
            dp = (int) (px / (metrics.densityDpi / 160f));
            dp = dp / 2;
        }

        if (dpiClassification == DisplayMetrics.DENSITY_HIGH) {
            dp = (int) (px / (metrics.densityDpi / 160f));
        } else if (dpiClassification == DisplayMetrics.DENSITY_XHIGH) {
            dp = (int) (px / (metrics.densityDpi / 160f));
            dp = dp * 2;
        }
        return dp;
    }

//    public static byte[] getFileDataFromBitmap(Context context, Bitmap bitmap) {
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
//
//        return byteArrayOutputStream.toByteArray();
//    }

//    public static void CopyStream(InputStream is, OutputStream os) {
//        final int buffer_size = 1024;
//        try {
//            byte[] bytes = new byte[buffer_size];
//            for (; ; ) {
//                int count = is.read(bytes, 0, buffer_size);
//                if (count == -1)
//                    break;
//                os.write(bytes, 0, count);
//            }
//        } catch (Exception ex) {
//        }
//    }

//    public static Bitmap RetrieveBmpFromFilePath(String picFilePath) {
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//        Bitmap bitmap = BitmapFactory.decodeFile(picFilePath, options);
//        return bitmap;
//    }

    public static Bitmap decodeSampledBitmapFromFile(String filePath, int reqWidth, int reqHeight) {
        //code sample taken from developer.android.com

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }

    public static void CopyFile(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static String GetFormatedDate(String dateStr) {

        String resultantDate;

        SimpleDateFormat originalFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

        Date date = null;
        SimpleDateFormat targetFormat = new SimpleDateFormat("dd MMM yyyy");

        try {
            date = originalFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        resultantDate = targetFormat.format(date);

        return resultantDate;


    }

    public static Bitmap getResizedBitmap(Bitmap bitmap, int h, int w) {

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float scaleWidth = ((float) w) / width;
        float scaleHeight = ((float) h) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, false);

        return resizedBitmap;
    }

    public static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static void showToastOnMainThread(final Activity mActivity, final String msg) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static int dpToPixel(Context mContext, int dp) {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static void DeleteAllUploadedTempMilestonePicFiles() throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp");
        if (!picFolderInSDCard.exists()) {
            try {
                if (picFolderInSDCard.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (picFolderInSDCard.listFiles().length != 0) {
                for (File eachFile : picFolderInSDCard.listFiles()) {
                    if (eachFile.getName().contains("temp_reject") || eachFile.getName().contains("temp_approve")) {
                        eachFile.delete();
                    }
                }
            }
        }
        return;
    }
    public static void DeleteAllUploadedPeojectAlbumPicFiles() throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        File picFolderInSDCard = new File(tempDir.getAbsoluteFile() + File.separator + "Mansionly"
                + File.separator + "QAApp"+ File.separator + "MansionlyProjectAlbum");
        if (!picFolderInSDCard.exists()) {
            try {
                if (picFolderInSDCard.mkdirs()) {
                    System.out.println("Directory created");
                } else {
                    StatFs memStatus = new StatFs(Environment.getExternalStorageDirectory().getPath());
                    long bytesAvailable = (long) memStatus.getBlockSize() * (long) memStatus.getAvailableBlocks();
                    if (bytesAvailable < 0)
                        System.out.println("Directory is not created due to memory full");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (picFolderInSDCard.listFiles().length != 0) {
                for (File eachFile : picFolderInSDCard.listFiles()) {
                    if (eachFile.getName().contains("album")) {
                        eachFile.delete();
                    }
                }
            }
        }
        return;
    }
}
