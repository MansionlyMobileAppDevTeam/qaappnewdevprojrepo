package com.mansionly.qaapp;

/**
 * Created by root on 3/3/17.
 */
public class Constants {
    public static final int CAPTURE_MEDIA = 100;
    public static final int CAMERA_PERMISSION_REQUEST = 101;
    public static final int LOCATION_REQUEST = 102;

    public interface RequestCode {
        int GOOGLE_SIGN_IN = 101;
    }
}
